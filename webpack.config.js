const path = require('path');

module.exports = {
    context  : path.resolve(__dirname, './src'),
    entry    : {
        da: "./index.js",
    },
    output   : {
        filename     : '[name].js',
        path         : path.resolve(__dirname, 'public'),
        publicPath   : '/',
        library      : 'da',
        libraryTarget: 'umd',
    },
    devServer: {
        publicPath: "./dist",
        public    : "localhost:8080/demo",
    },
    watch    : true,
    devtool  : "eval-source-map",
    module   : {
        rules: [
            {
                test   : /\.js$/,
                include: [/src/],
                use    : [
                    {
                        loader : 'babel-loader',
                        options: {presets: ['es2015']},
                    }
                ],
            },
        ]
    },
};

import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    extractPoint,
    adjust,
    splitCurve,
    clamp,
    simpleQuadratic,
    continueCurve,
} from "drawpoint";

class Head extends BodyPart {
    constructor(...data) {
        super({
            loc       : "head",
            forcedSide: null,
            // can't have head without neck
            parentPart: "neck",
            layer     : Layer.BELOW_HAIR,
            // want reflection to be drawn
            reflect   : true,
        }, ...data);
    }
}


class BrowShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+head",
            layer: Layer.HAIR,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const sp = splitCurve(0.5, ex.brow.outbot, ex.brow.inbot);
        const out = sp.left.p2;
        const inner = sp.right.p2;

        const bot = extractPoint(ex.nose.top);
        bot.cp1 = simpleQuadratic(inner, bot, 0.5, -1);

        out.cp1 = {
            x: bot.x + 0.4,
            y: bot.y - 0.3,
        };
        out.cp2 = {
            x: out.x + 0.5,
            y: out.y + 1
        };

        return [out, inner, bot, out];

    }
}


class HeadHumanShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+head",
            layer: Layer.HAIR,
        }, ...data);
    }

    calcDrawPoints(ex) {

        const {top, skullSide, skullBot, chinOut, chinBot} = calcHeadShading(ex,
            this.faceWidth);

        top.cp1 = {
            x: chinBot.x + this.faceWidth * 0.3,
            y: chinBot.y
        };
        top.cp2 = {
            x: top.x + this.faceWidth * 0.4,
            y: top.y
        };

        return [
            top, skullSide, skullBot, chinOut, chinBot, top
        ];
    }
}


function calcHeadShading(ex, faceWidth) {
    let sp = splitCurve(0.4, ex.skull, ex.skull.side);
    const top = extractPoint(sp.left.p2);
    let skullSide = sp.right.p2;
    skullSide = adjust(skullSide, -faceWidth * 0.045, 0);

    const skullBot = adjust(ex.skull.bot, -faceWidth * 0.03, 0);
    skullBot.cp1 = simpleQuadratic(skullSide, skullBot, 0.5, 0.5);
    // const jaw = adjust(ex.jaw, -w * 0.04, 0);
    // TODO adjust this with a face gauntness mod
    const chinOut = (ex.chin.out) ? adjust(extractPoint(ex.chin.out), -faceWidth * 0.026, 0) : {};

    sp = splitCurve(0.8, chinOut, ex.chin.bot);
    const chinBot = sp.left.p2;
    chinBot.cp1 = continueCurve(skullBot, chinOut, 0.7);
    chinBot.cp2 = adjust(chinBot.cp2, -faceWidth * 0.02, 0.1);

    return {
        top,
        skullSide,
        skullBot,
        chinOut,
        chinBot
    };
}

// head will export skull, ear.top, ear.bot, jaw, chin.out, chin.bot
export class HeadHuman extends Head {
    constructor(...data) {
        super({
            shadingParts: [HeadHumanShading]
        }, ...data);
    }

    getLineWidth(avatar) {
        return clamp(1.7 - avatar.dim.faceFem * 0.05, 0.7, 1.5);
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            const h = this.height;
            let skull = ex.skull = {
                x: 0,
                y: h
            };
            ex.ear = {};

            skull.side = {
                x  : this.faceWidth * 0.095 - this.faceFem * 0.01,
                y  : ex.skull.y - this.faceLength * 0.035,
                cp1: {
                    x: this.faceWidth * 0.045,
                    y: h
                },
            };
            skull.side.cp2 = {
                x: skull.side.x,
                y: h - 2
            };
            skull.bot = {
                x: skull.side.x - this.faceFem * 0.0135,
                y: skull.side.y - (5) * this.faceLength / 220,
            };

            // always directly below the bottom of the ear, but distance to it changes
            ex.jaw = {
                // x: skull.bot.x - (0.7 - this.faceFem * 0.02) + mods.jawJut * 0.1,
                x: skull.bot.x - clamp(1.5 - this.faceFem * 0.05 - mods.jawJut * 0.1,
                    0,
                    this.faceWidth * 0.2),
                y: skull.bot.y - clamp(this.faceLength * 0.025 - this.faceFem * 0.2,
                    0,
                    this.faceLength * 0.03)
            };

            ex.chin = {};
            // we define bottom of the chin because sometimes chin.out is just a slice of the
            // bezier
            // to it
            ex.chin.bot = {
                x  : 0,
                y  : ex.skull.y - this.faceLength * 0.1 + this.faceFem * 0.035,
                cp1: {
                    x: ex.jaw.x - clamp(1.5 - this.faceFem * 0.02, 0.5, 2),
                    y: ex.jaw.y -
                       clamp(1 + this.faceFem * 0.05 + mods.cheekFullness * 0.1,
                           0,
                           this.faceLength * 0.025)
                },
            }
            ;
            ex.chin.bot.cp2 = {
                x: this.chinWidth * 0.1 - clamp(this.faceFem * 0.22, 0, 3),
                y: ex.chin.bot.y - 1 + clamp(this.faceFem * 0.035, 0, 2),
            };

            let s = splitCurve(1 - 0.78, ex.jaw, ex.chin.bot);
            ex.chin.out = s.right.p1;
            ex.chin.out.cp1 = s.left.p2.cp1;
            ex.chin.bot = s.right.p2;
            if (this.faceFem < 13) {
                ex.chin.out.cp1 =
                    simpleQuadratic(ex.jaw, ex.chin.out, 0.5, this.faceFem * 0.1);
            }
        }
        return [
            ex.skull,
            ex.skull.side,
            ex.skull.bot,
            ex.jaw,
            ex.chin.out,
            ex.chin.bot
        ];
    }
}

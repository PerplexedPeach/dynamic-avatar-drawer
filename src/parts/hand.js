import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    extractPoint,
    adjust,
    splitCurve,
    rotatePoints,
} from "drawpoint";
import {averageQuadratic, requirePart} from "../util/draw";
import {averagePoint} from "../util/utility";

class Hand extends BodyPart {
    constructor(...data) {
        super({
            loc  : "hand",
            layer: Layer.ARMS,
        }, ...data);
    }
}


// hand exports hand.knuckle, hand.tip, hand.palm, thumb.tip, thumb.out
export class HandHuman extends Hand {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {
        requirePart("wrist", ex);
        if (calculate) {
            let hand = ex.hand = {};
            let thumb = ex.thumb = {};

            // anchor point where all drawing derives from
            const anchor = ex.wrist.out;
            const size = this.handSize * 0.1 + this.upperMuscle * 0.04;

            hand.knuckle = {
                x: anchor.x + size * 0.15,
                y: anchor.y - size * 0.6,
            };
            hand.tip = {
                x: anchor.x + size * 0.1,
                y: hand.knuckle.y - 2 - size * 0.6,
            };
            hand.tip.cp1 =
                averageQuadratic(hand.knuckle, hand.tip, 0.7, 1, -0.2);

            hand.palm = {
                x  : anchor.x * 0.65 + ex.wrist.in.x * 0.35,
                y  : hand.knuckle.y - size * 0.1,
                cp1: {
                    x: hand.tip.x - 0.5 - size * 0.15,//1.5 - size * 0.05,
                    y: hand.tip.y + size * 0.1
                },
            };
            hand.palm.cp2 = {
                x: hand.palm.x + 1 + size * 0.2,
                y: hand.palm.y - 1 - size * 0.2,
            };


            thumb.tip = {
                x  : hand.palm.x - size * 0.03,
                y  : hand.palm.y - 1 - size * 0.25,
                cp1: {
                    x: hand.palm.x - size * 0.1,
                    y: hand.palm.y - size * 0.1
                },
            };
            thumb.tip.cp2 = {
                x: thumb.tip.x + size * 0.1,
                y: thumb.tip.y + size * 0.1,
            };

            thumb.out = {
                x  : ex.wrist.in.x + 0.1,
                y  : ex.wrist.in.y - size * 0.2,
                cp1: {
                    x: thumb.tip.x - size * 0.15,
                    y: thumb.tip.y + size * 0.2
                },
            };
            thumb.out.cp2 = {
                x: thumb.out.x - size * 0.05,
                y: thumb.out.y - size * 0.2,
            };

            // continued with arm rotation
            // rotate arms
            rotatePoints(anchor,
                (mods.armRotation + mods.handRotation) * Math.PI / 180,
                hand.knuckle,
                hand.tip,
                hand.palm,
                thumb.tip,
                thumb.out);


            // ball up into fist
            if (this.upperMuscle > 12) {
                const bulk = this.upperMuscle - 12;
                hand.knuckle.y -= bulk * 0.05;
                thumb.tip = adjust(thumb.tip, -bulk * 0.03, bulk * 0.05);
                hand.tip.y += bulk * 0.05;

                thumb.out.cp1.x -= bulk * 0.05;
                thumb.out.cp2.x -= bulk * 0.02;
                rotatePoints(hand.knuckle, -bulk * 0.035, hand.tip, hand.palm);

                const sp = splitCurve(0.6, hand.knuckle, hand.tip);
                hand.fist = sp.left.p2;
                hand.tip.cp1 = sp.right.p2.cp1;
                rotatePoints(hand.fist,
                    -bulk * 0.05,
                    hand.tip,
                    hand.palm.cp1,
                    hand.palm.cp2);

                hand.palm.x -= bulk * 0.07;

                if (this.upperMuscle > 25) {
                    hand.tip = extractPoint(thumb.tip);
                    hand.palm = extractPoint(hand.palm);
                    thumb.out = extractPoint(thumb.out);
                    thumb.knuckle = averagePoint(thumb.tip, thumb.out);
                    thumb.knuckle.x -= bulk * 0.06;
                }

                rotatePoints(anchor,
                    -bulk * 0.01,
                    hand.tip,
                    hand.first,
                    hand.palm,
                    thumb.tip,
                    thumb.knuckle);
            }
        }
        return [
            ex.wrist.out,
            ex.hand.knuckle,
            ex.hand.fist,
            ex.hand.tip,
            ex.hand.palm,
            ex.thumb.tip,
            ex.thumb.knuckle,
            ex.thumb.out,
            ex.wrist.in,
        ];
    }
}


import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    extractPoint,
    clamp,
    reflect,
    simpleQuadratic,
} from "drawpoint";
import {DecorativePart} from "../decorative_parts/decorative_part";
import {adjustColor} from "../util/utility";
import {requirePart} from "../util/draw";

class Penis extends BodyPart {
    constructor(...data) {
        super({
            loc         : "penis",
            layer       : Layer.GENITALS,
            coverConceal: ["groin", "left leg"],
            aboveParts  : ["testicles"],
        }, ...data);
    }

    getLineWidth(avatar) {
        return clamp(avatar.getDim("penisSize") / 100, 0.8, 1.5);
    }
}


export class PenisHuman extends Penis {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return "inherit";
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            let penis = ex.penis = {};

            penis.center = {
                x: 0,
                y: ex.groin.y + 3
            };

            penis.top = {
                x: penis.center.x + 1 + mods.penisThickness * 0.01 + this.penisSize * 0.005,
                y: penis.center.y + this.penisSize * 0.006,
            };
            penis.bot = {
                x: penis.top.x -
                   clamp(0.5 - mods.penisThickness * 0.01 - this.penisSize * 0.0001, 0, 5),
                y: penis.top.y - this.penisSize * 0.1
            };

            penis.tip = {
                x: penis.center.x,
                y: penis.bot.y - 1 - this.penisSize * 0.01
            };

            penis.bot.cp1 = simpleQuadratic(penis.top,
                penis.bot,
                0.4,
                clamp(0.5 + mods.penisThickness * 0.01 - this.penisSize * 0.002, 0, 3));
            penis.tip.cp1 = {
                x: penis.bot.x + this.penisSize * 0.004,
                y: penis.bot.y - this.penisSize * 0.005
            };
            penis.tip.cp2 = {
                x: penis.tip.x + 1,
                y: penis.tip.y + 0.2
            };
        }

        let reflectedBot = reflect(ex.penis.bot);
        let reflectedTop = reflect(ex.penis.top);
        reflectedBot.cp1 = reflect(ex.penis.tip.cp2);
        reflectedBot.cp2 = reflect(ex.penis.tip.cp1);
        reflectedTop.cp1 = reflect(ex.penis.bot.cp1);
        return (this.penisSize > 10) ? [
            ex.penis.top,
            ex.penis.bot,
            ex.penis.tip,
            reflectedBot,
            reflectedTop,
        ] : [];
    }
}


class PenisHead extends DecorativePart {
    constructor(...data) {
        super({
            loc         : "penis",
            layer       : Layer.GENITALS,
            coverConceal: ["groin", "left leg"],
        }, ...data);
    }

    getLineWidth(avatar) {
        return clamp(avatar.getDim("penisSize") / 100, 0.8, 1.5);
    }
}


export class PenisHeadHuman extends PenisHead {
    constructor(...data) {
        super(...data);
    }

    fill(ignore, ex) {
        return adjustColor(ex.baseStroke,
            {
                l: -5,
                s: -5
            });
    }

    stroke(ignore, ex) {
        return adjustColor(ex.baseStroke,
            {
                l: -5,
                s: -3
            });
    }

    calcDrawPoints(ex, mods, calculate) {
        if (mods.arousal < 50) {
            return [];
        }

        requirePart("penis", ex);
        if (calculate) {
            ex.penis.tiptop = {
                x: ex.penis.center.x,
                y: ex.penis.bot.y + this.penisSize * 0.003
            };
        }

        let bot = extractPoint(ex.penis.bot);
        bot.cp1 = simpleQuadratic(ex.penis.tiptop, bot, 0.5, 0.5);
        let reflectedBot = reflect(ex.penis.bot);
        reflectedBot.cp1 = reflect(ex.penis.tip.cp2);
        reflectedBot.cp2 = reflect(ex.penis.tip.cp1);
        ex.penis.tiptop.cp1 = simpleQuadratic(reflectedBot, ex.penis.tiptop, 0.5, 0.5);
        return [ex.penis.tiptop, bot, ex.penis.tip, reflectedBot, ex.penis.tiptop];
    }
}


import {DecorativePart} from "./decorative_part";
import {Layer} from "../util/canvas";
import {none, adjust, drawSpecificCurl, breakPoint} from "drawpoint/dist-esm";
import {Materials} from "../materials";

export class LegFur extends DecorativePart {
    constructor(...data) {
        super(Materials.brownFur, {
            fill      : none,
            loc       : "+leg",
            layer     : Layer.FRONT,
            aboveParts: ["parts leg"]
        }, ...data);
    }

    calcDrawPoints(ex) {
        const points = [];
        // fur near the "feet"
        let p = ex.ankle.in;
        points.push(
            ...drawSpecificCurl({x: p.x, y: p.y, deflection: -.5},
                adjust(p, 1, -3), adjust(p, 3, 0)), breakPoint);
        p = adjust(ex.knee.in, 3, -3);
        points.push(
            ...drawSpecificCurl({x: p.x, y: p.y, deflection: -.5},
                adjust(p, 2, -6), adjust(p, 5, -1)), breakPoint);
        p = adjust(ex.calf.out, -8, 0);
        points.push(
            ...drawSpecificCurl({x: p.x, y: p.y, deflection: -.5},
                adjust(p, 2, -6), adjust(p, 4, 3)), breakPoint);
        p = adjust(ex.thigh.out, -9, 0);
        points.push(
            ...drawSpecificCurl({x: p.x, y: p.y, deflection: -1.},
                adjust(p, 3, -9), adjust(p, 5, 2)), breakPoint);

        return points;
    }
}

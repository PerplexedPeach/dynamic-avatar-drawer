import {getDefault, applyMods, removeMods} from "../util/player";
import {baseDimDesc, basedimDiscretePool} from "./dimensions";
import {Skeleton} from "../skeletons/skeleton";
import {Part, getAttachedLocation, partConflict, getChildLocation} from "../parts/part";
import {statLimits, statDiscretePool} from "./stats";
import {modLimits, modDiscretePool} from "./mods";
import {loaded} from "../load/load";
import {Hair} from "../hair_parts/hair_part";
import {ShadingPart} from "../draw/shading_part";
import {clamp} from "drawpoint";
import {extractUnmodifiedLocation, extractLocationModifier} from "../util/part";
import {Items} from "../items/item";
import {Expression} from "./expression";
import {Clothes} from "../clothes/clothing";
import {Bra, Panties} from "../clothes/underwear";
import {styles} from "../hair_parts/styles";

// used to generate default values

function defaultBaseDimensions(skeleton) {
    return getDefault(baseDimDesc[skeleton], basedimDiscretePool[skeleton])();
}

function defaultParts(partsName, skeleton) {
    let parts = [];
    const defParts = Skeleton[skeleton][partsName];
    for (let i = 0; i < defParts.length; ++i) {
        const {side, part} = defParts[i];
        parts.push(Part.create(part, {side}));
    }
    return parts;
}

function addSexParts(avatar) {
    const skeleton = Skeleton[avatar.skeleton];
    let sexParts;
    if (avatar.isFemale()) {
        sexParts = skeleton.femaleParts;
    }
    if (avatar.isMale()) {
        sexParts = skeleton.maleParts;
    }
    sexParts.forEach(({partGroup, side, part}) => {
        avatar.attachPart(Part.create(part, {side}), avatar[partGroup]);
    });
}

/**
 * Player holds all data necessary for draw to render it.
 * It is also meant to be extended by the user of the library to include gameplay statistics.
 * The gameplay statistics can then be linked to the calculation of drawing dimensions.
 * @memberof module:da
 */
export class Player {
    static defaultStats() {
        return getDefault(statLimits, statDiscretePool)();
    }

    static defaultMods() {
        return getDefault(modLimits, modDiscretePool)();
    }

    static defaultVitals() {
        // return getDefault(vitalLimits)();
        return {};
    }

    static defaultClothes() {
        /**
         * default Clothing for a Player to wear
         * @type {Clothing[]}
         */
        return [Clothes.create(Bra), Clothes.create(Panties)];
    }


    /**
     * @constructor
     * @param {object} data Properties of the Player object (override the default values)
     */
    constructor(data) {
        if (!loaded) {
            throw new Error(
                "trying to create Player without having loaded first! (call load() before)");
        }
        Object.assign(this,
            Player.defaultStats(), {
                skeleton       : "human",
                // default value construction; overriden by properties of data passed in
                // mods, physique, worn require dynamic default value construction, so are
                // assigned separately
                // ordered list of body parts (parts depending on others should be after)
                parts          : [],
                // parts that are of the face
                faceParts      : [],
                // parts that are for decorative/flavour only
                decorativeParts: [],
                hairParts      : [],
                clothingParts  : [],
                // calculated from stats, base dimensions (1-to-1), and mods
                inv            : [],
                Mods           : {},
                vitals         : {},  // where you would store max_hp, cur_hp, etc...
                traits         : [],
                _expression    : Expression.neutral,
            });

        // handle objects separately
        for (let property in data) {
            if (data.hasOwnProperty(property) &&
                (typeof data[property] === "number" || typeof data[property] === "string")) {
                this[property] = data[property];
            }
        }

        this.parts = defaultParts("defaultParts", this.skeleton);
        addSexParts(this);

        this.decorativeParts = defaultParts("defaultDecorativeParts", this.skeleton);
        this.faceParts = defaultParts("defaultFaceParts", this.skeleton);

        this.shadingParts = [];
        this.parts.forEach((part) => {
            if (part.shadingParts) {
                part.shadingParts.forEach((shadingPartPrototype) => {

                    const attachedLocation = getAttachedLocation(shadingPartPrototype);

                    if (this.getPartInLocation(attachedLocation)) {

                        this.attachPart(Part.create(shadingPartPrototype),
                            this.shadingParts);
                    }
                });
            }
        });
        this.faceParts.forEach((part) => {
            if (part.shadingParts) {
                part.shadingParts.forEach((shadingPartPrototype) => {

                    const attachedLocation = getAttachedLocation(shadingPartPrototype);

                    if (this.getPartInLocation(attachedLocation, this.faceParts)) {

                        this.attachPart(Part.create(shadingPartPrototype),
                            this.shadingParts);
                    }
                });
            }
        });

        this.clothingParts = [];
        this.clothes = [];
        this.items = [];

        // clothing book keeping
        // list of Clothes, not to be directly accessed
        Player.defaultClothes().forEach((clothing) => {
            this.wearClothing(clothing);
        });

        // upgrade with newer default values if necessary so saves are compatible (new stat wouldn't
        // be missing)
        if (data) {
            this.basedim =
                Object.assign({}, defaultBaseDimensions(this.skeleton), data.basedim);
            this.Mods = Object.assign({}, Player.defaultMods(), data.Mods);
            this.vitals = Object.assign({}, Player.defaultVitals(), data.vitals);

            if (data.parts) {
                // override body parts
                data.parts.forEach((part) => {
                    this.attachPart(part);
                });
            }
            if (data.decorativeParts) {
                // override decorative parts
                data.decorativeParts.forEach((part) => {
                    this.attachPart(part, this.decorativeParts);
                });
            }
            if (data.faceParts) {
                data.faceParts.forEach((part) => {
                    this.attachPart(part, this.faceParts);
                });
            }

            if (data.clothes) {
                this.removeAllClothing();
                data.clothes.forEach((clothing) => {
                    this.wearClothing(clothing);
                });
            }

            if (data.items) {
                data.items.forEach((item) => {
                    this.wieldItem(item);
                });
            }

            // public specification
        } else {
            this.basedim = defaultBaseDimensions(this.skeleton);
            this.Mods = Player.defaultMods();
            this.vitals = Player.defaultVitals();
        }


        // initialize dimensions
        this.calcDimensions();


    }

    toString() {
        return this.name;
    }

    /**
     * Keep stats within boundaries
     */
    clampStats() {
        for (var p in statLimits) {
            if (statLimits.hasOwnProperty(p) === false) {
                continue;
            }
            this[p] = clamp(this[p], statLimits[p].low, statLimits[p].high);
        }
    }

    // property getters
    /**
     * Get modified statistic
     * @param {string} param Name of statistic
     * @returns {number} Modified statistic value
     */
    get(param) {
        return this[param] + this._clampedMods[param];
    }

    /**
     * Get modified dimensions
     * @param {string} param Name of dimension
     * @returns {number} Modified dimension value
     */
    getDim(param) {
        return this.basedim[param] + this._clampedMods[param];
    }

    /**
     * Get modifier
     * @param {string} param Name of modifier
     * @returns {number} Modifier value
     */
    getMod(param) {
        return this._clampedMods[param];
    }

    /**
     * Get statistical description of a dimension
     * @param {string} param Name of dimension
     * @returns {object} Statistical dimension description with low, high, stdev, avg
     */
    getDimDesc(param) {
        return baseDimDesc[this.skeleton][param];
    }


    /**
     * Calculate each dimension with 'this' set as the player inside the function
     * This is automatically called when drawing, so it's rarely called manually.
     */
    calcDimensions() {
        // clamp mods for use in drawing (can't clamp original since doing so would remove
        // information about temporary mods
        this._clampedMods = {};
        for (let mod in this.Mods) {
            if (modLimits.hasOwnProperty(mod)) {
                this._clampedMods[mod] =
                    clamp(this.Mods[mod], modLimits[mod].low, modLimits[mod].high);
            }
        }

        // reset dimensions
        this.dim = {};
        const dimDesc = baseDimDesc[this.skeleton];
        for (let dimension in dimDesc) {
            if (dimDesc.hasOwnProperty(dimension) === false) {
                continue;
            }
            if (dimDesc[dimension].hasOwnProperty("calc")) {
                this.dim[dimension] = dimDesc[dimension].calc.call(this);
            } else {
                this.dim[dimension] = this.getDim(dimension);
            }
            this.dim[dimension] =
                clamp(this.dim[dimension], dimDesc[dimension].low, dimDesc[dimension].high);
        }
        this.dim.torsoLength = this.dim.height * 0.85 - this.dim.legLength;

        // define hair
        this.replaceHair(styles[Math.round(this.dim.hairStyle)]);
    }

    // body part interaction

    /**
     * Attach a new body part, replacing any conflicting parts if necessary
     * @param {Part} newPart New body part to be added
     * @param {Part[]} parts Part group of the Player to attach to
     * @returns {(Part|null)} Either the part that was removed, or null if nothing was removed
     */
    attachPart(newPart, parts = this.parts) {
        if (newPart instanceof ShadingPart) {
            parts = this.shadingParts;
        }

        let replacedPart = null;
        for (let p = 0; p < parts.length; ++p) {
            if (partConflict(newPart, parts[p])) {
                let oldPart = parts[p];
                parts[p] = newPart;
                replacedPart = oldPart;
                break;
            }
        }

        if (replacedPart === null) {
            // no existing part with this specific location
            parts.push(newPart);
        }

        // adding associated shading with this part if necessary
        if (newPart.shadingParts) {
            newPart.shadingParts.forEach((shadingPartPrototype) => {
                const attachedLocation = getAttachedLocation(shadingPartPrototype);

                if (this.getPartInLocation(attachedLocation) ||
                    this.getPartInLocation(attachedLocation, this.faceParts) ||
                    this.getPartInLocation(attachedLocation, this.hairParts)) {
                    this.attachPart(Part.create(shadingPartPrototype),
                        this.shadingParts);
                }
            });
        }

        return replacedPart;
    }

    /**
     * Either returns a reference to the part in a specific location or null if the part doesn't
     * exist
     * @param {string} location Where the part is located
     * @param {Part[]} parts Part group to search in
     * @param {number} [siblingIndex=0] Index relative to other parts in same location in same group
     * @returns {(Part|null)} Part in this location or null
     */
    getPartInLocation(location, parts = this.parts, siblingIndex = 0) {
        const testLocation = extractUnmodifiedLocation(location);
        for (let p = 0; p < parts.length; ++p) {
            if (extractUnmodifiedLocation(parts[p].loc) === testLocation) {
                if (siblingIndex > 0) {
                    --siblingIndex;
                    continue;
                }
                return parts[p];
            }
        }
        return null;
    }

    removeSpecificPart(partPrototype, parts = this.parts, siblingIndex = 0) {
        for (let p = 0; p < parts.length; ++p) {
            if (parts[p] instanceof partPrototype) {
                if (siblingIndex > 0) {
                    --siblingIndex;
                    continue;
                }

                const oldPart = parts[p];
                parts.splice(p, 1);
                this.doRemovePart(oldPart, parts);

                return oldPart;
            }
        }
        return null;
    }

    /**
     * Return whether a part is covered by clothing
     * Depends on the part to specify what locations are considered coverable
     * with part.coverConceal
     * @param part
     * @returns {boolean}
     */
    checkPartCoveredByClothing(part) {
        let partCovered = false;
        part.coverConceal.forEach((coveringLocation) => {
            if (coveringLocation === "this") {
                coveringLocation = part.loc;
            }
            const coveringClothing = this.getClothingInLocation(coveringLocation);
            if (coveringClothing.length) {
                partCovered = true;
            }
        });
        return partCovered;
    }

    /**
     * Remove a part at a specific location
     * @param {string} loc Where the part is located
     * @param {Part[]} parts Part group of the Player to remove from
     * @param {number} [siblingIndex=0] Index relative to other parts in same location in same group
     * @returns {(Part|null)} Part removed or null if nothing was removed
     */
    removePart(loc, parts = this.parts, siblingIndex = 0) {
        for (let p = 0; p < parts.length; ++p) {
            if (extractUnmodifiedLocation(parts[p].loc) === loc) {
                if (siblingIndex > 0) {
                    --siblingIndex;
                    continue;
                }

                const oldPart = parts[p];
                parts.splice(p, 1);
                this.doRemovePart(oldPart, parts);

                return oldPart;
            }
        }
        return null;
    }

    doRemovePart(oldPart, parts) {
        // ensure accompanying shadows are also removed
        if (oldPart.shadingParts) {
            oldPart.shadingParts.forEach((shadingPart) => {
                this.removeSpecificPart(shadingPart, this.shdadingParts);
            });
        }

        // recursively remove any child parts
        if (oldPart.hasOwnProperty("childParts")) {
            const childParts = oldPart.childParts;
            for (let i = 0; i < childParts.length; ++i) {
                const {childLoc} = getChildLocation(oldPart.loc, childParts[i]);
                this.removePart(childLoc, parts);
            }
        }
    }

    replaceHair(newHair) {
        // clear hair parts
        // remove any shadows manually
        this.hairParts.forEach((part) => {
            if (part.shadingParts) {
                part.shadingParts.forEach((shadingPartPrototype) => {
                    this.removeSpecificPart(shadingPartPrototype, this.shadingParts);
                });
            }
        });
        this.hairParts = [];

        newHair.forEach((part) => {
            this.attachPart(Hair.create(part), this.hairParts);
        });
    }

    // expression manipulation
    /**
     * Apply an expression, replacing the current expression
     * @param {Expression} expression All expression are kept in Expression
     */
    applyExpression(expression) {
        if (expression.hasOwnProperty("Mods") === false) {
            throw new Error("Invalid expression object since it has no Mods");
        }
        this.removeExpression();

        // apply modifiers
        for (let mod in expression.Mods) {
            if (expression.Mods.hasOwnProperty(mod)) {
                this.Mods[mod] += expression.Mods[mod];
            }
        }

        // any special modifications
        if (expression.hasOwnProperty("specificModification")) {
            if (expression.hasOwnProperty("removeSpecificModification")) {
                throw new Error("Expression has specific modifications but no way of removing them");
            }

            // apply specific operations on the Player object
            expression.specificModification(this);
        }
        this._expression = expression;
    }

    removeExpression() {
        // remove modifiers
        for (let mod in this._expression.Mods) {
            if (this._expression.Mods.hasOwnProperty(mod)) {
                this.Mods[mod] -= this._expression.Mods[mod];
            }
        }

        if (this._expression.hasOwnProperty("removeSpecificModification")) {
            // apply specific operations on the Player object
            this._expression.removeSpecificModification(this);
        }
    }

    // clothing manipulation
    /**
     * Find the clothes that occupies a certain body location
     * @param {string} location Unmodified body location
     * @returns {Clothing[]} list of Clothes objects that cover that body location
     */
    getClothingInLocation(location) {
        const clothes = [];
        this.clothingParts.forEach((part) => {
            if (extractUnmodifiedLocation(part.loc) === location &&
                clothes.indexOf(part._owner) < 0) {
                clothes.push(part._owner);
            }
        });
        return clothes;
    }

    /**
     * Find currently worn clothing that conflicts with given clothing
     * @param clothing The
     * @param location Modified body location
     * @returns {Clothing[]} list of Clothes objects that conflict with this clothing
     */
    getConflictingClothing(clothing) {
        const clothes = [];
        clothing.parts.forEach((part) => {
            const baseLoc = extractUnmodifiedLocation(part.loc);
            const modifier = extractLocationModifier(part.loc);
            this.clothingParts.forEach((wornPart) => {
                // exist in the same location
                if (extractUnmodifiedLocation(wornPart.loc) === baseLoc) {
                    let remove = false;
                    // ignore if they don't occupy the same clothing layer
                    if (wornPart._owner.clothingLayer !== clothing.clothingLayer) {
                        return;
                    }
                    // always remove if incoming clothing is the same type
                    if (Object.getPrototypeOf(wornPart._owner) ===
                        Object.getPrototypeOf(clothing)) {
                        remove = true;
                    } else {
                        // else see if modifiers conflict
                        const wornModifier = extractLocationModifier(wornPart.loc);
                        // can't coexist due to modifiers
                        if (modifier === "-" || wornModifier === "-") {
                            remove = true;
                        }
                        // not allowed by tolerant modifiers
                        else if (modifier !== "+" && wornModifier !== "+") {
                            remove = true;
                        }
                    }

                    // not already removing this clothing
                    if (remove && clothes.indexOf(wornPart._owner) < 0) {
                        clothes.push(wornPart._owner);
                    }
                }
            });
        });
        return clothes;
    }

    /**
     * Wear a Clothing item
     * @param {Clothing} clothing Clothing to be worn
     * @returns {(Clothing[]|null)} null if failed to wear clothing, or the list of removed
     * conflicting clothes
     */
    wearClothing(clothing) {
        // remove clothing in same location
        const removedClothing = this.getConflictingClothing(clothing);
        for (let c = 0; c < removedClothing.length; ++c) {
            // fail to remove; don't automatically recover partially removed
            if (this.removeClothing(removedClothing[c]) === false) {
                return null;
            }
        }

        // successfully removed clothing
        clothing.parts.forEach((part) => {
            // if there is a base part to attach this clothing part to
            if (this.getPartInLocation(extractUnmodifiedLocation(part.loc))) {
                this.clothingParts.push(part);
                if (part.shadingParts) {
                    part.shadingParts.forEach((shadingPartPrototype) => {
                        this.attachPart(Part.create(shadingPartPrototype,
                            {_owner: clothing}),
                            this.shadingParts);
                    });
                }
            }
        });
        applyMods(this.Mods, clothing.Mods);
        this.clothes.push(clothing);

        return removedClothing;
    }

    /**
     * Remove an article of clothing
     * @param {Clothing} clothing
     * @returns {boolean} Whether clothing was successfully removed
     */
    removeClothing(clothing) {
        // copy over non-removed parts to new list
        if (clothing.cursed) {
            return false;
        }
        const newParts = [];
        this.clothingParts.forEach((part) => {
            if (part._owner !== clothing) {
                newParts.push(part);
            } else {
                if (part.shadingParts) {
                    part.shadingParts.forEach((shadingPart) => {
                        this.removeSpecificPart(shadingPart, this.shadingParts);
                    });
                }
            }

        });
        this.clothingParts = newParts;
        removeMods(this.Mods, clothing.Mods);
        this.clothes.splice(this.clothes.indexOf(clothing), 1);

        return true;
    }

    /**
     * Remove any clothing of article that can be removed
     * @returns {Array} Array of clothing that was removed
     */
    removeAllClothing() {
        const removedClothing = [];
        for (let clothingIndex = this.clothes.length - 1;
             clothingIndex >= 0;
             --clothingIndex) {
            const clothing = this.clothes[clothingIndex];
            if (this.removeClothing(clothing)) {
                removedClothing.push(clothing);
            }
        }
        return removedClothing;
    }

    // item manipulation
    wieldItem(item) {
        Items.loadItem(item);
        item.modifyPose.call(this);
        this.items.push(item);
    }

    removeItem(item) {
        item.restorePose.call(this);
        this.items.splice(this.items.indexOf(item), 1);
    }

    heightAdjust() {
        let extraHeight = 0;

        // take the max height of what's being worn in shoes location
        this.getClothingInLocation("left feet").forEach(getMaxHeight);
        this.getClothingInLocation("right feet").forEach(getMaxHeight);

        return extraHeight;

        function getMaxHeight(clothing) {
            if (clothing.shoeHeight && clothing.shoeHeight > extraHeight) {
                extraHeight = clothing.shoeHeight;
            }
        }
    }

    isFemale() {
        return this.fem > 5;
    }

    isMale() {
        return !this.isFemale();
    }
}



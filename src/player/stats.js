/**
 * Core stats (where user would define their gameplay stats)
 * Each are statistic descriptions with low, high, average, and stdev (assume normal)
 * @memberof module:da
 */
export const statLimits = {
    age: {
        low  : 0,
        high : 1e9,
        avg  : 30,
        stdev: 6,
        bias : 0,
    },
    fem: {
        low  : 0,
        high : 11,
        avg  : 5,
        stdev: 1,
        bias : 2,
    },
    sub: {
        low  : 0,
        high : 11,
        avg  : 4,
        stdev: 1,
        bias : 1,
    },
};

/**
 * Discrete core stats
 * @memberof module:da
 */
export const statDiscretePool = {
    // pool of available values for discrete properties
    skeleton: ["human"],  // underlying racial structure of player
};

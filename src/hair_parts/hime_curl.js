import {HairPart, Hair} from "./hair_part";
import {
    adjustPoints,
    adjust,
    splitCurve,
    simpleQuadratic,
    drawPoints,
    breakPoint,
    endPoint,
    extractPoint,
    reflect, clamp
} from "drawpoint";
import {ShadingPart} from "../draw/shading_part";
import {seamWidth, Layer} from "../util/canvas";
import {calcHimeCut} from "./hime_cut";

class HimeCurlShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+front hair",
            layer: Layer.GENITALS,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const {right, out, bot, fork, forkIn} = calcHimeCurl(ex, this.hairLength);
        let [top, mid] = adjustPoints(-2, 0, right, out);
        bot.cp1 = adjust(bot.cp1, -3, 0);
        bot.cp2 = adjust(bot.cp2, -4, 0);

        const sp = splitCurve(0.35, mid, bot);
        const shadingFork = adjust(sp.left.p2, -2, 0);
        shadingFork.cp1 = sp.left.p2.cp1;
        shadingFork.cp2 = {
            x: shadingFork.x + this.hairLength * 0.04,
            y: shadingFork.y - this.hairLength * 0.2
        };
        bot.cp1 = {
            x: shadingFork.x - this.hairLength * 0.02,
            y: shadingFork.y - this.hairLength * 0.07,
        };
        bot.cp2 = sp.right.p2.cp2;

        right.cp1 = {
            x: bot.x + 12,
            y: bot.y
        };
        right.cp2 = {
            x: right.x + 5,
            y: top.y
        };

        // shade the fork
        fork.y += 1.5;
        fork.x += 0.7;
        fork.cp1 = simpleQuadratic(fork, forkIn, 0.5, -7);
        fork.cp2 = null;

        return [
            right,
            mid,
            shadingFork,
            bot,
            right,
            breakPoint,
            bot,
            fork,
            forkIn,
            fork,
            extractPoint(bot),
            endPoint
        ];
    }
}


export class HimeCurlMedium extends HairPart {
    constructor(...data) {
        super(Hair.hairMedium, {
            shadingParts: [HimeCurlShading],
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength;
        const {left, top, right, out, bot, fork, forkIn, forkTop} = calcHimeCurl(ex, hl);
        ex.hair = ex.hair || {};
        ex.hair.mid = {
            left,
            top,
            right,
            out,
            bot,
            fork,
            forkIn,
            forkTop
        };

        ctx.beginPath();
        drawPoints(ctx, left, top, right, out, bot, fork, forkIn, forkTop);
        ctx.fill();
        ctx.stroke();
    }
}


function calcHimeCurl(ex, hl) {
    let {right, top} = calcHimeCut(ex, clamp(hl, 0, 53));
    top.y += hl * 0.01;
    top.x -= seamWidth;
    right = adjust(right, hl * 0.005, 0);
    right.cp1.y += hl * 0.01;
    top.cp1.y += hl * 0.01;
    top.cp2.x -= hl * 0.005;
    const left = reflect(right);
    right.x += hl * 0.012;

    const bot = {
        x: right.x,
        y: right.y - hl * 0.8
    };

    const out = {
        x: right.x - hl * 0.01,
        y: right.y * 0.7 + bot.y * 0.3
    };

    // right side down to the tip
    out.cp1 = {
        x: right.x + hl * 0.01,
        y: right.y - hl * 0.15
    };
    out.cp2 = {
        x: out.x,
        y: out.y + hl * 0.1
    };

    // up to tip
    bot.cp1 = {
        x: out.x,
        y: out.y - hl * 0.07
    };
    bot.cp2 = {
        x: bot.x + hl * 0.1,
        y: bot.y + hl * 0.1
    };

    // fork at the tip of the curls
    const fork = {
        x: bot.x - hl * 0.03,
        y: bot.y + hl * 0.2
    };
    const forkIn = {
        x: fork.x - hl * 0.02,
        y: fork.y - hl * 0.1,
    };

    // go back up to neck
    const forkTop = extractPoint(ex.neck.top);
    forkTop.y -= 2;
    // curl up to neck
    forkTop.cp1 = {
        x: forkIn.x - hl * 0.18,
        y: forkIn.y + hl * 0.23
    };
    forkTop.cp2 = {
        x: forkTop.x,
        y: forkTop.y - hl * 0.23
    };

    // fork curls
    fork.cp1 = {
        x: bot.x + hl * 0.02,
        y: bot.y + hl * 0.07
    };
    fork.cp2 = {
        x: fork.x - hl * 0.02,
        y: fork.y - hl * 0.07
    };

    // fork out
    forkIn.cp1 = simpleQuadratic(fork, forkIn, 0.3, -hl * 0.02);
    return {
        left,
        top,
        right,
        out,
        bot,
        fork,
        forkIn,
        forkTop
    };
}

export class HimeCurlSide extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, {
            loc       : "+front hair",
            aboveParts: ["hairParts hair"],
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength;
        const curl = hl - 35;
        if (curl < 0) {
            return;
        }
        let {left, right} = calcHimeCut(ex, clamp(hl, 0, 53));
        right = adjust(right, 0.4, 1.5);
        right.cp1.y += hl * 0.01;

        let sp = splitCurve(0.1, right, left);
        const inner = sp.left.p2;

        let bot;
        if (curl < 15) {
            bot = {
                x: right.x - 3,
                y: right.y - curl
            };
        } else {
            sp = splitCurve(clamp(-0.3 + curl * 0.015, -0.3, 0.8), ex.jaw, ex.chin.out);
            bot = adjust(sp.left.p2, 0.2, -0.2);
        }
        bot.cp1 = {
            x: right.x,
            y: right.y - curl * 0.1
        };
        bot.cp2 = {
            x: bot.x + curl * 0.13,
            y: bot.y + curl * 0.06
        };
        inner.cp1 = simpleQuadratic(bot, inner, 0.45, -curl * 0.07);

        // first draw the bang (don't want to stroke the top as well so we'll fill later
        ctx.save();
        ctx.scale(-1, 1);
        ctx.beginPath();
        drawPoints(ctx, right, bot, inner);
        ctx.fill();
        ctx.stroke();
        ctx.restore();
    }
}


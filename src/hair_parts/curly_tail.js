import {Hair, HairPart} from "./hair_part";
import {
    clamp,
    splitCurve,
    drawPoints,
    adjust,
    scale,
    drawSpecificCurl,
    extractPoint
} from "drawpoint";

export class CurlyTailFront extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = clamp(this.hairLength, 0, 70);
        // right side
        {
            let tip = {
                x: ex.skull.x + hl * 0.02,
                y: ex.skull.y + 3
            };
            let sp = splitCurve(clamp(0.75 + hl * 0.0045, 0, 1.1), tip, ex.skull.side);
            let bot = sp.left.p2;
            bot.cp1.x += 3 + hl * 0.01;
            bot.cp1.y += 3 + hl * 0.01;
            bot.cp2.x = bot.x;
            tip.cp1 = {
                x: bot.x - 4 + hl * 0.03,
                y: bot.y + 3 + hl * 0.1
            };
            tip.cp2 = {
                x: tip.x,
                y: tip.y - this.faceLength / 15 - hl * 0.05
            };
            ctx.beginPath();
            drawPoints(ctx, tip, bot, tip);
            ctx.fill();
            ctx.stroke();
        }
        {
            // left side

            const {tip, out, bot} = calcCurlyTop(ex, hl);
            // draw front parting
            out.cp2.y -= hl * 0.03;
            out.cp2.x += hl * 0.01;
            out.cp1.x += hl * 0.05;
            out.cp1.y += 4 + hl * 0.05;

            bot.cp1 = {
                x: out.x,
                y: clamp(out.y - 5 - hl * 0.05, bot.y, out.y)
            };
            bot.cp2 = {
                x: clamp(bot.x + hl * 0.07, 0, out.x),
                y: bot.y + 2 + hl * 0.07
            };

            tip.cp1 = scale(bot, 3, bot.cp2);
            tip.cp2 = {
                x: tip.x - 7,
                y: tip.y - this.faceLength / 20
            };
            if (hl < 10) {
                tip.cp2.y -= (10 - hl) * 0.5;
            }
            let sp = splitCurve(0.5, bot, tip);
            const [curlLeft, curlBot, curlTop] = drawSpecificCurl(
                {
                    x         : sp.left.p2.x,
                    y         : sp.left.p2.y,
                    deflection: -hl * 0.03
                },
                {
                    x: sp.left.p2.x,
                    y: sp.left.p2.y - hl * 0.08
                },
                {
                    x         : tip.x,
                    y         : tip.y,
                    deflection: -5
                }
            );
            curlTop.cp2 = sp.right.p2.cp2;

            ctx.save();
            ctx.scale(-1, 1);
            ctx.beginPath();
            drawPoints(ctx, tip, out, bot, curlLeft, curlBot, curlTop);
            ctx.fill();
            ctx.stroke();
            ctx.restore();
        }
    }
}


export class CurlyTailMedium extends HairPart {
    constructor(...data) {
        super(Hair.hairMedium, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength;

        const tail = hl - 40;
        if (tail > 0) {

            const {out, bot} = calcCurlyTop(ex, clamp(hl, 0, 70));

            const left = {
                x  : bot.x + tail * 0.04,
                y  : bot.y - tail * 0.35,
                cp1: {
                    x: bot.x,
                    y: bot.y - tail * 0.2
                }
            };
            left.cp2 = {
                x: left.x,
                y: left.y + tail * 0.15
            };

            const tip = {
                x  : bot.x - tail * 0.1,
                y  : bot.y - tail,
                cp1: {
                    x: left.x,
                    y: left.y - tail * 0.3
                },
            };
            tip.cp2 = {
                x: tip.x - tail * 0.05,
                y: tip.y + tail * 0.4
            };
            const right = extractPoint(ex.neck.top);
            right.cp1 = {
                x: tip.x - tail * 0.3,
                y: tip.y + tail * 0.2
            };
            right.cp2 = {
                x: right.x,
                y: right.y - tail * 0.05
            };

            ctx.save();
            ctx.scale(-1, 1);
            ctx.beginPath();
            drawPoints(ctx, bot, left, tip, right, out);
            ctx.fill();
            ctx.stroke();
            ctx.restore();
        }
    }
}


function calcCurlyTop(ex, hl) {
    const tip = {
        x: ex.skull.x - hl * 0.08,
        y: ex.skull.y + 3
    };
    const out = adjust(ex.skull.side, 1 + hl * 0.03, clamp(2 - hl * 0.05, -3, 2));
    const bot = {
        x: out.x - clamp(hl * 0.05, 0, 10),
        y: out.y - clamp(hl * 0.25, 0, 25)
    };
    if (hl > 50) {
        bot.x += (hl - 50) * 0.06;
    }

    return {
        tip,
        out,
        bot
    };
}

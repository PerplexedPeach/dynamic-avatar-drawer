import {HairPart, Hair} from "./hair_part";
import {
    adjust,
    splitCurve,
    simpleQuadratic,
    drawPoints,
    extractPoint,
    scale,
    continueCurve,
    rotatePoints,
    clamp,
} from "drawpoint";

export class SideHighTailFront extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = clamp(this.hairLength, 0, 70);
        {
            let tip = {
                x: ex.skull.x + hl * 0.02,
                y: ex.skull.y + 3
            };
            let sp = splitCurve(clamp(0.75 + hl * 0.0045, 0.9, 1.1), tip, ex.skull.side);
            let bot = sp.left.p2;
            bot.cp1.x += 3 + hl * 0.01;
            bot.cp1.y += 3 + hl * 0.01;
            bot.cp2.x = bot.x;
            tip.cp1 = {
                x: bot.x - 4 + hl * 0.03,
                y: bot.y + 3 + hl * 0.1
            };
            tip.cp2 = {
                x: tip.x,
                y: tip.y - this.faceLength / 15 - hl * 0.05
            };
            ctx.beginPath();
            drawPoints(ctx, tip, bot, tip);
            ctx.fill();
            ctx.stroke();
        }
        {

            const {tip, out, bot} = calcSideHighTailTop(ex, hl);
            out.cp2.y -= hl * 0.03;
            out.cp2.x += hl * 0.01;
            out.cp1.x += hl * 0.05;
            out.cp1.y += 4 + hl * 0.05;

            bot.cp1 = {
                x: out.x,
                y: clamp(out.y - 5 - hl * 0.05, bot.y, out.y)
            };
            bot.cp2 = {
                x: clamp(bot.x + hl * 0.07, 0, out.x),
                y: bot.y + 2 + hl * 0.07
            };

            tip.cp1 = scale(bot, 3, bot.cp2);
            tip.cp2 = {
                x: tip.x - 5,
                y: tip.y - this.faceLength / 15 - hl * 0.05
            };
            if (hl < 10) {
                tip.cp2.y -= (10 - hl) * 0.5;
            }

            ctx.save();
            ctx.scale(-1, 1);
            ctx.beginPath();
            drawPoints(ctx, tip, out, bot, tip);
            ctx.fill();
            ctx.stroke();
            ctx.restore();
        }
    }
}


export class SideHighTailTail extends HairPart {
    constructor(...data) {
        super(Hair.hairMedium, ...data);
    }

    renderHairPoints(ctx, ex, ignore, extraColors) {
        const hl = this.hairLength;
        const tail = this.hairLength - 50;
        if (tail < 0) {
            return;
        }

        const {tip} = calcSideHighTailTop(ex, clamp(hl, 0, 70));
        tip.x *= -1;
        tip.y -= tail * 0.01;

        const top = {
            x: tip.x,
            y: tip.y + tail * 0.12
        };
        const left = {
            x: tip.x - tail * 0.09,
            y: tip.y
        };
        const right = {
            x: tip.x + tail * 0.09,
            y: tip.y
        };

        top.cp1 = {
            x: left.x + hl * 0.02,
            y: left.y + hl * 0.02
        };
        top.cp2 = {
            x: top.x - tail * 0.1,
            y: top.y - tail * 0.02
        };
        right.cp1 = {
            x: top.x + tail * 0.1,
            y: top.y - tail * 0.02
        };
        right.cp2 = {
            x: right.x - hl * 0.02,
            y: right.y + hl * 0.02,
        };

        rotatePoints(tip, -0.24, top, left, right);

        // right tail
        {
            const back = {};
            back.out = {
                x: top.x + tail * 0.2,
                y: top.y - tail * 0.8
            };
            back.outbot = {
                x: top.x + tail * 0.18,
                y: back.out.y - tail * 0.8
            };
            back.in = extractPoint(ex.neck.top);

            back.out.cp1 = {
                x: top.x + tail * 0.3,
                y: top.y - tail * 0.1
            };
            back.out.cp2 = {
                x: back.out.x - tail * 0.1,
                y: back.out.y + tail * 0.3,
            };
            back.outbot.cp1 = continueCurve(top, back.out);
            back.outbot.cp2 = {
                x: back.outbot.x + tail * 0.2,
                y: back.outbot.y + tail * 0.05
            };
            back.in.cp1 = {
                x: back.outbot.x - tail * 0.05,
                y: back.outbot.y + tail * 0.4
            };
            back.in.cp2 = {
                x: back.in.x - tail * 0.1,
                y: back.in.y - tail * 0.5
            };

            ctx.beginPath();
            drawPoints(ctx, top, back.out, back.outbot, back.in);
            ctx.fill();
            ctx.stroke();
        }

        // left tail
        const leftTail = this.hairLength - 90;
        if (leftTail > 0) {
            const back = {};
            back.top = extractPoint(ex.neck.top);
            back.tip = {
                x: back.top.x + leftTail * 0.6,
                y: back.top.y - leftTail * 3.5
            };

            back.tip.cp1 = {
                x: back.top.x + leftTail * 1.5,
                y: back.top.y - leftTail * 2
            };
            back.tip.cp2 = {
                x: back.tip.x - leftTail * 0.5,
                y: back.tip.y + leftTail * 1.2
            };

            back.right = extractPoint(ex.neck.cusp);
            back.right.x += 0.5;
            back.right.cp1 = {
                x: back.tip.x - leftTail * 0.6,
                y: back.tip.y + leftTail
            };
            back.right.cp2 = {
                x: back.right.x + leftTail * 0.3,
                y: back.right.y - leftTail * 0.8
            };

            ctx.save();
            ctx.scale(-1, 1);
            ctx.beginPath();
            drawPoints(ctx, back.top, back.tip, back.right);
            ctx.fill();
            ctx.stroke();
            ctx.restore();
        }

        // top knot
        ctx.beginPath();
        drawPoints(ctx, left, top, right);
        ctx.fill();
        ctx.stroke();

        // hair accessory
        {
            let sp = splitCurve(0.3, left, top);
            const leftBand = sp.left.p2;
            sp = splitCurve(1 - 0.3, top, right);
            const rightBand = sp.left.p2;
            rightBand.cp1 = simpleQuadratic(leftBand, rightBand, 0.5, tail * 0.03);

            ctx.save();
            ctx.strokeStyle = extraColors.hairAccessoryColor;
            ctx.beginPath();
            drawPoints(ctx, leftBand, rightBand);
            ctx.stroke();
            ctx.restore();
        }
    }

}


function calcSideHighTailTop(ex, hl) {
    const tip = {
        x: ex.skull.x - hl * 0.08,
        y: ex.skull.y + 3
    };
    const out = adjust(ex.skull.side, hl * 0.03, clamp(2 - hl * 0.05, -3, 2));
    const bot = {
        x: out.x - clamp(hl * 0.05, 0, 10),
        y: out.y - clamp(hl * 0.25, 0, 25)
    };
    if (hl > 50) {
        bot.x += (hl - 50) * 0.06;
    }

    return {
        tip,
        out,
        bot
    };
}


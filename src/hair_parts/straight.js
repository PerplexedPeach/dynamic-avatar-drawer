import {HairPart, Hair} from "./hair_part";
import {
    adjust,
    splitCurve,
    simpleQuadratic,
    drawPoints,
    breakPoint,
    endPoint,
    extractPoint,
    continueCurve,
    none,
} from "drawpoint";
import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {averagePoint, adjustColor, extractHex} from "../util/utility";

class StraightFrontShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "front hair",
            layer: Layer.HAIR,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const {top, topBot} = calcStraight.call(this, ex);
        topBot.cp1 = simpleQuadratic(top, topBot, 0.5, 2.5);
        topBot.cp2 = null;
        top.cp1 = simpleQuadratic(topBot, top, 0.5, -.5);

        return [top, topBot, top];
    }
}


class StraightFrontSideShading extends ShadingPart {
    constructor(...data) {
        super({
            loc    : "+front hair",
            layer  : Layer.HAIR,
            reflect: true,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const {top, side, topBot} = calcStraight.call(this, ex);

        let sp = splitCurve(0.8, top, side);

        const points = [];
        {
            const p = {
                x: 1,
                y: top.y * 0.8 + topBot.y * 0.2
            };

            let pEnd = adjust(extractPoint(sp.left.p2), -0.3, 0);
            const curl = 7;
            const thickness = 0.4;
            const bias = 0.35;
            pEnd.cp1 = simpleQuadratic(p, pEnd, bias, curl);
            p.cp1 = simpleQuadratic(pEnd, p, 1 - bias, -curl + thickness);
            points.push(p, pEnd, p);
        }
        {
            const p = {
                x: -1,
                y: top.y * 0.4 + topBot.y * 0.6
            };

            let pEnd = adjust(extractPoint(sp.left.p2), -0.5, -1.5);
            const curl = 7;
            const thickness = 0.4;
            const bias = 0.55;
            pEnd.cp1 = simpleQuadratic(p, pEnd, bias, curl);
            p.cp1 = simpleQuadratic(pEnd, p, 1 - bias, -curl + thickness);
            points.push(breakPoint, p, pEnd, p);
        }
        points.push(endPoint);
        // return [];
        return points;
    }
}


export class StraightFrontShine extends HairPart {
    constructor(...data) {
        super({
            loc       : "+ears hair",
            layer     : Layer.HAIR,
            aboveParts: ["hairParts hair"],
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        let {top, topBot} = calcStraight.call(this, ex);

        top = adjust(top, 2.5, -1);
        topBot = adjust(topBot, 2.5, 1);
        const mid = adjust(averagePoint(top, topBot, 0.5), 0.5, 0);

        topBot.cp1 = {
            x: top.x - 1,
            y: top.y
        };
        topBot.cp2 = {
            x: topBot.x - 1,
            y: topBot.y + 0.5
        };

        mid.cp1 = {
            x: topBot.x + 1.5,
            y: topBot.y
        };
        mid.cp2 = {
            x: mid.x - 0.2,
            y: mid.y - 0.3
        };

        top.cp1 = continueCurve(topBot, mid);
        top.cp2 = {
            x: top.x + 2,
            y: top.y
        };

        ctx.fillStyle = adjustColor(extractHex(ctx.fillStyle),
            {
                l: 5,
                s: -5,
            });
        ctx.scale(-1, 1);
        ctx.beginPath();
        drawPoints(ctx, top, topBot, mid, top);
        ctx.fill();
        ctx.scale(-1, 1);
    }
}


export class StraightFront extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, {
            reflect     : true,
            shadingParts: [StraightFrontShading, StraightFrontSideShading]
        }, ...data);
    }

    stroke() {
        return none;
    }

    renderHairPoints(ctx, ex) {
        const {top, side, topBot} = calcStraight.call(this,
            ex);

        ctx.beginPath();
        drawPoints(ctx,
            top,
            side,
            topBot,
            top);
        ctx.fill();
    }
}


class StraightSideBangShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+front hair",
            layer: Layer.BELOW_HAIR,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const hl = this.hairLength - 12;
        if (hl < 0) {
            return;
        }

        const {sideBot, innerBot} = calcStraightSide.call(this, ex, hl);
        sideBot.y += hl * 0.1;
        const bot = {
            x: innerBot.x + 6,
            y: innerBot.y + 10
        };
        const top = {
            x: sideBot.x + 3,
            y: sideBot.y + 2
        };
        return [sideBot, innerBot, bot, top, sideBot];
    }
}


export class StraightSideBang extends HairPart {
    constructor(...data) {
        super(Hair.hairAboveEars, {
            shadingParts: [StraightSideBangShading],
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength - 12;
        if (hl < 0) {
            return;
        }

        const {top, side, sideBot, innerBot, innerTop} = calcStraightSide.call(this, ex, hl);
        sideBot.cp1 = simpleQuadratic(side, sideBot, 0.5, 0.5 + hl * 0.005);
        innerTop.cp1 = simpleQuadratic(innerBot, innerTop, 0.5, -0.5);

        ctx.beginPath();
        drawPoints(ctx, top, side, sideBot, innerBot, innerTop);
        ctx.fill();
    }
}


function calcStraightSide(ex, hl) {
    const {top, side} = calcStraight.call(this, ex);

    side.x += hl * 0.02;
    const sideBot = {
        x: side.x - hl * 0.03,
        y: side.y - hl * 0.65
    };
    const innerBot = {
        x: sideBot.x - hl * 0.02,
        y: sideBot.y - hl * 0.05
    };

    const sp = splitCurve(0.6, top, side);
    const innerTop = extractPoint(sp.left.p2);

    return {
        top,
        side,
        sideBot,
        innerBot,
        innerTop
    };
}

export class StraightBack extends HairPart {
    constructor(...data) {
        super(Hair.hairBack, {
            reflect: true
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        const hl = this.hairLength - 12;
        if (hl < 0) {
            return;
        }

        const {top, side} = calcStraight.call(this, ex, hl + 12);

        side.x += hl * 0.02;
        const sideBot = {
            x: side.x + hl * 0.05,
            y: side.y - hl
        };
        const inner = {
            x: 0,
            y: sideBot.y - hl * 0.05
        };

        ctx.beginPath();
        drawPoints(ctx, top, side, sideBot, inner);
        ctx.fill();
        ctx.stroke();
    }
}


function calcStraight(ex) {
    const top = extractPoint(ex.skull);
    top.y += 1;

    let sp = splitCurve(0.5, ex.skull.side, ex.skull.bot);
    const side = sp.left.p2;
    side.cp1 = {
        x: top.x * 0.5 + side.x * 0.5,
        y: top.y + 2
    };
    side.cp2 = {
        x: side.x + 3.5,
        y: top.y * 0.7 + side.y * 0.3
    };

    const topBot = {
        x: top.x,
        y: top.y - this.faceLength * 0.04,
    };
    topBot.cp1 = {
        x: side.x,
        y: side.y * 0.4 + top.y * 0.6
    };
    topBot.cp2 = {
        x: topBot.x,
        y: topBot.y - this.faceLength * 0.02
    };

    top.cp1 = simpleQuadratic(topBot, top, 0.5, 1);

    return {
        top,
        side,
        topBot
    };
}


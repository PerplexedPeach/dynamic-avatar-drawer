import {Hair, HairPart} from "./hair_part";
import {adjust, adjustPoints, continueCurve, drawPoints, scale, clamp} from "drawpoint";
import {Layer} from "../util/canvas";
import {Part} from "../parts/part";
import {ShadingPart} from "../draw/shading_part";

export class AsianBunBack extends HairPart {

    constructor(...data) {
        super(Hair.hairBack, ...data);
    }

    // hair has special draw methods
    renderHairPoints(ctx, ex, ignore, extraColors) {
        let hl = this.hairLength;

        function drawBackBuns() {
            const center = adjust(ex.neck.top, 1, -1);
            ctx.beginPath();
            ctx.ellipse(center.x,
                center.y,
                hl * 0.08,
                hl * 0.06,
                0.25 * Math.PI,
                0,
                2 * Math.PI);
            ctx.fill();
            ctx.stroke();
        }

        drawBackBuns();

        // ctx.save();
        ctx.scale(-1, 1);
        drawBackBuns();
        ctx.scale(-1, 1);
        // ctx.restore();

        hl = this.hairLength;
        // bun
        if (hl > 10) {
            // chopsticks?
            const center = {
                x: 0,
                y: ex.skull.y + 3
            };
            const back = {
                x: center.x + hl * 0.13,
                y: center.y + hl * 0.05
            };
            const front = {
                x: center.x + hl * 0.1,
                y: center.y + hl * 0.1
            };

            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = extraColors.hairAccessoryColor;
            drawPoints(ctx, center, back);
            ctx.stroke();
            ctx.restore();

            ctx.beginPath();
            ctx.ellipse(center.x, center.y, hl * 0.1, hl * 0.06, 0, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();

            ctx.beginPath();
            ctx.save();
            ctx.strokeStyle = extraColors.hairAccessoryColor;
            drawPoints(ctx, adjust(center, 1 + hl * 0.02, 1 + hl * 0.02), front);
            ctx.stroke();
            ctx.restore();
        }

    }
}


class LargeSideBangShading extends ShadingPart {
    constructor(...data) {
        super({
            loc       : "+head",
            layer     : Layer.HAIR,
            forcedSide: Part.LEFT,
        }, ...data);
    }

    calcDrawPoints(ex, ignore, ignore1, ignore2, avatar) {
        const hl = clamp(avatar.dim.hairLength, 0, 50);
        let {tip, bot} = calcLargeSideBang.call(this, ex, hl);
        const [a, b] = adjustPoints(-hl * 0.03, -hl * 0.04, tip.cp1, tip.cp2);
        const [c, d] = adjustPoints(hl * 0.06, hl * 0.05, tip.cp1, tip.cp2);

        bot.cp1 = d;
        bot.cp2 = c;

        tip.cp1 = a;
        tip.cp2 = b;

        return [bot, tip, bot];
    }
}


export class AsianBunFront extends HairPart {
    constructor(...data) {
        super(Hair.hairFront, {
            shadingParts: [LargeSideBangShading],
        }, ...data);
    }

    renderHairPoints(ctx, ex) {
        // right side
        {
            const hl = clamp(this.hairLength, 0, 70);
            let tip = {
                x: ex.skull.x - 2,
                y: ex.skull.y + 3
            };
            let bot = adjust(ex.skull.side, 1, 0);
            bot.cp1.x += 3 + hl * 0.01;
            bot.cp1.y += 3 + hl * 0.01;
            bot.cp2.x = bot.x;
            tip.cp1 = {
                x: bot.x - 4,
                y: bot.y + 3
            };
            tip.cp2 = {
                x: tip.x,
                y: tip.y - this.faceLength / 15
            };
            ctx.beginPath();
            drawPoints(ctx, tip, bot, tip);
            ctx.fill();
            ctx.stroke();
        }
        // left side
        {
            const hl = clamp(this.hairLength, 0, 50);
            ctx.save();

            const {tip, out, bot} = calcLargeSideBang.call(this, ex, hl);

            ctx.scale(-1, 1);
            ctx.beginPath();
            drawPoints(ctx, tip, out, bot, tip);
            ctx.fill();
            ctx.stroke();
            ctx.restore();
        }


    }
}


function calcLargeSideBang(ex, hl) {
    // draw front parting
    let tip = {
        x: ex.skull.x - 2,
        y: ex.skull.y + 3
    };
    let out = adjust(ex.skull.side, 1 + hl * 0.03, clamp(2 - hl * 0.05, -3, 2));
    // out.cp1.x = out.x;
    out.cp2.x = out.x;
    out.cp2.y -= 2;
    out.cp1.x += hl * 0.05;
    out.cp1.y += 2 + hl * 0.04;

    let bot = {
        x: out.x - clamp(hl * 0.1, 0, 10),
        y: out.y - clamp(hl, 0, 20)
    };
    bot.x = clamp(bot.x, 13, out.x);
    bot.cp2 = {
        x: clamp(bot.x + hl * 0.08, 0, Math.POSITIVE_INFINITY),
        y: bot.y + 2 + hl * 0.07
    };
    if (hl > 5) {
        bot.x -= (hl - 5) * 0.1;
        if (hl > 20) {
            const extraLength = hl - 20;
            bot = adjust(bot, -extraLength * 0.02, -extraLength * 0.15);
        }
    }
    bot.cp1 = continueCurve(tip, out);

    tip.cp1 = scale(bot, 2, bot.cp2);
    tip.cp1.y = out.y;
    tip.cp2 = {
        x: tip.x - 8,
        y: tip.y - this.faceLength / 15
    };
    if (hl < 10) {
        tip.cp2.y -= (10 - hl) * 0.5;
    }
    return {
        tip,
        out,
        bot
    };
}


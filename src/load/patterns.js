import {addPattern, addDebugPattern} from "../util/pattern";

export default function loadPatterns() {
    addPattern("sequins",
        "http://us.123rf.com/450wm/agonda/agonda1503/agonda150300035/37619569-melange-wool-knitting-pattern-seamless-background.jpg?ver=6");

    addPattern("soft brown fur",
        "https://cdn.tobi.com/swatch_images/black-multi-cant-be-tamed-fur-vest-swatch.jpg");
    addPattern("black leather",
        "http://www.textures123.com/free-texture/leather/leather-texture05.jpg");

    addPattern("yellow stripes",
        "http://images.naldzgraphics.net/2014/07/16-yellow-stripe-texture.jpg");

    addPattern("purple squares",
        "http://images.naldzgraphics.net/2014/07/17-square-fabric-textures.jpg");

    addPattern("zebra",
        "http://www.deluxevectors.com/images/sample/seamless-pattern-zebra.jpg");

    addPattern("chain mail 1",
        "http://st.depositphotos.com/1268230/2584/i/950/depositphotos_25842101-Seamless-computer-generated-metal-chain.jpg");

    addPattern("fishnet",
        "http://i.imgur.com/fU41Daz.png");

    addPattern("lace",
        "http://i.imgur.com/RZc72OK.gif");

    addPattern("red plaid",
        "http://www.creattor.com/files/37/1681/plaid-fabrics-textures-screenshots-1.jpg");

    addDebugPattern("green camouflage",
        "res/green camouflage.png",
        "http://i.imgur.com/cSQUcjj.png");

    addDebugPattern("camouflage",
        "res/camouflage.jpg",
        "http://images.naldzgraphics.net/2014/07/15-camo-fabric-texture.jpg");

    addDebugPattern("kimono flowers",
        "res/kimono_flowers.png",
        "http://oksancia.com/wp-content/uploads/2011/03/beautiful_garden_seamless_pattern_wm.png");

    addDebugPattern("bandages",
        "res/bandages.png",
        "http://i.imgur.com/Om7lYpO.png");
}

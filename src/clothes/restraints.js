import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    drawPoints,
    extractPoint,
    splitCurve, simpleQuadratic, adjust, interpolateCurve, breakPoint
} from "drawpoint/dist-esm";
import {BraceletPart} from "./gloves";
import {Part} from "../parts/part";
import {BandedAnkletPart, connectEndPoints, dist} from "..";
import {Location} from "..";

export class RestraintChainPart extends ClothingPart {
    constructor(...data) {
        super({
                chainWidth : 2,
                chainStroke: "#6d6d6d",
                chainDash  : [3, 7],
            },
            ...data);
    }
}

export class BondageRopePart extends RestraintChainPart {
    constructor(...data) {
        super({
                chainWidth : 2.5,
                chainStroke: "#d92f43",
                chainDash  : [1, 6],
            },
            ...data);
    }
}

export class ChestBondagePart extends BondageRopePart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : `+${Location.CHEST}`,
            aboveParts         : [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            reflect            : true,
            aboveSameLayerParts: ["chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {top, center, bot, sideTop, sideBot} = calcTopBondage.call(this, ex);

        ctx.lineWidth = this.chainWidth;
        ctx.strokeStyle = this.chainStroke;

        ctx.setLineDash(this.chainDash);
        ctx.beginPath();
        drawPoints(ctx, top, center, bot, breakPoint, center, sideTop, breakPoint, bot, sideBot);
        ctx.stroke();
    }
}

export class WaistBondagePart extends BondageRopePart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : `+${Location.TORSO}`,
            aboveParts         : [`parts ${Location.TORSO}`, `decorativeParts ${Location.TORSO}`],
            reflect            : true,
            belowSameLayerParts: ["torso", "groin"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {bot: top} = calcTopBondage.call(this, ex);

        const mid = {x: 4, y: ex.waist.y - 2};
        mid.cp1 = simpleQuadratic(top, mid, 0.5, -dist(top, mid) * 0.1);

        const side = extractPoint(ex.waist);
        side.cp1 = simpleQuadratic(mid, side, 0.5, -1);

        const midBot = {x: 0, y: ex.pelvis.y + 10};
        midBot.cp1 = simpleQuadratic(mid, midBot, 0.5, -dist(mid, midBot) * 0.1);

        const bot = adjust(extractPoint(ex.groin), 0, 2);

        ctx.lineWidth = this.chainWidth;
        ctx.strokeStyle = this.chainStroke;

        ctx.setLineDash(this.chainDash);
        ctx.beginPath();
        drawPoints(ctx, top, mid, midBot, breakPoint, mid, side, breakPoint, midBot, bot);
        ctx.stroke();
    }
}



export class ChestPentagramBondagePart extends BondageRopePart { //TODO - doesn't look as good as intended
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : `+${Location.CHEST}`,
            aboveParts         : [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            reflect            : true,
            aboveSameLayerParts: ["chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const {top, center, bot, sideTop, sideBot, upperCenter, upperBot} = calcTopPentagramBondage.call(this, ex);
        ctx.lineWidth = this.chainWidth;
        ctx.strokeStyle = this.chainStroke;
		ctx.setLineDash(this.chainDash);
        ctx.beginPath();
        drawPoints(ctx, top,  upperBot, bot, breakPoint, center, sideTop, breakPoint, bot, sideBot, breakPoint, sideTop, upperCenter, breakPoint, top, upperCenter, );
        ctx.stroke();
    }
}


function calcTopPentagramBondage(ex) {
    let sp = splitCurve(0.3, ex.neck.cusp, ex.collarbone);

    const top = adjust(sp.right.p1, 0, -1.5);
    const center = {x: 0, y: ex.armpit.y * 0.7 + ex.neck.cusp.y * 0.3};
    center.cp1 = simpleQuadratic(top, center, 0.5, dist(top, center) * 0.15);

    const bot = {x: 0, y: ex.armpit.y - 11};

    let sideTop = extractPoint(ex.armpit);
    if (ex.breast) {
        sideTop = adjust(extractPoint(ex.breast.top), 1.5, 0.5);
    }
    sideTop.cp1 = simpleQuadratic(center, sideTop, 0.5, 0.3);

    let q = {x: null, y: bot.y + 1};
    const [sideBot] = interpolateCurve(ex.armpit, ex.waist, q);
    sideBot.x -= 0.5;
    sideBot.cp1 = simpleQuadratic(bot, sideBot, 0.5, -1);

	const upperBot = adjust(bot,0,6.66);
	const upperCenter = adjust(extractPoint(center),0,8);
		
    return {top, center, bot, sideTop, sideBot, upperCenter, upperBot};
}

function calcTopBondage(ex) {
    let sp = splitCurve(0.4, ex.neck.cusp, ex.collarbone);

    const top = adjust(sp.right.p1, 0, -1.5);
    const center = {x: 0, y: ex.armpit.y * 0.7 + ex.neck.cusp.y * 0.3};
    center.cp1 = simpleQuadratic(top, center, 0.5, dist(top, center) * 0.15);

    const bot = {x: 0, y: ex.armpit.y - 10};

    let sideTop = extractPoint(ex.armpit);
    if (ex.breast) {
        sideTop = adjust(extractPoint(ex.breast.top), 1.5, 0.5);
    }
    sideTop.cp1 = simpleQuadratic(center, sideTop, 0.5, 2);

    let q = {x: null, y: bot.y + 1};
    const [sideBot] = interpolateCurve(ex.armpit, ex.waist, q);
    sideBot.x -= 0.5;
    sideBot.cp1 = simpleQuadratic(bot, sideBot, 0.5, -1);

    return {top, center, bot, sideTop, sideBot};
}


export class WristRestraintChainPart extends RestraintChainPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : `+${Location.ARM}`,
            aboveParts: [`parts ${Location.ARM}`, `decorativeParts ${Location.ARM}`, `parts ${Location.HAND}`],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(0.15, ex.wrist.in, ex.elbow.in);
        const right = adjust(extractPoint(sp.right.p1), 1, 0);
        const left = {x: -right.x, y: right.y};

        left.cp1 = simpleQuadratic(right, left, 0.5, 5);

        ctx.lineWidth = this.chainWidth;
        ctx.strokeStyle = this.chainStroke;

        ctx.setLineDash(this.chainDash);
        ctx.beginPath();
        drawPoints(ctx, right, left);
        ctx.stroke();
    }
}

export class AnkleRestraintChainPart extends RestraintChainPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : `+${Location.LEG}`,
            aboveParts: [`parts ${Location.LEG}`, `decorativeParts ${Location.LEG}`],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(this.startAlongLeg + this.legCoverage * 0.5, ex.ankle.in, ex.calf.in);
        const right = adjust(extractPoint(sp.right.p1), 1, 0);
        const left = {x: -right.x, y: right.y};

        ctx.lineWidth = this.chainWidth;
        ctx.strokeStyle = this.chainStroke;

        ctx.setLineDash(this.chainDash);
        ctx.beginPath();
        drawPoints(ctx, right, connectEndPoints(right, left));
        ctx.stroke();
    }
}

/**
 * Base Clothing classes
 */
export class Restraints extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.OUTER,
        }, ...data);
    }
}

export class Bondage extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.BASE,
        }, ...data);
    }
}

/**
 * Concrete Clothing classes
 */
export class WristRestraints extends Restraints {
    constructor(...data) {
        super({armCoverage: 0.75}, ...data);
        this.Mods = Object.assign({
            armRotation : -35,
            handRotation: 12
        }, this.Mods);
    }

    fill() {
        return "#1e1e1e";
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: BraceletPart,
            },
            {
                side: Part.RIGHT,
                Part: BraceletPart,
            },
            {
                side: Part.RIGHT,
                Part: WristRestraintChainPart,
            },
        ];
    }
}

export class AnkleRestraints extends Restraints {
    constructor(...data) {
        super({
            startAlongLeg: 0.25,
            legCoverage  : 0.4,
        }, ...data);
    }

    fill() {
        return "#1e1e1e";
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: BandedAnkletPart,
            },
            {
                side: Part.RIGHT,
                Part: BandedAnkletPart,
            },
            {
                side: Part.RIGHT,
                Part: AnkleRestraintChainPart,
            },
        ];
    }
}

export class ChestBondage extends Bondage {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChestBondagePart,
            },
        ];
    }
}

export class ChestPentagramBondage extends Bondage {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChestPentagramBondagePart,
            },
        ];
    }
}

export class WaistBondage extends Bondage {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: WaistBondagePart,
            },
        ];
    }
}

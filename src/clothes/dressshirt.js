import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {connectEndPoints} from "../draw/draw";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {LongSleevePart, calcSweaterBase, calcLongSleeve} from "./sweater";
import {calcJacket} from "./jacket";
import {
    simpleQuadratic,
    reverseDrawPoint,
    drawPoints,
    extractPoint,
    none,
    splitCurve,
    adjust,
    clamp,
    drawCircle,
    breakPoint,
} from "drawpoint";

export function calcDressShirt(ex) {
    const {outBot, outMid, breastBot, breastTip, outTop} = calcJacket.call(
        this,
        ex);
    let {top, cusp, collarbone, bot} = calcSweaterBase.call(this, ex);

    top.x += this.topParted;
    top.y -= this.topParted * 0.4;
    bot.bot.x += this.botParted;
    bot.bot = connectEndPoints(top, bot.bot, (this.topParted + this.botParted) * -0.01);
    outBot.cp1 = simpleQuadratic(bot.bot, outBot, 0.5, -1);

    cusp = reverseDrawPoint(cusp, collarbone);

    return {
        collarbone,
        cusp,
        top,
        bot,
        outBot,
        outMid,
        breastBot,
        breastTip,
        outTop
    };
}

/**
 * ClothingPart drawn classes/components
 */
class DressShirtBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+torso",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            aboveSameLayerParts: ["groin"]
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {
            collarbone,
            cusp,
            top,
            bot,
            outBot,
            outMid,
            breastBot,
            breastTip,
        } = calcDressShirt.call(this, ex);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            cusp,
            top,
            bot.bot,
            outBot,
            outMid,
            breastBot,
            breastTip,
            extractPoint(ex.armpit)
        );
        ctx.fill();
        ctx.clip();

        // lining
        setStrokeAndFill(ctx,
            {
                stroke: this.liningPattern,
                fill  : none
            },
            ex);
        ctx.lineWidth = this.liningWidth;
        ctx.beginPath();
        drawPoints(ctx, top, bot.bot);
        ctx.stroke();

    }
}

class DressShirtCollarPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "+neck",
            reflect   : true,
            aboveParts: ["parts neck", "parts torso", "decorativeParts torso"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {collarbone, cusp, top,} = calcDressShirt.call(this, ex);

        const sp = splitCurve(1 - this.collarCoverage, collarbone, cusp);
        const out = sp.left.p2;

        const bot = adjust(extractPoint(top),
            this.collarWidth + this.topParted * 0.25,
            -this.collarHeight + this.topParted * 0.3);

        // scale to simulate clothing thickness
        setStrokeAndFill(ctx,
            {
                stroke: none,
                fill  : this.collarPattern
            },
            ex);

        ctx.beginPath();
        drawPoints(ctx,
            out,
            cusp,
            top,
            bot,
            out
        );
        ctx.fill();

    }
}

class DressShirtButtonPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+torso",
            reflect            : false,
            forcedNoSideString : true,
            aboveParts         : ["parts torso", "decorativeParts torso"],
            aboveSameLayerParts: ["torso"],
        }, {
            buttonStroke   : "#fff",
            buttonFill     : "#ccc",
            buttonRadius   : 1,
            buttonThickness: 1,
            buttonCoverage : 0.77
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {top, bot} = calcDressShirt.call(this, ex);

        const points = [];
        for (let t = 0.1; t < 1; t += (1.001 - this.buttonCoverage)) {
            const sp = splitCurve(t, top, bot.bot);
            let shiftedX = 0;
            // when the button is undone
            if (sp.left.p2.x > 0.2 || sp.left.p2.x < -0.2) {
                shiftedX = clamp(Math.abs(sp.left.p2.x), 0, this.buttonRadius * 1.5);
            }
            const center = adjust(sp.left.p2, shiftedX, 0);
            points.push(...drawCircle(center, this.buttonRadius), breakPoint);
        }

        // scale to simulate clothing thickness
        setStrokeAndFill(ctx,
            {
                stroke: this.buttonStroke,
                fill  : this.buttonFill,
            },
            ex);
        ctx.lineWidth = this.buttonThickness;

        ctx.beginPath();
        drawPoints(ctx,
            ...points
        );
        ctx.fill();
        ctx.stroke();

    }
}

class DressShirtBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "+chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        // no breast means we cover just the nipples
        if (ex.hasOwnProperty("breast") === false) {
            Clothes.simpleStrokeFill(ctx, ex, this);
            ctx.beginPath();
            ctx.arc(ex.chest.nipples.x, ex.chest.nipples.y, 5, 0, 2 * Math.PI);
            ctx.fill();
            return;
        }

        // clip to base
        let topTop;
        let botBot;
        {
            const {
                collarbone,
                cusp,
                top,
                bot,
                outBot,
            } = calcDressShirt.call(this, ex);
            botBot = bot.bot;
            topTop = top;

            ctx.beginPath();
            drawPoints(ctx,
                collarbone,
                cusp,
                top,
                bot.bot,
                outBot,
                // don't clip outer side
                {
                    x: outBot.x + 50,
                    y: outBot.y
                },
                {
                    x: collarbone.x + 50,
                    y: collarbone.y
                }
            );
            ctx.clip();
        }

        const top = adjust(ex.breast.top, 0, 0);
        const tip = adjust(ex.breast.tip, 0.1, 0);
        const bot = adjust(ex.breast.bot, 0, -0.1);
        const cleavage = adjust(ex.breast.cleavage, 0, -0.1);
        const inner = adjust(ex.breast.in, 0, -0.1);

        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            top,
            tip,
            bot,
            inner,
            cleavage,
            connectEndPoints(cleavage, top)
        );
        ctx.fill();
        ctx.clip();

        // lining
        setStrokeAndFill(ctx,
            {
                stroke: this.liningPattern,
                fill  : none
            },
            ex);
        ctx.lineWidth = this.liningWidth;
        ctx.beginPath();
        drawPoints(ctx, topTop, botBot);
        ctx.stroke();
    }
}


class LongSleeveCuffPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.ARMS,
            loc                : "+arm",
            reflect            : false,
            aboveParts         : ["parts arm", "decorativeParts arm"],
            aboveSameLayerParts: ["arm"],
        }, {
            cuffPattern: "#a4c3ca",
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {
            elbowOut,
            out,
            bot,
            elbow,
        } = calcLongSleeve.call(this, ex);
        let sp = splitCurve(0.7, elbowOut, out);
        const outTop = adjust(sp.right.p1, 1.5, 0);
        out.cp1 = sp.right.p2.cp1;
        out.cp2 = sp.right.p2.cp2;

        sp = splitCurve(0.3, bot, elbow);
        const inTop = sp.left.p2;

        // scale to simulate clothing thickness
        setStrokeAndFill(ctx,
            {
                stroke: none,
                fill  : this.cuffPattern
            },
            ex);

        ctx.beginPath();
        drawPoints(ctx,
            outTop,
            out,
            bot,
            inTop,
            outTop
        );
        ctx.fill();

        // cuff buttons
        {
            const buttonScale = 0.5;
            const points = drawCircle(adjust(outTop, -2, -2),
                this.buttonRadius * buttonScale);

            // scale to simulate clothing thickness
            setStrokeAndFill(ctx,
                {
                    stroke: this.buttonStroke,
                    fill  : this.buttonFill,
                },
                ex);
            ctx.lineWidth = this.buttonThickness * buttonScale;

            ctx.beginPath();
            drawPoints(ctx,
                ...points
            );
            ctx.fill();
            ctx.stroke();
        }
    }
}


/**
 * Base Clothing classes
 */
export class DressShirt extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,

            stomachCoverage: 0.9,
            neckCoverage   : 0,

            liningWidth  : 3.5,
            liningPattern: "#98aaaf",

            collarCoverage: 0.25,
            collarHeight  : 7,
            collarWidth   : 3,
            collarPattern : "#a4c3ca",

            // parting of the top and bottom in shirt in cm
            topParted: 4,
            botParted: 0,

            /**
             * How tightly it clings to the body
             */
            cling: 0.6,
        }, ...data);
    }
}


/**
 * Concrete Clothing classes
 */
export class WomenDressShirt extends DressShirt {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return none;
    }

    fill() {
        return "hsl(198.9,42.2%,82.4%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: DressShirtBasePart
            },
            {
                side: null,
                Part: DressShirtCollarPart
            },
            {
                side: null,
                Part: DressShirtBreastPart
            },
            {
                side: Part.LEFT,
                Part: LongSleevePart
            },
            {
                side: Part.RIGHT,
                Part: LongSleevePart
            },
            {
                side: Part.RIGHT,
                Part: LongSleeveCuffPart
            },
            {
                side: Part.LEFT,
                Part: LongSleeveCuffPart
            },
            {
                side: Part.RIGHT,
                Part: DressShirtButtonPart
            },
        ];
    }
}


export class MenDressShirt extends DressShirt {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return none;
    }

    fill() {
        return "hsl(198.9,42.2%,82.4%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: DressShirtBasePart
            },
            {
                side: null,
                Part: DressShirtCollarPart
            },
            {
                side: null,
                Part: DressShirtBreastPart
            },
            {
                side: Part.LEFT,
                Part: LongSleevePart
            },
            {
                side: Part.RIGHT,
                Part: LongSleevePart
            },
            {
                side: Part.RIGHT,
                Part: LongSleeveCuffPart
            },
            {
                side: Part.LEFT,
                Part: LongSleeveCuffPart
            },
            {
                side: Part.LEFT,
                Part: DressShirtButtonPart
            },
        ];
    }
}


import {Clothes, ClothingPart, Clothing} from "./clothing";
import {shine} from "../draw/shading_part";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {ShoeSidePart} from "./shoes";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    adjust,
    continueCurve,
    scale,
} from "drawpoint";

class HeelPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "feet",
            aboveParts: ["parts feet", "parts leg"],
            belowParts: ["shadingParts feet"],
            // aboveSameLayerParts: ["feet"],
        }, ...data);
    }
}


class HeelBaseShine extends HeelPart {
    constructor(...data) {
        super({
            layer: Layer.ARMS,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        // fan shaped shine
        const {bot, outBot, out} = calcHeels.call(this, ex);
        bot.y += 3;
        bot.x += 1;

        const left = adjust(outBot, -1, 0.5);
        const top = adjust(out, -0.5, -4.8);

        left.cp1 = simpleQuadratic(bot, left, 0.7, -1);
        top.cp1 = simpleQuadratic(left, top, 0.5, -0.5);
        bot.cp1 = simpleQuadratic(top, bot, 0.3, 1);

        ctx.fillStyle = shine;
        ctx.beginPath();
        drawPoints(ctx, bot, left, top, bot);
        ctx.fill();
    }
}


class HeelBasePart extends HeelPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {

        const toe = ex.toe;

        toe.toebox = {
            x: toe.center.x,
            y: toe.center.y
        };

        const {out, outBot, bot, inBot, inTop, tongue} = calcHeels.call(this, ex);

        bot.cp1 = simpleQuadratic(outBot, bot, 0.6, 2);
        inBot.cp1 = simpleQuadratic(bot, inBot, 0.6, 2);
        tongue.cp1 = scale(inTop, 0.5, inBot);
        tongue.cp2 = {
            x: tongue.x - 4,
            y: tongue.y
        };
        out.cp1 = continueCurve(inTop, tongue, 1);
        out.cp2 = scale(out, 0.5, outBot);

        outBot.cp1 = simpleQuadratic(out, outBot, 0.8, 0.6);
        inTop.cp1 = simpleQuadratic(inBot, inTop, 0.1, 1);


        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, out, outBot, bot, inBot, inTop, tongue, out);
        ctx.fill();


        // back of the heels
        const backTop = extractPoint(ex.ankle.in);
        let sp = splitCurve(0.6, ex.toe.in, ex.ankle.inbot);
        const backBot = sp.left.p2;
        backBot.cp1 = simpleQuadratic(backTop, backBot, 0.2, -3.3);
        backTop.cp1 = simpleQuadratic(backBot, backTop, 0.7, 1.4);

        ctx.beginPath();
        drawPoints(ctx, backTop, backBot, backTop);
        ctx.fill();

    }
}


class HeelStrapPart extends HeelPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {

        const left = extractPoint(ex.ankle.out);
        const right = extractPoint(ex.ankle.in);
        right.cp1 = simpleQuadratic(left, right, 0.5, 2);


        setStrokeAndFill(ctx,
            {
                fill  : this.fill,
                stroke: this.fill
            },
            ex);
        ctx.lineWidth = this.strapWidth;
        ctx.beginPath();
        drawPoints(ctx, left, right);
        ctx.stroke();

    }
}


export function calcHeels(ex) {
    const toe = ex.toe;
    // draw from outside in
    let sp = splitCurve(0.7, ex.ankle.outbot, toe.out);
    const out = sp.left.p2;

    const outBot = {
        x: toe.out.x + 2,
        y: toe.in.y + 2,
    };

    const bot = {
        x: toe.out.x * 0.45 + toe.in.x * 0.55,
        y: toe.in.y - this.heelPointiness
    };

    const inBot = {
        x: toe.in.x - 2.5,
        y: toe.in.y + 2
    };

    sp = splitCurve(0.6, toe.in, ex.ankle.inbot);
    const inTop = sp.left.p2;

    const tongue = {
        x: bot.x,
        y: toe.center.y - 2 + this.toeCoverage * 1.8
    };

    return {
        out,
        outBot,
        bot,
        inBot,
        inTop,
        tongue
    };
}


class HeelSideBaseClosedPart extends ShoeSidePart {
    constructor(...data) {
        super(...data);
    }

    renderShoeSidePart(ex, ctx) {

        const {
            toeTip,
            soleBot,
            archTip,
            heelTip,
            heelTipIn,
            heelBack,
            counterTip,
            vamp,
        } = calcHeelBaseClosed.call(this, ex);

        soleBot.cp1 = {
            x: toeTip.x - 3,
            y: toeTip.y - 3
        };
        soleBot.cp2 = {
            x: soleBot.x - 8,
            y: soleBot.y
        };

        archTip.cp1 = {
            x: soleBot.x + 28,
            y: soleBot.y
        };
        archTip.cp2 = {
            x: archTip.x - 20,
            y: archTip.y - 10
        };

        heelTip.cp1 = {
            x: heelTip.x,
            y: archTip.y
        };

        heelBack.cp1 = {
            x: heelTip.x + 2,
            y: heelTip.y + 10
        };
        heelBack.cp2 = {
            x: heelBack.x - 1,
            y: heelBack.y
        };


        counterTip.cp1 = {
            x: heelBack.x + 3,
            y: heelBack.y + 10
        };
        counterTip.cp2 = {
            x: counterTip.x + 3,
            y: counterTip.y - 5
        };


        vamp.cp1 = {
            x: counterTip.x - 19,
            y: counterTip.y - 5
        };
        vamp.cp2 = {
            x: vamp.x + 18,
            y: vamp.y - 10
        };

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: this.fill,
                fill  : this.fill
            },
            ex);
        drawPoints(ctx,
            toeTip,
            soleBot,
            archTip,
            heelTip,
            heelTipIn,
            heelBack,
            counterTip,
            vamp,
            toeTip);
        ctx.fill();
    }
}

class HeelSideSimpleStrapPart extends ShoeSidePart {
    constructor(...data) {
        super(...data);
    }

    renderShoeSidePart(ex, ctx) {

        const {counterTip} = calcHeelBaseClosed.call(this, ex);
        const strapRadius = 10;
        const center = adjust(counterTip, -strapRadius, 0);

        ctx.beginPath();
        setStrokeAndFill(ctx,
            {
                stroke: this.fill,
                fill  : this.fill
            },
            ex);
        ctx.lineWidth = 2;
        ctx.ellipse(center.x - 1,
            center.y - 2,
            strapRadius,
            3.5,
            -0.95 * Math.PI,
            0,
            2 * Math.PI);
        ctx.stroke();
    }
}

export function calcHeelBaseClosed() {
    const toeTip = {
        x: 5,
        y: 12,
    };
    const soleBot = {
        x: toeTip.x + 13,
        y: toeTip.y - 9
    };
    const archTip = {
        x: toeTip.x + 65 - this.shoeHeight * 4,
        y: toeTip.y + this.shoeHeight * 5
    };
    const heelTip = {
        x: archTip.x + 6 - this.heelTipWidth * 0.5,
        y: soleBot.y
    };
    const heelTipIn = {
        x: heelTip.x + this.heelTipWidth * 0.5,
        y: heelTip.y
    };
    const heelBack = {
        x: archTip.x + 10,
        y: archTip.y
    };
    const counterTip = {
        x: archTip.x + 4,
        y: heelBack.y + 19
    };
    const vamp = {
        x: toeTip.x + 10 + this.toeCoverage * 1.7,
        y: toeTip.y + 4 + this.toeCoverage * 1.3
    };
    return {
        toeTip,
        soleBot,
        archTip,
        heelTip,
        heelTipIn,
        heelBack,
        counterTip,
        vamp,
    };
}

export class Heels extends Clothing {
    fill() {
        return "#000";
    }

    constructor(...data) {
        super({
            clothingLayer : Clothes.Layer.MID,
            thickness     : 1,
            /**
             * How high the heel is to lift the player's height in inches
             */
            shoeHeight    : 3,
            platformHeight: 0,
            heelTipWidth  : 5,
            basePointiness: 5,
            toeCoverage   : 0.5,
            /**
             * How constrained the toes are together
             */
            shoeTightness : 10,
        }, ...data);
        // resolve dynamics between modifiers
        this.Mods = Object.assign({
            feetWidth : -this.shoeTightness,
            feetLength: 0
        }, this.Mods);
        this.Mods.feetLength += (this.shoeHeight - this.platformHeight) * 4;
        this.Mods.feetWidth -= (this.shoeHeight - this.platformHeight);
        this.heelPointiness =
            this.basePointiness - (this.shoeHeight - this.platformHeight) * 0.5;
    }
}


export class ClosedToePumps extends Heels {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: HeelBasePart
            },
            {
                side: Part.RIGHT,
                Part: HeelBasePart
            },
            {
                side: Part.LEFT,
                Part: HeelBaseShine
            },
            {
                side: Part.RIGHT,
                Part: HeelSideBaseClosedPart
            },
        ];
    }
}


export class ClosedToeStrappedPumps extends Heels {
    constructor(...data) {
        super({
            strapWidth: 1.5,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: HeelBasePart
            },
            {
                side: Part.RIGHT,
                Part: HeelBasePart
            },
            {
                side: Part.LEFT,
                Part: HeelStrapPart
            },
            {
                side: Part.RIGHT,
                Part: HeelStrapPart
            },
            {
                side: Part.LEFT,
                Part: HeelBaseShine
            },
            {
                side: Part.RIGHT,
                Part: HeelSideBaseClosedPart
            },
            {
                side: Part.RIGHT,
                Part: HeelSideSimpleStrapPart
            },
        ];
    }
}

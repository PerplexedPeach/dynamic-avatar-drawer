import {Clothes, ClothingPart, Clothing} from "./clothing";
import {connectEndPoints} from "../draw/draw";
import {seamWidth, Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {getPattern} from "../util/pattern";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    clamp,
    clone,
} from "drawpoint";

class BraTopStrapPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : true,
            aboveParts: ["parts neck", "parts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const bra = calcBra(ex);
        if (bra === null) {
            return;
        }

        const sp = splitCurve(0.2, ex.neck.cusp, ex.collarbone);
        const strap = {};
        strap.top = sp.left.p2;

        bra.top.cp1 = {
            x: strap.top.x - 2,
            y: strap.top.y - 15
        };
        bra.top.cp2 = {
            x: bra.top.x,
            y: bra.top.y + 5
        };


        setStrokeAndFill(ctx,
            {
                fill  : this.fill,
                stroke: this.stroke
            },
            ex);
        ctx.lineWidth = this.strapWidth;

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            strap.top,
            bra.top);
        ctx.stroke();

    }
}


class BraBotStrapPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+chest",
            reflect   : true,
            aboveParts: ["parts torso"],
            belowParts: ["parts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const bra = calcBra(ex);
        if (bra === null) {
            return;
        }

        const strap = {};
        strap.out = {
            x: ex.breast.bot.x,
            y: ex.breast.cleavage.y
        };
        strap.outbot = {
            x: ex.breast.bot.x,
            y: ex.breast.bot.y + 1
        };
        strap.bot = {
            x: -seamWidth,
            y: ex.breast.bot.y + 3
        };
        strap.mid = {
            x: -seamWidth,
            y: ex.breast.cleavage.y
        };

        setStrokeAndFill(ctx,
            {
                fill  : this.fill,
                stroke: this.stroke
            },
            ex);

        // bottom strap
        ctx.beginPath();
        drawPoints(ctx, strap.out, strap.outbot, strap.bot, strap.mid, strap.out);
        ctx.fill();
    }
}

class BraPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {
        const bra = calcBra(ex);
        if (bra === null) {
            return;
        }

        bra.out.cp1 = simpleQuadratic(bra.top, bra.out, 0.4, 1);
        bra.top.cp1 = simpleQuadratic(ex.breast.cleavage, bra.top, 0.6, 2);

        setStrokeAndFill(ctx,
            {
                fill  : this.fill,
                stroke: this.stroke
            },
            ex);
        ctx.beginPath();
        drawPoints(ctx,
            bra.top,
            bra.out,
            bra.tip,
            ex.breast.bot,
            ex.breast.in,
            ex.breast.cleavage,
            bra.top);
        ctx.fill();
        ctx.stroke();
    }
}


export function calcBra(ex) {
    if (ex.hasOwnProperty("breast") === false) {
        return null;
    }
    const sp = splitCurve(0.7, ex.breast.top, ex.breast.tip);
    const bra = {
        out: clone(sp.right.p1),
        tip: clone(sp.right.p2)
    };
    bra.top = {
        x: ex.breast.bot.x,
        y: bra.out.y + 2
    };
    return bra;
}

class BreastWrapPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx, ignore, avatar) {
        if (!ex.breast) {
            return;
        }

        const top = clone(ex.breast.top);
        let sp = splitCurve(clamp(-0.25 + avatar.getDim("breastSize") * 0.01, 0, 1), ex.breast.top,
            ex.breast.tip);
        const topStroke = sp.left.p2;
        const tipStroke = sp.right.p2;

        // don't display nipples and other chest decorations
        let chestDecorativeIndex = 0;
        let part = avatar.getPartInLocation("chest",
            avatar.decorativeParts,
            chestDecorativeIndex);
        while (part) {
            part.coverConceal = ["chest"];
            part = avatar.getPartInLocation("chest",
                avatar.decorativeParts,
                ++chestDecorativeIndex);
        }

        setStrokeAndFill(ctx,
            {
                fill  : this.fill,
                stroke: this.stroke
            },
            ex);
        ctx.beginPath();
        drawPoints(ctx,
            top,
            ex.breast.tip,
            ex.breast.bot,
            ex.breast.in,
            connectEndPoints(ex.breast.in, top)
        );
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            topStroke,
            tipStroke,
            ex.breast.bot
        );
        ctx.stroke();
    }
}


class BreastWrapStrapPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+chest",
            reflect   : true,
            aboveParts: ["parts torso"],
            belowParts: ["parts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx, ignore, avatar) {
        let top = {
            x: 0,
            y: ex.armpit.y
        };

        let outTop;
        const armpit = extractPoint(ex.armpit);
        if (ex.breast) {
            top.y = ex.breast.top.y - 2;
            outTop = extractPoint(ex.breast.top);
            outTop.cp1 = {
                x: outTop.x * 0.5,
                y: top.y
            };
            armpit.cp1 = simpleQuadratic(outTop, armpit, 0.5, 1);
        }


        let sp = splitCurve(
            clamp(this.chestCoverage + avatar.getDim("breastSize") * 0.01, 0, 1),
            ex.armpit,
            ex.waist);
        const outBot = sp.left.p2;

        const bot = {
            x: 0,
            y: outBot.y - 1
        };
        bot.cp1 = {
            x: outBot.x * 0.5,
            y: bot.y
        };

        Clothes.simpleStrokeFill(ctx, ex, this);

        // bottom strap
        ctx.beginPath();
        drawPoints(ctx, top, outTop, armpit, outBot, bot);
        ctx.fill();
        ctx.stroke();
    }
}


class PantiesPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "groin",
            reflect   : true,
            aboveParts: ["parts groin", "parts torso"],
            belowParts: ["parts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (ex.hasOwnProperty("groin") === false) {
            return;
        }

        const panties = {};
        panties.top = {
            x: -seamWidth,
            y: ex.pelvis.y
        };

        const sp = splitCurve(0.9, ex.waist, ex.hip);
        panties.out = sp.right.p1;
        panties.out.cp1 = simpleQuadratic(panties.top, panties.out, 0.4, -3);
        panties.outbot = sp.right.p2;

        panties.bot = {
            x: -seamWidth,
            y: ex.groin.y
        };
        panties.bot.cp1 = {
            x: panties.outbot.x,
            y: panties.outbot.y
        };
        panties.bot.cp2 = {
            x: panties.bot.x + 5,
            y: panties.bot.y
        };

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx,
            panties.top,
            panties.out,
            panties.outbot,
            panties.bot,
            panties.top);
        ctx.fill();

        // only draw outline for top and bottom
        ctx.beginPath();
        drawPoints(ctx,
            panties.top, panties.out, panties.outbot, panties.bot);
        ctx.stroke();
    }

}


export class Underwear extends Clothing {
    constructor(...data) {
        super({clothingLayer: Clothes.Layer.BASE}, ...data);
    }

}


export class Bra extends Underwear {
    constructor(...data) {
        super(...data);
    }

    // stroke() {
    //     return "hsl(0,30%,30%)";
    // }

    fill() {
        return "hsl(0,50%,50%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BraBotStrapPart
            },
            {
                side: null,
                Part: BraPart
            }
        ];
    }
}

export class BreastWrap extends Underwear {
    constructor(...data) {
        super({
            chestCoverage: 0.6,
            thickness    : 0.5,
            wrapSize     : 30,
        }, ...data);
        this.fill = getPattern("bandages", this.wrapSize);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BreastWrapPart
            },
            {
                side: null,
                Part: BreastWrapStrapPart
            },
        ];
    }
}

export class Panties extends Underwear {
    constructor(...data) {
        super(...data);
    }

    fill() {
        return "hsl(0,50%,50%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: PantiesPart
            }
        ];
    }
}


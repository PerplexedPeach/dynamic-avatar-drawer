import {none, clone} from "drawpoint";
import {extractUnmodifiedLocation, extractLocationModifier} from "../util/part";
import {getSideLocation, getSideValue} from "../parts/part";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";

/**
 * Where all Clothing and ClothingPart should go
 * @namespace Clothes
 * @memberof module:da
 */

/**
 * Clothing holds statistics and information about the clothes, but no drawing methods
 * Instead, it holds ClothingParts that each know how to draw themselves
 * @memberof module:da
 */
export class Clothing {
    constructor(...data) {

        /**
         * Default properties of every clothing object
         * @property {module:da.Clothes.Layer} clothingLayer Layer relative to other clothing (how close
         * is the clothing to the skin)
         * @property {object[]} partPrototypes Pairs of side and ClothingPart prototypes
         * @property {module:da.Layer} partPrototypes[].side Side of the clothing part
         * @property {module:da.ClothingPart} partPrototype[].part A clothing part used by this clothing
         * @type {{clothingLayer: number, partPrototypes: Array}}
         */
        Object.assign(this, {
            clothingLayer: Clothes.Layer.BASE,
            thickness    : 1.2,
        });
        // actual parts storage
        this.parts = [];

        // replace each part (prototype) with an actual part object
        Object.getPrototypeOf(this).partPrototypes.forEach((partPair) => {
            let {side, Part} = partPair;

            const thisPart = new Part();
            // mixin behaviour by assuming all component part's properties
            Object.assign(this, thisPart);

            if (thisPart.forcedSide !== undefined) {
                side = thisPart.forcedSide;
            }
            const sideString = getSideLocation(side);

            if (thisPart.forcedNoSideString == false) {
                // rename for more specificity
                if (sideString === "right" ||
                    sideString === "left") {
                    const baseLocation = extractUnmodifiedLocation(thisPart.loc);
                    const locationModifiers = extractLocationModifier(thisPart.loc);
                    thisPart.loc = locationModifiers + sideString + " " + baseLocation;
                }
            }

            thisPart.side = getSideValue(side);
            thisPart._owner = this;

            this.parts.push(thisPart);
        });

        // do this after all parts have their default properties assigned
        Object.assign(this, ...data.map(clone));
    }

    stroke() {
        return none;
    }

    fill() {
        return "#fff";
    }
}

export const Clothes = {
    /**
     * Clothing layers (separate from drawing layers)
     * @readonly
     * @enum
     * @memberof module:da.Clothes
     */
    Layer: Object.freeze({
        BASE      : 0,
        INNER     : 1,
        MID       : 2,
        OUTER     : 3,
        NUM_LAYERS: 4,
    }),
    /**
     * Create a Clothing instance
     * @memberof module:da.Clothes
     * @param {Clothing} Clothing Clothing prototype to instantiate
     * @param {object} data Overriding data
     * @returns {Clothing} Instantiated clothing object
     */
    create(Clothing, ...data) {
        return new Clothing(...data);
    },

    simpleStrokeFill(ctx, ex, clothing) {
        setStrokeAndFill(ctx,
            {
                fill  : clothing.fill,
                stroke: clothing.stroke
            },
            ex);
        ctx.lineWidth = clothing.thickness;
    },
};


/**
 * Holds no statistical information (state)
 * instead relying on the owning Clothing object to do so
 * @memberof module:da
 */
export class ClothingPart {
    constructor(...data) {
        Object.assign(this, {
            // drawing layer (not clothing layer)
            layer             : Layer.FRONT,
            loc               : "torso",
            /**
             * When this ClothingPart has a side, whether to force it to not append
             * a side to its location (e.g. if it's torso and you want it to appear on a side,
             * but don't want the location to be "left torso")
             */
            forcedNoSideString: false,
        }, ...data);
    }

    renderClothingPoints() {
    }
}



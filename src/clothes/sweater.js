import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {connectEndPoints} from "../draw/draw";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    none,
    splitCurve,
    adjust,
    clone,
    breakPoint,
} from "drawpoint";

/**
 * ClothingPart drawn classes/components
 */
class SweaterBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            aboveSameLayerParts: ["groin"]
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {top, out, cusp, collarbone, waist, bot} = calcSweaterBase.call(this, ex);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            top,
            out,
            cusp,
            collarbone,
            ex.armpit,
            ex.lat,
            waist,
            bot.out,
            bot.bot);
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            top,
            out,
            cusp,
            collarbone, breakPoint,
            ex.armpit,
            ex.lat,
            waist,
            bot.out,
            bot.bot);
        ctx.stroke();

    }
}


/**
 * Calculate the drawpoints for the torso base of a sweater
 * @this {ClothingPart}
 * @param {number} this.neckCoverage [0,1] proportion of the neck that's covered by the sweater
 * @param {number} this.stomachCoverage [0,1] proportion of the stomach that's covered
 * @param {number} this.thickness how defined the outline should be
 * @param ex
 * @returns {{top: {x: number, y: number}, out: *|p2|{x, y}|l|i, cusp: *, collarbone: *, waist: *, bot: {}}}
 */
export function calcSweaterBase(ex) {
    let sp = splitCurve(1 - this.neckCoverage, ex.neck.top,
        ex.neck.cusp
    );

    const out = sp.left.p2;
    const top = {
        x: 0,
        y: out.y - 2
    };
    out.cp1 = {
        x: top.x * 0.5 + out.x * 0.5,
        y: top.y
    };

    const cusp = extractPoint(ex.trapezius);
    cusp.cp1 = {
        x: out.x + this.neckCoverage * 2,
        y: out.y - 1
    };
    cusp.cp2 = {
        x: cusp.x,
        y: cusp.y + this.neckCoverage * 3
    };

    // where does this shirt end
    sp = splitCurve(this.stomachCoverage, ex.waist, ex.hip);
    const bot = {};
    bot.out = sp.left.p2;
    bot.out.x += this.thickness * 0.4;

    bot.bot = extractPoint(ex.pelvis);
    bot.bot.y += bot.out.y - ex.hip.y;
    bot.bot.cp1 = {
        x: bot.bot.x * 0.5 + bot.out.x * 0.5,
        y: bot.bot.y
    };

    const collarbone = clone(ex.collarbone);
    const waist = adjust(ex.waist, this.thickness * 0.8, 0);

    return {
        top,
        out,
        cusp,
        collarbone,
        waist,
        bot,
    };
}

class CoveredBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        // no breast means we cover just the nipples
        if (ex.hasOwnProperty("breast") === false) {
            ctx.save();
            ctx.lineWidth = 10;
            setStrokeAndFill(ctx,
                {
                    fill  : none,
                    stroke: this.fill
                },
                ex);
            ctx.beginPath();
            drawPoints(ctx, breakPoint, ex.chest.nipples);
            ctx.stroke();
            ctx.restore();
            return;
        }

        const top = adjust(ex.breast.top, 0, 0);
        const tip = adjust(ex.breast.tip, 0.1, 0);
        const bot = adjust(ex.breast.bot, 0, -0.1);
        const cleavage = adjust(ex.breast.cleavage, 0, -0.1);
        const inner = adjust(ex.breast.in, 0, -0.1);

        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            top,
            tip,
            bot,
            inner,
            cleavage,
            connectEndPoints(cleavage, top)
        );
        ctx.fill();

        // fill out breasts
        ctx.beginPath();
        drawPoints(ctx,
            top,
            tip,
            bot,
            inner
        );
        ctx.stroke();

    }
}


/**
 * Long sleeve means between elbow and wrist
 */
export class LongSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            sleeveLength: 1,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {
            collarbone,
            deltoids,
            shoulder,
            elbowOut,
            out,
            bot,
            elbow,
            armpit
        } = calcLongSleeve.call(this, ex);

        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            deltoids,
            shoulder,
            elbowOut,
            out,
            bot,
            elbow,
            armpit,
            connectEndPoints(armpit, collarbone)
        );
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            deltoids,
            shoulder,
            elbowOut,
            out,
            bot,
            elbow,
            armpit
        );
        ctx.stroke();

    }
}

/**
 * Calculate the draw points for a long sleeve part
 * @param ex
 * @returns {{collarbone: ({x, y}|*), deltoids: *, shoulder: Object, elbowOut: Object, out, bot, elbow, armpit: Object}}
 */
export function calcLongSleeve(ex) {
    let sp = splitCurve(this.sleeveLength, ex.elbow.out, ex.wrist.out);
    let out = sp.left.p2;
    out.cp1.x += 0.5 * this.thickness;
    out.cp2.x += 0.3 * this.thickness;

    sp = splitCurve((1 - this.sleeveLength) * 0.9, ex.wrist.in, ex.elbow.in);
    let bot = sp.left.p2;
    let elbow = sp.right.p2;
    elbow = adjust(elbow, -this.thickness * 0.4, 0);

    const collarbone = extractPoint(ex.collarbone);
    collarbone.x -= 0.2;

    const shoulder = adjust(ex.shoulder, this.thickness * 0.5, 0);
    const elbowOut = adjust(ex.elbow.out, this.thickness * 0.5, 0);

    bot.cp1 = simpleQuadratic(out, bot, 0.5, 1);

    const armpit = adjust(ex.armpit, -0.2, 0);
    const deltoids = ex.deltoids;
    return {
        collarbone,
        deltoids,
        shoulder,
        elbowOut,
        out,
        bot,
        elbow,
        armpit
    };
}

/**
 * Short sleeve means between shoulder and elbow
 */
class ShortSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        let sp = splitCurve(1 - this.sleeveLength, ex.shoulder, ex.elbow.out);
        let out = sp.left.p2;

        sp = splitCurve((1 - this.sleeveLength) * 0.9, ex.elbow.in, ex.armpit);
        let bot = sp.left.p2;
        let armpit = sp.right.p2;
        armpit = adjust(armpit, -0.1, 0);

        // adjust(ex.collarbone, this.thickness * 0.2, this.thickness * 0.5),
        const {collarbone} = calcSweaterBase.call(this, ex);
        collarbone.x -= 0.1;
        const shoulder = adjust(ex.shoulder, this.thickness * 0.5, 0);

        bot.cp1 = simpleQuadratic(out, bot, 0.5, 1);

        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            ex.deltoids,
            shoulder,
            out,
            bot,
            armpit,
            connectEndPoints(armpit, collarbone)
        );
        ctx.fill();

        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            ex.deltoids,
            shoulder,
            out,
            bot,
            armpit
        );
        ctx.stroke();

    }
}


/**
 * Base Clothing classes
 */
export class Sweater extends Clothing {
    constructor(...data) {
        super({
            clothingLayer  : Clothes.Layer.MID,
            /**
             * How much of the stomach should be covered (1 means fully)
             */
            stomachCoverage: 0.8,
            /**
             * How much of the neck will the collar cover
             */
            neckCoverage   : 0.5,
            /**
             * How far to extend the sleeve (between 0 and 1)
             */
            sleeveLength   : 0.8,
        }, ...data);
    }
}


/**
 * Concrete Clothing classes
 */
export class LongSleevedSweater extends Sweater {
    constructor(...data) {
        super({
            stomachCoverage: 0.7,
            neckCoverage   : 0.5,
            sleeveLength   : 1,
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: SweaterBasePart
            },
            {
                side: null,
                Part: CoveredBreastPart
            },
            {
                side: Part.LEFT,
                Part: LongSleevePart
            },
            {
                side: Part.RIGHT,
                Part: LongSleevePart
            },
        ];
    }
}


export class AsymmetricSleevedSweater extends Sweater {
    constructor(...data) {
        super({
            stomachCoverage: 0.7,
            neckCoverage   : 0.5,
            sleeveLength   : 1,
        }, ...data);
    }

    stroke() {
        return "#000";
    }

    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: SweaterBasePart
            },
            {
                side: null,
                Part: CoveredBreastPart
            },
            {
                side: Part.LEFT,
                Part: ShortSleevePart
            },
            {
                side: Part.RIGHT,
                Part: LongSleevePart
            },
        ];
    }
}



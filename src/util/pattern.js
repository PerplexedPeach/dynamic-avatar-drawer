/**
 * Created by Johnson on 2017-06-17.
 */
export const IMAGE_MAXSIZE = 100;
let RELEASE_MODE = false;


// canvas used for patterns
const patternCanvas = document.createElement("canvas");
patternCanvas.width = IMAGE_MAXSIZE;
patternCanvas.height = IMAGE_MAXSIZE;

const patternCtx = patternCanvas.getContext("2d");

// canvas used for scaling images
const imageCanvas = document.createElement("canvas");


/**
 * Cache of loaded patterns, lazily created as needed by getPattern.
 * Each one is either a function that returns either a color, gradient, pattern (all are statically cached),
 * or a function that returns either a color, gradient, or pattern (dynamically cached since the parameters depend
 * on draw exports).
 */
const cachedPatterns = {};

/**
 * Patterns to load as soon as we want to draw. Populated by getPattern as necessary.
 * @type {Promise[]}
 */
export const patternLoadingQueue = [];


/**
 * patternName -> pattern production function map
 * Populated by calling addPattern, and queried by getPattern if pattern hasn't been cached yet
 */
const producePattern = {};


/**
 * List names of available patterns to gettable by getPattern
 * @returns {string[]}
 */
export function listAvailablePatterns() {
    const patterns = [];
    for (let pat in producePattern) {
        if (producePattern.hasOwnProperty(pat)) {
            patterns.push(pat);
        }
    }
    return patterns;
}

export function getPatternFullName(patternName, patternSize) {
    return patternName + "." + patternSize;
}

export function getPatternBaseName(patternName) {
    const splitPoint = patternName.indexOf(".");
    if (splitPoint < 0) {
        return patternName;
    }
    return patternName.substr(0, splitPoint);
}

/**
 * Get a fill or stroke pattern. When used inside draw, guaranteed to supply a usable pattern.
 * Used outside of draw could result in unfulfilled promises.
 * @param {string} patternName Name of the pattern
 * @param {number} patternSize How large the image canvas should be for this pattern
 * @returns {Array} pattern method
 */
export function getPattern(patternName, patternSize = IMAGE_MAXSIZE) {
    console.log("getting pattern " + patternName);
    // cache along with the size to prevent reloads
    const fullName = getPatternFullName(patternName, patternSize);
    if (cachedPatterns.hasOwnProperty(fullName)) {
        return cachedPatterns[fullName];
    }

    if (producePattern.hasOwnProperty(patternName) === false) {
        throw new Error("Trying to get pattern that hasn't been added yet: " + patternName);
    }

    // create the promise
    patternLoadingQueue.push(loadPattern(patternName,
        producePattern[patternName], patternSize));

    // return wrapper around delayed call
    return [
        "pattern",
        {
            patternName: fullName,
            patternSize
        }
    ];
}

export function getLoadedPattern(pattern, ctx, ex) {
    if (Array.isArray(pattern) && pattern[0] === "pattern") {
        const source = cachedPatterns[pattern[1].patternName];
        // need dynamic production
        if (typeof source === "function") {
            const data = Object.assign({
                ctx,
                ex
            }, pattern[1]);
            return source(data);
        }
        return source;
    } else if (typeof pattern === "function") {
        return pattern.call(this, ctx, ex);
    }
    return pattern;
}

export function addPattern(patternName, patternSource) {
    "use strict";
    if (producePattern[patternName]) {
        return;
    }
    producePattern[patternName] = patternSource;
}


export function addDebugPattern(patternName, debugSrc, releaseSrc) {
    let src = debugSrc;
    if (RELEASE_MODE) {
        src = releaseSrc;
    }
    return addPattern(patternName, src);
}

/**
 * Load pattern from some source method (either function or an image path) asynchronously
 * @param {string} patternName
 * @param {(string|function)} patternSource
 * @param {number} patternSize How large the image canvas should be for this pattern
 * @returns {Promise} Promise to produce this pattern (and cache it)
 */
export function loadPattern(patternName, patternSource, patternSize) {
    // depending on what kind of pattern was put in, we have different promises to fulfill

    const fullName = getPatternFullName(patternName, patternSize);
    /**
     * Custom method of creating the pattern, such as anything requiring ctx
     */
    if (typeof patternSource === "function") {
        /**
         * We heuristically decide whether pattern can be cached or has to be dynamic
         * by looking at the number of parameters the function accepts.
         * If it's <= 1 parameters, we asssume it takes ctx only and can be cached
         * Else we assume it needs ex or some other parameters
         */
        if (patternSource.length <= 1) {
            return new Promise((resolve) => {
                cachedPatterns[fullName] = patternSource(patternCtx);
                resolve();
            });
        } else {
            return new Promise((resolve) => {
                cachedPatterns[fullName] = patternSource;
                resolve();
            });
        }

    } else {
        // load image for the first time and cache it
        return new Promise((resolve, reject) => {
            const image = new Image();
            image.onload = () => {
                if (image.width < patternSize) {
                    cachedPatterns[fullName] = patternCtx.createPattern(image, "repeat");
                } else {
                    const scale = image.width / patternSize;
                    const height = Math.round(image.height / scale);
                    imageCanvas.width = patternSize;
                    imageCanvas.height = height;

                    const imageCtx = imageCanvas.getContext("2d");
                    imageCtx.drawImage(image,
                        0,
                        0,
                        image.width,
                        image.height,
                        0,
                        0,
                        patternSize,
                        height);
                    cachedPatterns[fullName] = patternCtx.createPattern(imageCanvas, "repeat");
                }

                resolve();
            };
            image.onerror = () => {
                alert(Error("failed to load pattern '" + patternName + "' from " +
                            patternSource));
                reject();
            };
            image.src = patternSource;
        });
    }
}

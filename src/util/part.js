/**
 * Created by Johnson on 2017-06-17.
 */
/**
 * Extract the side component from a location string
 * @memberof module:da
 * @param {string} partLoc Location string of a part
 * @returns {(string|null)} Either the side string or null if no side indicated
 */
export function extractSideLocation(partLoc) {
    const loc = extractUnmodifiedLocation(partLoc);
    // should be first word separated by whitespace
    if (loc.indexOf(" ") < 0) {
        return null;
    }
    return loc.substr(0, loc.indexOf(" "));
}

/**
 * Extract the base component from a location string
 * @memberof module:da
 * @param {string} partLoc Location string of a part
 * @returns {string} The base component
 */
export function extractBaseLocation(partLoc) {
    const loc = extractUnmodifiedLocation(partLoc);
    // last word separated by whitespace
    if (loc.indexOf(" ") < 0) {
        return loc;
    }
    return loc.substr(loc.lastIndexOf(" ") + 1);
}

/**
 * Extract the underlying location (side + base) of a location after stripping it of
 * potential modifier characters, which are prepended (use + for non-restrictive, - for exclusive)
 * @memberof module:da
 * @param {string} partLoc location name of part
 */
export function extractUnmodifiedLocation(partLoc) {
    for (let c = 0; c < partLoc.length; ++c) {
        switch (partLoc[c]) {
        case "+":
        case "-":
            break;
        default:
            return partLoc.substr(c);
        }
    }
}

/**
 * Extract modifier characters (+ for non-restrictive, - for exclusive)
 * @memberof module:da
 * @param {string} partLoc Location name of part
 * @returns {string} Modifier characters for this location
 */
export function extractLocationModifier(partLoc) {
    let modifiers = "";
    for (let c = 0; c < partLoc.length; ++c) {
        switch (partLoc[c]) {
        case "+":
        case "-":
            modifiers += partLoc[c];
            break;
        default:
            break;
        }
    }
    return modifiers;
}
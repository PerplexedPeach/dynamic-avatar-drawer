/**
 * Get a default properties factory
 * @param {Object} limits Descriptors of properties, each with avg, low, high, stdev, bias
 * @param {Object} discretePool List of valid values for discrete valued properties
 * @returns {run} properties factory
 */
export function getDefault(limits, discretePool) {
    function run() {
        var defaults = {};
        for (let p in limits) {
            if (limits.hasOwnProperty(p)) {
                defaults[p] = limits[p].avg;
            }
        }

        if (discretePool) {
            for (let p in discretePool) {
                if (discretePool.hasOwnProperty(p)) {
                    defaults[p] = discretePool[p][0];
                }
            }
        }
        return defaults;
    }

    return run;
}

/**
 * Apply additional modifiers
 * @param sourceMods Modifiers to be modified
 * @param addMods Modifiers to add
 */
export function applyMods(sourceMods, addMods) {
    for (let mod in addMods) {
        if (addMods.hasOwnProperty(mod)) {
            if (sourceMods.hasOwnProperty(mod)) {
                sourceMods[mod] += addMods[mod];
            } else {
                sourceMods[mod] = addMods[mod];
            }
        }
    }
}

/**
 * Remove modifiers
 * @param sourceMods Modifiers to be modified
 * @param addMods Modifiers to remove
 */
export function removeMods(sourceMods, addMods) {
    for (let mod in addMods) {
        if (addMods.hasOwnProperty(mod)) {
            if (sourceMods.hasOwnProperty(mod)) {
                sourceMods[mod] -= addMods[mod];
            }
        }
    }
}

// CREATE CHARACTER
/**
 * Retrieve female bias with global lookup fallback
 * @param {Object} propertyDescriptor Statistical description of property including avg, low, high
 * @param {string} propertyName
 * @returns {number} Female bias
 */
export function getBiasMod(propertyDescriptor, propertyName) {
    // own defined property takes precidence over the globally defined one
    if (propertyDescriptor.hasOwnProperty("bias")) {
        return propertyDescriptor.bias;
    }
    if (da.femBias.hasOwnProperty(propertyName)) {
        return da.femBias[propertyName];
    }
    // default to 1 (higher values correlated with higher femininity)
    return 1;
}

/**
 * Randomly create a Player
 * @param {number} bias female bias - how probably their physical traits are feminine
 * @returns {Player}
 */
export function createRandomCharacter(bias, stdevWeight = 1) {
    if (typeof bias === "string") {
        bias = parseFloat(bias);
    }
    // bias is a number from 0 to 1 with 1 being the most feminine bias and 0 most
    // masculine bias
    var pc = new da.Player();

    // generated with default (average) stats, perturb with bias
    const dimensions = da.baseDimDesc[pc.skeleton];
    for (let dim in dimensions) {
        if (dimensions.hasOwnProperty(dim)) {
            const dimDesc = dimensions[dim];
            if (dimDesc.hasOwnProperty("stdev") === false) {
                throw new Error(`dimension ${dim} has no stdev defined`);
            }
            pc.basedim[dim] +=
                da.randNormal(bias * da.getBiasMod(dimDesc, dim),
                    dimDesc.stdev * stdevWeight);
        }
    }

    // generate numerical statistics
    for (let s in da.statLimits) {
        if (da.statLimits.hasOwnProperty(s)) {
            var stat = da.statLimits[s];
            if (stat.hasOwnProperty("stdev") === false) {
                throw new Error(`stat ${s} has no stdev defined`);
            }
            pc[s] += da.randNormal(bias * da.getBiasMod(stat, s),
                stat.stdev * stdevWeight);
        }
    }
    // generate discrete statistics
    for (let s in da.statDiscretePool) {
        if (da.statDiscretePool.hasOwnProperty(s)) {
            // uniform randomly pick a value from any of the available ones
            const valuePool = da.statDiscretePool[s];
            pc[s] = valuePool[Math.floor(Math.random() * valuePool.length)];
        }
    }

    pc.clampStats();

    // names is a discrete statistic, but it's special in that different genders tend to
    // have different pools of values
    if (bias > 0) {
        pc.name = da.femaleNames[Math.floor(Math.random() * da.femaleNames.length)];
        if (bias > 0.5) {
            pc.gender = "female";
        }
    } else {
        pc.name = da.maleNames[Math.floor(Math.random() * da.maleNames.length)];
        if (bias < -0.5) {
            pc.gender = "male";
        }
    }

    for (let m in da.modLimits) {
        if (da.modLimits.hasOwnProperty(m)) {
            const mod = da.modLimits[m];
            if (mod.hasOwnProperty("stdev") === false) {
                throw new Error(`mod ${m} has no stdev defined`);
            }
            pc.Mods[m] +=
                da.randNormal(bias * da.getBiasMod(mod, m),
                    mod.stdev * stdevWeight);
        }
    }

    return pc;
}

export function createTransformation(object, showTransformation, transformBy) {
    let completionPercent = 0;

    function transform(percent) {
        let stepPercent = percent;
        if (completionPercent + percent > 1) {
            stepPercent = 1 - completionPercent;
            completionPercent = 1;
        } else {
            completionPercent += percent;
        }

        performTransformation(object, transformBy, stepPercent);
        return completionPercent;
    }

    function performTransformation(entity, modifications, stepPercent) {
        for (let prop in modifications) {
            if (modifications.hasOwnProperty(prop) && entity.hasOwnProperty(prop)) {
                switch (typeof modifications[prop]) {
                case "object":
                    performTransformation(entity[prop], modifications[prop], stepPercent);
                    break;
                case "number":
                    entity[prop] += modifications[prop] * stepPercent;
                    break;
                default:
                    console.log(`unrecognized transformation property type ${typeof modifications[prop]} for ${prop}`);
                }
            }
        }
    }

    // transformation object
    return {
        object,
        transform,
        showTransformation
    };
}

export function transformAndShow(transformation, duration = 5000) {
    let last = null;

    return new Promise((resolve) => {
        function doTransform(timestamp) {
            const delta = (last === null) ? 0 : timestamp - last;
            last = timestamp;
            const stepPercent = delta / duration;

            const transformationCompletion = transformation.transform(stepPercent);
            transformation.showTransformation();

            if (transformationCompletion < 1) {
                window.requestAnimationFrame(doTransform);
            } else {
                resolve();
            }
        }

        window.requestAnimationFrame(doTransform);
    });
}

import {FacePart} from "./face_part";
import {adjustColor} from "../util/utility";
import {
    extractPoint,
    clamp,
    breakPoint,
    continueCurve,
    adjust,
    rotatePoints,
    endPoint
} from "drawpoint";

class Eyelash extends FacePart {
    constructor(...data) {
        super({
            loc       : "eyelash",
            aboveParts: ["eyelid"],
            parentPart: "eyelid"
        }, ...data);
    }
}

export class EyelashHuman extends Eyelash {
    constructor(...data) {
        super(...data);
    }

    fill(ignore, ex) {
        return adjustColor(ex.hairFill,
            {
                l: -10,
                s: -10
            });
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            const eyelash = ex.eyelash = {};
            const eyelid = ex.eyelid;

            const lashVisibility = mods.eyelashAngle * this.eyelashLength * 0.1;

            eyelash.top = {
                x: ex.eyes.top.x + mods.eyelashBias * 0.1,
                y: eyelid.top.y + lashVisibility
            };
            eyelash.top.cp1 = {
                x: eyelid.in.x * 0.7 + eyelash.top.x * 0.3,
                y: eyelid.in.y * 0.5 + eyelash.top.y * 0.5
            };
            eyelash.top.cp2 = {
                x: eyelid.in.x * 0.4 + eyelash.top.x * 0.6,
                y: eyelash.top.y
            };

            eyelash.out = extractPoint(ex.eyes.out);
            eyelash.out.cp1 = continueCurve(ex.eyes.out, eyelash.top, 1);


            eyelash.outBot = extractPoint(ex.eyes.out);
            eyelash.outBot.cp1 = adjust(ex.eyes.in.cp2,
                mods.eyelashBias * clamp(this.eyelashLength, 0, 4) * 0.01,
                -this.eyelashLength * 0.03);
            eyelash.outBot.cp2 = adjust(ex.eyes.in.cp1,
                mods.eyelashBias * clamp(this.eyelashLength, 0, 5) * 0.01,
                -this.eyelashLength * 0.03);

            const eyes = ex.eyes;
            rotatePoints(eyes.center, mods.eyeTilt * Math.PI / 180, eyelash.top,
                eyelash.out);
        }

        return [
            ex.eyes.out,
            ex.eyelid.top,
            ex.eyelid.in,
            ex.eyelash.top,
            ex.eyelash.out,
            breakPoint,
            ex.eyes.out,
            ex.eyes.in,
            ex.eyelash.outBot,
            endPoint
        ];
    }

}

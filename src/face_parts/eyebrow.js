import {FacePart} from "./face_part";
import {adjustColor} from "../util/utility";
import {clamp, simpleQuadratic, rotatePoints} from "drawpoint";

class Brow extends FacePart {
    constructor(...data) {
        super({
            loc       : "brow",
            aboveParts: ["parts head", "faceParts eyelid"],
        }, ...data);
    }
}


export class BrowHuman extends Brow {

    constructor(...data) {
        super( ...data);

    }

    fill(ignore, ex) {
        return adjustColor(ex.hairFill,
            {
                l: -5,
                s: -5
            });
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            const brow = ex.brow = {};
            const naturalHeight = 0.7 + this.faceFem * 0.01;
            brow.inbot = {
                x: ex.eyes.center.x - 2 + mods.browCloseness * 0.1,
                y: ex.eyes.center.y + naturalHeight - 0.5 + mods.browHeight * 0.1,
            };

            const thickness = mods.browThickness * 0.1 - this.faceFem * 0.005;
            brow.intop = {
                x: brow.inbot.x + mods.browSharpness * 0.1,
                y: brow.inbot.y + clamp(0.95 + thickness, 0, 5)
            };
            brow.intop.cp1 = simpleQuadratic(brow.inbot, brow.intop, 0.5, 0.5);

            brow.outbot = {
                x: ex.eyes.center.x + 2.5 + mods.browLength * 0.1,
                y: ex.eyes.center.y + naturalHeight + mods.browHeight * 0.1
            };

            brow.outtop = {
                x: brow.outbot.x + mods.browOutBias * 0.1,
                y: brow.outbot.y +
                   clamp(0.7 + thickness + mods.browOutBias * 0.05, 0, 5),
            };
            brow.outtop.cp1 =
                simpleQuadratic(brow.intop, brow.outtop, 0.4, mods.browTopCurl * 0.1);
            if (mods.browOutBias > -5) {
                brow.outbot.cp1 =
                    simpleQuadratic(brow.outtop, brow.outbot, 0.5, 0.5);
            }
            brow.inbot.cp1 =
                simpleQuadratic(brow.outbot, brow.intop, 0.6, -mods.browBotCurl * 0.1);

            // apply tilt
            rotatePoints(brow.inbot, mods.browTilt * Math.PI / 180, brow.inbot,
                brow.intop,
                brow.outtop, brow.outbot);
        }

        return [ex.brow.inbot, ex.brow.intop, ex.brow.outtop, ex.brow.outbot, ex.brow.inbot];
    }
}


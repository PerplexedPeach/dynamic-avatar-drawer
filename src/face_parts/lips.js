import {FacePart} from "./face_part";
import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {
    extractPoint,
    clamp,
    splitCurve,
    simpleQuadratic,
    breakPoint,
    endPoint,
    clone
} from "drawpoint";

class LipShading extends ShadingPart {
    constructor(...data) {
        super({
            loc       : "+lips",
            reflect   : true,
            layer     : Layer.BELOW_HAIR,
            aboveParts: ["faceParts lips"],
        }, ...data);
    }

    fill() {
        return "hsl(0,0%,85%)";
    }

    calcDrawPoints(ex) {
        let center = clone(ex.lips.bot.top);
        return [
            breakPoint,
            center,
            ex.lips.out.lower,
            ex.lips.bot,
            center,
            endPoint,
        ];
    }
}


class Lips extends FacePart {
    constructor(...data) {
        super({
            loc         : "lips",
            reflect     : true,
            aboveParts  : ["mouth"],
            shadingParts: [LipShading],
        }, ...data);
    }
}


export class LipsHuman extends Lips {
    constructor(...data) {
        super(...data);
    }

    fill(ignore, ex) {
        return ex.baseLipColor;
    }

    clipFill() {
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            const h = this.faceLength * 0.1;
            const faceMasculinity = 40 - this.faceFem;

            // height that parting offsets the bottom lips
            const topPartingHeight = mods.lipParting * 0.007;
            const botPartingHeight = -mods.lipParting * 0.015;

            const lips = ex.lips = {};
            lips.center = {
                x: -0.1,
                y: ex.skull.y - h * 0.78 + mods.lipHeight * 0.1 +
                   this.faceFem * 0.018
            };
            lips.top = {
                x: lips.center.x,
                y: lips.center.y + clamp(this.lipSize * 0.015 + this.faceFem * 0.005 +
                   mods.lipTopSize * 0.01, 0, 3) + topPartingHeight
            };

            // tip of the Cupid's bow
            lips.tip = {
                x: clamp(0.7 - this.faceFem * 0.003 + mods.lipCupidsBow * 0.01, 0, 5) *
                   this.lipSize / 20,
                y: lips.top.y + mods.lipTopSize * 0.005 +
                   this.faceFem * 0.003 + this.lipSize * 0.01,
            };
            lips.tip.cp1 =
                simpleQuadratic(lips.top, lips.tip, 0.5,
                    0.1 + mods.lipTopCurve * 0.001);

            // outer corner
            lips.out = {
                x: 1 + this.lipSize * 0.08 + mods.lipWidth * 0.005 + faceMasculinity * 0.01,
                y: lips.center.y + mods.lipCurl * 0.02,
            };
            // lips.out.cp1 =
            //     simpleQuadratic(lips.tip, lips.out, 0.3,
            //         clamp(mods.lipTopCurve * 0.01, -0.2, 2));
            lips.out.cp1 = {
                x: lips.tip.x * 0.5 + lips.out.x * 0.5 + mods.lipTopCurve * 0.002,
                y: lips.tip.y
            };
            lips.out.cp2 = {
                x: lips.tip.x * 0.3 + lips.out.x * 0.7,
                y: lips.tip.y * 0.5 + lips.out.y * 0.5 + mods.lipTopCurve * 0.01
            };

            // bottom of lips
            lips.bot = {
                x: 0,
                y: lips.center.y - 0.3 - this.lipSize * 0.02 - this.faceFem * 0.008 -
                   mods.lipBotSize * 0.004 + botPartingHeight
            };
            lips.bot.cp1 = simpleQuadratic(lips.out, lips.bot, 0.5,
                this.lipSize * 0.02 + this.faceFem * 0.003 + mods.lipBotSize * 0.003 -
                botPartingHeight * 0.5);
            lips.bot.cp1.y -= mods.lipCurl * 0.01;


            const biasHeightOffset = -0.1 - mods.lipBias * 0.006;
            // top of the bot lips
            lips.bot.top = {
                x: lips.center.x,
                y: lips.center.y + botPartingHeight + biasHeightOffset
            };
            // bot of the top lips
            lips.top.bot = {
                x: lips.center.x,
                y: lips.center.y + topPartingHeight * 1.2 + biasHeightOffset,
            };

            // lower lips
            lips.out.lower = extractPoint(lips.out);
            lips.out.lower.cp1 =
                simpleQuadratic(lips.bot.top,
                    lips.out.lower,
                    0.3,
                    mods.lipBias * 0.02 - mods.lipParting * 0.005);


            // inner corner of the lips, along the curve from bot.top to out
            const sp = splitCurve(0.6 + mods.lipParting / 400, lips.bot.top, lips.out.lower);
            lips.out.in = sp.left.p2;

            // curve up to the top of the bottom lips
            lips.top.bot.cp1 =
                simpleQuadratic(lips.out.in, lips.top.bot, 0.7, -mods.lipParting * 0.005);

        }

        return [
            breakPoint,
            ex.lips.top,
            ex.lips.tip,
            // tracePoint(ex.lips.out, 0.5),
            ex.lips.out,
            ex.lips.bot,
            ex.lips.bot.top,
            ex.lips.out.in,
            ex.lips.top.bot,
            ex.lips.top,
            endPoint,
        ];
    }
}


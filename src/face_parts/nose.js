import {FacePart} from "./face_part";
import {Layer} from "../util/canvas";
import {ShadingPart} from "../draw/shading_part";
import {
    clamp,
    simpleQuadratic,
    breakPoint,
    continueCurve,
    adjust,
    endPoint,
    reflect,
} from "drawpoint";

class Nose extends FacePart {
    constructor(...data) {
        super({
            loc: "nose",
        }, ...data);
    }
}


class NoseHumanShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+nose",
            layer: Layer.BELOW_HAIR,
        }, ...data);
    }

    calcDrawPoints(ex, mods) {
        const nose = ex.nose;
        const top = nose.top;
        const bot = nose.bot;

        // draw from top to bottom and then bottom to top
        // bot.cp1 = simpleQuadratic(top, bot, 0.8, 0.3);
        bot.cp1 = {
            x: top.x,
            y: top.y - mods.noseLength * 0.05
        };
        bot.cp2 = {
            x: bot.x + 0.6 + mods.noseRoundness * 0.1,
            y: bot.y + mods.noseLength * 0.02
        };

        const out = adjust(nose.out, 0, 0);
        out.cp1 = simpleQuadratic(bot, out, 0.5, mods.nostrilSize * 0.1);

        const nostrilRatio = clamp(mods.nostrilSize * 0.01 - mods.noseRidgeHeight * 0.02,
            0,
            0.3);
        const mid = {
            x: bot.x * nostrilRatio + out.x * (1 - nostrilRatio),
            y: bot.y + 1 + mods.nostrilSize * 0.05
        };
        mid.cp1 = {
            x: out.x + mods.nostrilSize * 0.02,
            y: out.y + mods.nostrilSize * 0.05,
        };
        mid.cp2 = {
            x: mid.x + 0.6,
            y: mid.y - 0.3,
        };

        top.cp1 = continueCurve(out, mid, mods.nostrilSize * 0.1);
        top.cp2 = {
            x: clamp(top.x + 0.1 + mods.noseWidth * 0.06 + mods.noseRidgeHeight * 0.1 -
                     mods.noseRoundness * 0.1,
                0,
                100),
            y: top.y + 0.2
        };

        return [top, bot, out, mid, top];
    }
}


export class NoseHuman extends Nose {
    constructor(...data) {
        super({
            shadingParts: [NoseHumanShading]
        }, ...data);
    }

    fill(ignore, ex) {
        return ex.baseStroke;
    }

    clipFill() {
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            let nose = ex.nose = {};
            let h = this.faceLength * 0.1;

            // nostrils
            nose.center = {
                x: 0,
                y: ex.skull.y - h * 0.65 + mods.noseHeight * 0.1 + this.faceFem * 0.02
            };

            nose.in = {
                x: nose.center.x + mods.noseWidth * 0.037 - this.faceFem * 0.007 +
                   mods.nostrilSize * 0.01,
                y: nose.center.y - 0.3
            };
            nose.out = {
                x: nose.center.x + mods.noseWidth * 0.043 - this.faceFem * 0.017 +
                   0.25 + mods.nostrilSize * 0.08,
                y: nose.center.y + 0.3
            };

            let nostrilVisibility = clamp(0.5 - this.faceFem * 0.005, 0, 0.4);
            nose.out.cp1 =
                simpleQuadratic(nose.in, nose.out, 0.5, nostrilVisibility);
            nose.in.cp1 =
                simpleQuadratic(nose.out, nose.in, 0.5, nostrilVisibility);

            // normally reflect for the other nostril
            nose.left = {};
            nose.left.in = reflect(nose.in);
            nose.left.out = reflect(nose.out);
            nose.left.in.cp1 = reflect(nose.in.cp1);
            nose.left.out.cp1 = reflect(nose.out.cp1);

            // nose bridge
            nose.top = {
                x: nose.center.x + mods.noseWidth * 0.01 +
                   clamp(mods.noseRoundness * 0.01, 0, 5),
                y: nose.center.y +
                   (mods.noseLength * 0.1 - mods.noseRoundness * 0.1) * h * 0.045
            };
            nose.bot = {
                x: nose.top.x,
                y: nose.center.y + 0.3
            };
        }
        return [
            breakPoint,
            ex.nose.in,
            ex.nose.out,
            ex.nose.in,
            breakPoint,
            ex.nose.left.in,
            ex.nose.left.out,
            ex.nose.left.in,
            endPoint,
        ];
    }
}

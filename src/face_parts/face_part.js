import {none} from "drawpoint";
import {Layer} from "../util/canvas";

/**
 * Base class for parts that are on the face (head)
 * @memberof module:da
 */
export class FacePart {
    constructor(...data) {
        Object.assign(this, {
            layer       : Layer.BELOW_HAIR,
            reflect     : false,
            coverConceal: [],
            uncoverable : false,
        }, ...data);
    }

    stroke() {
        return none;
    }

    fill() {
        return "inherit";
    }

    // how thick the stroke line should be
    getLineWidth() {
        return 1.5;
    }
}


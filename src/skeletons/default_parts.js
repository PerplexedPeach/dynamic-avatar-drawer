import {Skeleton} from "./skeleton";
import {Part} from "../parts/part";
import {TesticlesHuman} from "../parts/testicles";
import {PenisHeadHuman, PenisHuman} from "../parts/penis";
import {VaginaHuman} from "../parts/vagina";
import {HeadHuman} from "../parts/head";
import {NeckHuman} from "../parts/neck";
import {ArmHuman} from "../parts/arm";
import {HandHuman} from "../parts/hand";
import {TorsoHuman} from "../parts/torso";
import {LegHuman} from "../parts/leg";
import {GroinHuman} from "../parts/groin";
import {ButtHuman} from "../parts/butt";
import {ChestHuman, NipplesHuman} from "../parts/chest";
import {FeetHuman} from "../parts/feet";
import {EarsHuman} from "../face_parts/ears";
import {EyesHuman} from "../face_parts/eyes";
import {MouthHuman} from "../face_parts/mouth";
import {LipsHuman} from "../face_parts/lips";
import {NoseHuman} from "../face_parts/nose";
import {PupilHuman} from "../face_parts/pupil";
import {IrisHuman} from "../face_parts/iris";
import {EyelidHuman} from "../face_parts/eyelid";
import {EyelashHuman} from "../face_parts/eyelash";
import {BrowHuman} from "../face_parts/eyebrow";
import {
    BellyButtonOutline,
    DeltoidsOutline,
    CollarboneOutline,
    PectoralOutline,
    AbdominalOutline,
    QuadricepsOutline
} from "../decorative_parts/outline";

/**
 * Create the default body parts
 * Should be called after you finish mutating the part templates
 * But before you create any parts or players
 * compose this to add more default parts
 */
export function loadDefaultParts() {

    Skeleton.human.maleParts = [
        {
            partGroup: "parts",
            side     : null,
            part     : TesticlesHuman,
        },
        {
            partGroup: "parts",
            side     : null,
            part     : PenisHuman,
        },
        {
            partGroup: "decorativeParts",
            side     : null,
            part     : PenisHeadHuman,
        },
    ];

    Skeleton.human.femaleParts = [
        {
            partGroup: "parts",
            side     : null,
            part     : VaginaHuman,
        },
    ];
    Skeleton.human.defaultParts = [
        {
            side: null,
            part: HeadHuman
        },
        {
            side: null,
            part: NeckHuman
        },
        {
            side: Part.LEFT,
            part: ArmHuman
        },
        {
            side: Part.RIGHT,
            part: ArmHuman
        },
        {
            side: Part.LEFT,
            part: HandHuman
        },
        {
            side: Part.RIGHT,
            part: HandHuman
        },
        {
            side: null,
            part: TorsoHuman
        },
        {
            side: Part.LEFT,
            part: LegHuman
        },
        {
            side: Part.RIGHT,
            part: LegHuman
        },
        {
            side: Part.LEFT,
            part: FeetHuman
        },
        {
            side: Part.RIGHT,
            part: FeetHuman
        },
        {
            side: null,
            part: GroinHuman
        },
        {
            side: null,
            part: ButtHuman
        },
        {
            side: null,
            part: ChestHuman
        },
    ];

    // face
    Skeleton.human.defaultFaceParts = [
        {
            side: null,
            part: EarsHuman
        },
        {
            side: null,
            part: NoseHuman
        },
        {
            side: null,
            part: LipsHuman
        },
        {
            side: null,
            part: MouthHuman
        },
        {
            side: Part.LEFT,
            part: EyesHuman
        },
        {
            side: Part.RIGHT,
            part: EyesHuman
        },
        {
            side: Part.LEFT,
            part: IrisHuman
        },
        {
            side: Part.RIGHT,
            part: IrisHuman
        },
        {
            side: Part.LEFT,
            part: PupilHuman
        },
        {
            side: Part.RIGHT,
            part: PupilHuman
        },
        {
            side: Part.LEFT,
            part: EyelidHuman
        },
        {
            side: Part.RIGHT,
            part: EyelidHuman
        },
        {
            side: Part.LEFT,
            part: EyelashHuman
        },
        {
            side: Part.RIGHT,
            part: EyelashHuman
        },
        {
            side: Part.LEFT,
            part: BrowHuman
        },
        {
            side: Part.RIGHT,
            part: BrowHuman
        },
    ];

    Skeleton.human.defaultDecorativeParts = [
        {
            side: null,
            part: NipplesHuman
        },
        {
            side: null,
            part: BellyButtonOutline
        },
        {
            side: Part.LEFT,
            part: DeltoidsOutline
        },
        {
            side: Part.RIGHT,
            part: DeltoidsOutline
        },
        {
            side: null,
            part: CollarboneOutline
        },
        {
            side: null,
            part: PectoralOutline
        },
        {
            side: null,
            part: AbdominalOutline
        },
        {
            side: Part.LEFT,
            part: QuadricepsOutline
        },
        {
            side: Part.RIGHT,
            part: QuadricepsOutline
        },
    ];
}

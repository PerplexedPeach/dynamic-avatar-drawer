---
layout: page
title: Usage guide (v1.0)
permalink: /usage.1.0.html
css: syntax
group: Usage
---
<div class="toc">
 <a class="toc-link toch2" href="#load">Loading the module</a>
 <a class="toc-link toch2" href="#canvas">Working with the canvas</a>
 <a class="toc-link toch2" href="#player">Using the Player object</a>
 <a class="toc-link toch3" href="#player#create">Creating the Player</a>
 <a class="toc-link toch3" href="#player#draw">Drawing the Player</a>
 <a class="toc-link toch3" href="#player#interact">Interacting with a Player object</a>
 <a class="toc-link toch2" href="#extend">Extending Player for your Game</a>
 <a class="toc-link toch3" href="#extend#draw">Linking Game Stats to Drawing</a>
 <a class="toc-link toch2" href="#clothes">Clothing</a>
 <a class="toc-link toch2" href="#create">Create Assets</a>
 <a class="toc-link toch3" href="#create#scale">Understanding Scalability</a>
 <a class="toc-link toch3" href="#create#example">Examples and How-To</a>
 <p class="toc-caption"></p>
 <p class="toc-toggle">toggle TOC (ctrl + &#8660;)</p>
</div>

<div class="block">
<div class="text-block">
    <p>
        This is a tool for developers of text-based games to visualize characters dynamically
        based on their statistics.
        For the usage guide of a previous version, see <a href="usage.0.13.html">0.13</a>
    </p>
    <p>
        The tool comes in the form of a Javascript module named da, which
        <b>the first thing you should do is copy the entire content of da.js to the top of your
            Javascript code</b>.
    </p>
    <p>
        You need to use the module in this order:
        <ol>
        <li>extend da.Player with your own gameplay statistics</li>
        <li>link your gameplay statistics to dimension calculations</li>
        <li>[optional] create additional assets (clothing, body parts, patterns)</li>
        <li>load the da module</li>
        <li>get canvas group to draw to</li>
        <li>draw and modify players at will in your game code
        (e.g. change clothing, modify stats, add/remove body parts)</li>
    </p>
</div>

<h2 class="anchor">Loading the module <a class="anchor-link" title="permalink to section"
                                         href="#load" name="load">&para;</a></h2>
<div class="text-block">
    <p>
        Some assets require loading. You should define extensions to the library (such as
        implementing
        a combat system based on da.BodyParts) before loading. <b>You must load before calling
        any drawing methods.</b>
    </p>
</div>

{% highlight javascript %}
da.load(); {% endhighlight %}

<h2 class="anchor">Working with the canvas <a class="anchor-link" title="permalink to section"
                                              href="#canvas" name="canvas">&para;</a></h2>
<div class="text-block">
    <p>
        The canvas is where we draw to. We use a group of canvases, each representing a
        different layer (with different z-levels).
    </p>
    <p>
        The following example shows how to get and create a canvas group.
    </p>
</div>

{% highlight javascript %}
// assume in your html you had
<div id="player"></div>

// create a canvas group (since it's the first time we're getting it)
var canvasGroup = da.getCanvasGroup("player", {
// provide some styling options; width and height are particularly important
border: "1px solid black",
width: 900,
height: 1200,
});


// you can also create the canvasGroup holder dynamically without the html in place
var canvasHolder = document.createElement("div");
canvasHolder.id = "something";
// place it somewhere on the web page
document.body.appendChild(canvasHolder);
canvasGroup = da.getCanvasGroup(canvasHolder, {
width: 900,
height: 1200,
});
{% endhighlight %}


<div class="text-block">
    <p>
        When you create the canvas, the second argument is any CSS overrides you'd like to
        provide.
    </p>
    <p>
        All functions accept either the canvas group ID (a string), or the actual group element
        itself.
        Working with the group ID is probably better for more users, but anytime you want
        the canvas group itself, you can call:
    </p>
</div>

{% highlight javascript %}
// assume the canvas group has been created, a second one won't be recreated
var canvasGroup = da.getCanvasGroup("player");
{% endhighlight %}

<div class="text-block">
    <p>
        To toggle whether the canvas can be seen or not:
    </p>
</div>

{% highlight javascript %}
// hiding it and then immediately showing it again
da.hideCanvasGroup("player");
da.showCanvasGroup("player");
{% endhighlight %}


<h2 class="anchor">Using the Player object <a class="anchor-link" title="permalink to section"
                                              href="#player" name="player">&para;</a></h2>

<div class="text-block">
    <p>
        This section just shows how to create a Player object and how to draw it. Later sections
        will describe how to extend it for your own game by adding stats and changing the way
        they're drawn.
    </p>
    <p>
        The key components of a Player object which you need to know:
    </p>
    <ul>
        <li><b>gameplay statistics</b> - these are defined by you and are relevant to your game.
            physical dimensions can be partially calculated from these.
        </li>
        <li><b>physical dimensions</b> - values for each physical part of a body calculated
            from gameplay statistics, modifiers, and the Player's base dimensions.
        </li>
        <li><b>Mods</b> - modifiers either from temporary status effects, clothing worn, or
            genetic variation. 
        </li>
    </ul>
</div>

<h3 class="anchor">Creating the Player <a class="anchor-link" title="permalink to section"
                                          href="#player#create" name="player#create">&para;</a>
</h3>
<div class="text-block">
    <p>
        To create a player, use new:
    </p>
</div>
{% highlight javascript %}
// EXAMPLE creating a specific Player object (either the PC or an NPC)
var PC = new da.Player({
    name : "HAL 9000",
    occupation : "Pod Bay Opener",
    // provide specific values here to override the default ones set
    age : 26,
    // base physical dimensions
    basedim: {
        armThickness: 60,
        height: 165,
        breastSize: 10,
        hairLength: 60,
        skin: -1,
        upperMuscle: 5,
        waistWidth: 105,
        lipSize: 23,
        faceFem: 40,
        // can have arbitrary expressions in here
        hairLightness: 30 - 12,
        hairSaturation: 50 - 12,
        legFem: 22,
        lowerMuscle: 10,
        hipWidth: 130,
        penisSize: 50,
        legLength: 100,
        shoulderWidth: 70,
    },

    // overriding body parts
    parts: [
        da.Part.create(null, da.Part.VaginaHuman),
        //            da.Part.create(null, da.Part.TesticlesHuman),
        //            da.Part.create(null, da.Part.PenisHuman),
    ],
    faceParts: [],
    decorativeParts: [
        //            da.Part.create(null, da.Part.PenisHeadHuman),
        da.Part.create(null, da.Part.BeautyMark),
    ],
    Mods: {
        browBotCurl: 6,
        eyeTilt: 5,
        eyeTopSize: 0,
        lipTopCurve: 30,
        lipTopSize: 10,
        lipBotSize: 0,
        lipWidth: -100,
        lipCupidsBow: -10,
        breastPerkiness: 4,
        eyeBotSize: 4,
        arousal: 0,
    },

    // overriding clothing (default to simple red underwear)
    clothes: [
        da.Clothes.create(da.Clothes.Bra, da.Materials.sheerFabric),
        da.Clothes.create(da.Clothes.Panties, da.Materials.sheerFabric)
    ],

});
{% endhighlight %}

<div class="text-block">
    <p>
        You could also randomly generate a character with a femininity bias between -1 and 1,
        with positive values being more feminine, and an indication of how random you want
        the process to be (> 0).
    </p>
</div>

{% highlight javascript %}
var fembias = 0.5;
var randomness = 0.8;
PC = da.createRandomCharacter(fembias, randomness);
{% endhighlight %}

<h3 class="anchor">Drawing the Player <a class="anchor-link" title="permalink to section"
                                         href="#player#draw" name="player#draw">&para;</a></h3>
{% highlight javascript %}
// make sure you called da.load() before this!
// assuming we got canvasGroup as above and created a player named PC 
var exports = da.draw(canvasGroup, PC);
{% endhighlight %}

<div class="text-block">
    <p>
        The draw function returns an export containing locations of <b>drawpoints</b>
        (explained later).
    </p>
</div>


<h3 class="anchor">Interacting with a Player object <a class="anchor-link"
                                                       title="permalink to section"
                                                       href="#player#interact"
                                                       name="player#interact">&para;</a></h3>
<div class="text-block">
    <p>
        You can modify gameplay stats, base dimensions, and Mods on the Player object directly.
    </p>
</div>

{% highlight javascript %}
PC.basedim.armLength += 2;
PC.fem += 1;
{% endhighlight %}

<div class="text-block">
    <p>
        You can get modified statistics and dimensions. You should always use these methods to read stats.
    </p>
</div>

{% highlight javascript %}
var totalFem = PC.get("fem");
var totalLegFem = PC.getDim("legFem");
{% endhighlight %}

<div class="text-block">
    <p>
        You can get body parts at specific locations.
    </p>
</div>

{% highlight javascript %}
var rightHand = PC.getPartInLocation("right hand");

// you can also search for parts of other part groups (decorations, face)
var rightHandDecoration = PC.getPartInLocation("right hand", PC.decorativeParts);
{% endhighlight %}

<div class="text-block">
    <p>
        You can add or remove body parts.
    </p>
</div>

{% highlight javascript %}
// create the body parts
var rightLegFur = da.Part.create(da.Part.RIGHT, da.Part.LegFur);
var rightLeg = da.Part.create(da.Part.RIGHT, da.Part.LegHuman,
        {
            stroke: da.Materials.brownFur.stroke,
            fill: da.Materials.brownFur.fill
        });
var myHoof = da.Part.create(da.Part.RIGHT, da.Part.HoofHorse,
        {
            stroke: "black",
            fill: "#392613"
        });

// replace any body parts in those loctions
var replacedPart = PC.attachPart(myHoof);
PC.attachPart(rightLeg);

// fur is a decorative part and should live in the decorativeParts
PC.attachPart(rightLegFur, PC.decorativeParts);


// chop off their hand and we'll show a stump 
PC.removePart("left hand");

// also removes their right hand
PC.removePart("right arm");

// chop off their entire leg (which will remove their feet as well)
// also will remove any decorative parts linked to these parts
PC.removePart("right leg");

// remember to redraw after modifying the player
da.draw(canvasGroup, PC);
{% endhighlight %}


<h2 class="anchor">Extending Player for your Game <a class="anchor-link"
                                                  title="permalink to section" href="#extend"
                                                  name="extend">&para;</a></h2>
<div class="text-block">
    <p>
        To add stats and Mods relevant for your game, define a property on the da.limit
        objects, which provides a statistical description of that poperty.
        You will want to connect the statistic you create to how physical dimensions are calculated,
        which is detailed in the next section.
    </p>
</div>

{% highlight javascript %}
// create additional core stat of int (intelligence) with default value of 5
// give the stat some bounds and distribution
da.statLimits["int"] = {
    low: 0,
    high: 10,
    avg: 5,
    stdev: 2.5,
    bias: 0.3
};

// you can do the same with modifiers
// as soon as you create a new stat, a new modifier will automatically be created
da.modLimits["tanned"] = {
    low:0, 
    high:1e9, 
    avg:1, 
    stdev:2, 
    bias:0
};
{% endhighlight %}

<div class="text-block">
    <p>
        You can also add non-statistic properties to every Player object by default.
    </p>
</div>

{% highlight javascript %}
// every Player will have an action stack they'll want to complete
da.Player.defaultProperties.todo = [];

// every Player will track who's their friend
da.Player.defaultProperties.friends = [];
{% endhighlight %}


<h3 class="anchor">Linking Game Stats to Drawing <a class="anchor-link"
                                                          title="permalink to section"
                                                          href="#extend#draw"
                                                          name="extend#draw">&para;</a></h3>
<div class="text-block">
    <p>
        OK so now you defined properties for your own game, but they don't do anything to affect the drawing!
        You must link your game properties to how specifc physical dimensions are calculated.
    </p>
</div>

{% highlight javascript %}
// the first argument is a string of {skeleton}.{dimension}
// base is the dimension value calculated from all functions before
da.extendDimensionCalc("human.handSize", function (base) {
    return base + this.get("int") * 2;
});
{% endhighlight %}


<h2 class="anchor">Clothing <a class="anchor-link" title="permalink to section" href="#clothes"
                               name="clothes">&para;</a></h2>

<div class="text-block">
    <p>
        All clothes is defined under da.Clothes. The system is made up of Clothing objects,
        which hold clothing state, and ClothingPart objects that do the drawing and hold no state.
        You shouldn't have to deal with ClothingPart unless you're creating your own clothing templates.
    </p>
    <p>
        Creating a piece of clothing involves instantiating a template with some overrides.
    </p>
</div>

{% highlight javascript %}
// sheerFabric is a predfined styling object
var sheerBra = da.Clothes.create(da.Clothes.Bra, da.Materials.sheerFabric);

// you can just as easily override with an object of your own
var blackBra = da.Clothes.create(da.Clothes.Bra, {stroke:"black", fill:"rgb(20,20,20)"});

// Clothing often have other properties that you can modify as you create it
var thickStrappedBra = da.Clothes.create(da.Clothes.Bra, {strapWidth: 3});
{% endhighlight %}


<div class="text-block">
<p>
    Getting, wearing, and removing clothing are all pretty straightforward.
    When you wear a piece of clothing, any other clothing occupying shared parts will be removed
    if possible and returned (see reference for API).
</p>
</div>

{% highlight javascript %}
// get an array of all clothing in this location
var chestWear = PC.getClothingInLocation("chest");

// remove everything here 
chestWear.forEach(function (clothing) {
    PC.removeClothing(clothing);
});

// wear something else
PC.wearClothing(sheerBra);
{% endhighlight %}

<div class="text-block">
    <p>
        Each coloring option (for most templates that's stroke and fill) can
        be any CSS compliant color string, such as "rgba(100, 23, 2, 0.5)", "black",
        "hsl(300, 25%, 23%)", or a function taking a contxt and the drawing exports
        and returning a CSS compliant color string.

        This means anything that can be a Canvas image pattern is also able to go here.
        However, loading and manipulating patterns is still under construction and the API
        could change signifcantly.
    </p>
</div>

<h2 class="anchor">Create Assets <a class="anchor-link"
                                                         title="permalink to section"
                                                         href="#create"
                                                         name="create">&para;</a>
</h2>
<div class="text-block">
    <p>
        An exciting part of DAD is that it's built so you can easily extend it to create your own assets
        in the form of clothing and body parts. This is an advanced usage of the tool, but
        any large scale project will probably want to go beyond the standard assets provided.
    </p>
    <p>
        If you do create custom assets, you retain copyright of it, but I encourage you to make
        it publically available so I have the opportunity to include it into the standard library
        DAD is shipped with.
    </p>
    <p>
        If you are interested in creating assets, then you should heavily consult the API 
        <a href="out/index.html">reference</a>
    </p>
</div>

<h3 class="anchor">Understanding Scalability <a class="anchor-link"
                                                         title="permalink to section"
                                                         href="#create#scale"
                                                         name="create#scale">&para;</a>
</h3>
<div class="text-block">
    <p>
        It's important to understand how DAD keeps everything to scale with body dimensions.
        The fundamental concept here is <b>drawpoints</b>. These are points exported by
        the draw method that are each in a format like:
    </p>
</div>
{% highlight javascript %}
ex.thumb.out = {
    x: units in cm relative to center of body, with center = 0,
    y: units in cm relative to floor, with floor = 0,
    cp1: optional control point (with its own x and y)
    cp2: optional second control point
}
{% endhighlight %}

<div class="text-block">
    <p>
        Rendering involves connecting sequences of drawpoints using lines, and quadratic and bezier curves.
        You can see the drawpoints and how they move in the tester provided (by toggling it on via button).
    </p>
    <p>
        You connect draw points with the da.drawPoints method, which smartly decide whether to connect using
        line, quadratic, or bezier curve depending on how many control points exist on the draw point.
    </p>
</div>
{% highlight javascript %}
// these are the points of a human leg
var humanLegDrawpoints = [
                da.extractPoint(ex.hip),
                ex.thigh.out,
                ex.knee.out,
                ex.calf.out,
                ex.ankle.out,
                // insert the sequence of draw points from your child feet part (if it exists)
                {child: "feet"},
                ex.ankle.in,
                ex.calf.in,
                ex.knee.in,
                ex.knee.intop,
                ex.thigh.in,
                ex.thigh.top,
            ];

// your body parts just have to export the sequence of draw points
// the drawpoints are parsed and reordered internally, but basically it's called like
da.drawPoint(ctx, ...humanLegDrawPoints);
{% endhighlight %}

<div class="text-block">
<p>
    cp1 and cp2 are for the curve going into the drawpoint. Having only cp1 would make it a quadratic curve,
    while having both cp1 and cp2 makes it a bezier curve.
</p>
</div>
<img src="http://i.imgur.com/GYzYdwI.png"/>

<div class="text-block">
<p>
    You can see the control points for any drawpoint by wrapping it with 
    <code>da.tracePoint(ex.ankle.out)</code> when passing it to drawPoints.
</p>
</div>


<h3 class="anchor">Examples and How-To <a class="anchor-link" title="permalink to section"
                                        href="#create#example" name="create#example">&para;</a></h3>
<div class="text-block">
    <p>
        The best way to create your own assets is to look at how existing ones are created.
        Look in the source and consult the reference.
    </p>
    <p>
        Creating new hair is very similar to creating clothing, where you're in a situation of
        simply reading and adjusting drawpoints. For body parts, you actually have to define
        and export the drawpoints.
    </p>
</div>

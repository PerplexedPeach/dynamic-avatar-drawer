---
layout: post
title:  "1.19 Focused Window and Hiding Info"
date:   2019-02-23
categories: release
---

- not render certain info [guide]({{site.baseurl}}/usage.1.0.html#player#draw)
    - name, gender
    - height, height bar
    - shoe side view
- draw focused window [guide]({{site.baseurl}}/usage.1.0.html#player#drawfocus)
    - focus on a specific part
    - draw with higher resolution


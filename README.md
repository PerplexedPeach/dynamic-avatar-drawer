[live tester / playground](http://perplexedpeach.gitlab.io/dynamic-avatar-drawer)

Function
===
Model and View for characters in Javascript-enabled games and stories.

It provides:
 - procedural drawing of characters and clothing
 - inventory management and clothing layering
 - facial expression system
 - mixing body parts (e.g. elf ears on human head)
 - missing body parts (e.g. hand was cut off)
 - smooth animation for transformations as a result of stat changes
 - interface for defining custom stats and mapping those to appearance dimensions

Usage
===
Include `da.js` in your project and call methods on the `da` module.
It's found under `public/da.js` in this repository.

For a usage guide, see the `_posts` directory of this repository (for instructions in markdown).
Alternatively build the site locally with `jekyll build`. (you'll need to install jekyll, but
the repository is already configured for jekyll builds)

For an API reference, see the `ref` directory of this repository.

Use the playground to generate live code and see their effects.

For development, you'll need to install some dependencies. First install the latest version of 
[node and npm](https://nodejs.org/en/download/)

Then open a console, navigate to the project root directory, then run
```bash
npm install
```

For development, run either

```bash
npm run build
``` 
or
```bash
npm run build_windows
```

depending on what platform you're running. This transpiles the source from ES6 down to ES5
so more browsers can use it. The resulting da.js file will be the transpiled source.
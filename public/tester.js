/* eslint no-native-reassign: "off" */
/* eslint quotes: "warn" */
/* eslint-env es6:false */
/*
 global ex da PC canvasGroup dat config presetCharacters
 */

/**
 * Extend dat.gui to have title capabilities, courtesy of
 * greginvm from http://stackoverflow.com/questions/27362914/how-to-add-tooltips-to-dat-gui
 */

/* dat.GUI copies the prototype of superclass Controller to all other controllers, so it is not enough to add it only to
 the super class as the reference is not maintained */
function eachController(fnc) {
    for (var controllerName in dat.controllers) {
        if (dat.controllers.hasOwnProperty(controllerName)) {
            fnc(dat.controllers[controllerName]);
        }
    }
}

function setTitle(v) {
    // __li is the root dom element of each controller
    if (v) {
        this.__li.setAttribute("title", v);
    } else {
        this.__li.removeAttribute("title");
    }
    return this;
}

eachController(function (controller) {
    if (!controller.prototype.hasOwnProperty("title")) {
        controller.prototype.title = setTitle;
    }
});

Object.defineProperty(dat.GUI.prototype, "removeFolder", {
    value: function (name) {
        this.__folders[name].close();
        this.__folders[name].domElement.parentNode.parentNode.removeChild(this.__folders[name].domElement.parentNode);
        this.__folders[name] = undefined;
        this.onResize();
    }
});

var gui;


// shortcut keys
document.addEventListener("keydown", function (e) {
    if (e.getModifierState("Control") === false) {
        return;
    }
    switch (e.key) {
    case "q":
        toggleDrawPoints();
        e.preventDefault();
        break;
    case "c":
        if (e.getModifierState("Alt") === true) {
            generateRandomCharacter();
            e.preventDefault();
        }
        break;
    case "s":
        e.preventDefault();
        e.stopPropagation();
        if (e.getModifierState("Shift") === true) {
            PC = loadPlayer(PC);
        } else {
            savePlayer(PC);
        }
        break;
    case "o":
        e.preventDefault();
        toggleGUI(gui);
        break;
    default:
        return;
    }
}, true);

var guiClosed = true;

function toggleGUI(gui) {
    var baseFolders = gui.__folders;
    for (var folder in baseFolders) {
        if (baseFolders.hasOwnProperty(folder)) {
            recursivelyToggleGUI(baseFolders[folder], !guiClosed);
        }
    }
    guiClosed = !guiClosed;
}

function recursivelyToggleGUI(item, closed) {
    item.closed = closed;
    var folders = item.__folders;
    for (var folder in folders) {
        if (folders.hasOwnProperty(folder)) {
            recursivelyToggleGUI(folders[folder], closed);
        }
    }
}

var drawdp = false;

function toggleDrawPoints() {
    drawdp = !drawdp;
    if (drawdp) {
        drawDrawPoints(ex.ctx, ex);
    } else {
        dp.length = 0;
        window.ex = da.draw(canvasGroup, PC, Object.assign({}, config, {passThrough: true}));
    }
}

/** get all draw points */
function getDrawPoints(ex, levelToGo, prefix) {
    if (typeof ex !== "object" || levelToGo < 1) {
        return null;
    }
    // can't hold any draw points
    var pts = [];
    for (var name in ex) {
        if (!ex.hasOwnProperty(name) || name === "cp1" || name === "cp2") {
            continue;
        }

        var p = ex[name];
        if (!p || typeof p !== "object") {
            continue;
        }
        if (p.hasOwnProperty("x") && p.hasOwnProperty("y")) {
            pts.push(Object.assign({name: prefix ? prefix + "." + name : name}, p));
        }

        // additionally there are subpoints under this location
        var innerdrawpts = getDrawPoints(p, levelToGo - 1, name);
        if (innerdrawpts) {
            pts.extend(innerdrawpts);
        }

    }
    return pts;
}


function drawExportedPoints(ctx, pts, labelColor) {
    ctx.beginPath();
    for (var i = 0, len = pts.length; i < len; ++i) {
        var p = pts[i];
        ctx.moveTo(p.x, p.y);
        ctx.arc(p.x, p.y, 0.5, 0, 2 * Math.PI, true);
    }
    ctx.strokeStyle = labelColor;
    ctx.stroke();
}

// list of all draw points
var dp = [];

function drawDrawPoints(ctx, ex) {
    if (!drawdp) {
        return;
    }
    dp = getDrawPoints(ex, 4);
    drawExportedPoints(ctx, dp, "black");
}


var textCanvas = document.createElement("canvas");
textCanvas.width = 200;
textCanvas.height = 24;
var textCtx = textCanvas.getContext("2d");
textCtx.font = "24px consolas";
textCtx.scale(1, -1);
textCtx.fillStyle = "black";
textCtx.textBaseline = "bottom";

function labelDrawPoint(ctx, pt) {
    // display name to the nearest drawpoint to click point
    var mindist = 1000;
    var minp = null;
    var dx = 0;
    var dy = 0;
    // check against all draw points
    for (var i = 0; i < dp.length; ++i) {
        var dist = Math.pow(pt.x - dp[i].x, 2) + Math.pow(pt.y - dp[i].y, 2);
        if (dist < mindist) {
            mindist = dist;
            minp = dp[i];
            dx = da.roundToDec(pt.x - dp[i].x, 1);
            dy = da.roundToDec(pt.y - dp[i].y, 1);
        }
    }
    if (minp) {
        textCtx.clearRect(0, 0, textCanvas.width, -textCanvas.height);
        textCtx.beginPath();
        textCtx.fillText(minp.name, 0, 0);
        ctx.drawImage(textCanvas, minp.x + 2, minp.y, textCanvas.width / 4, textCanvas.height / 4);
        console.log(minp.name, dx, dy);
    }
}

var canvas = canvasGroup.firstChild;
canvas.oncontextmenu = function (e) {
    e.preventDefault();
    return false;
};
canvas.addEventListener("mousedown", function (e) {
    e.preventDefault();
    var x;
    var y;
    if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
    } else {
        x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;

    var pt = ex.ctx.transformPoint(x, y);


    if (drawdp) {
        switch (e.which) {

        case 1: {
            break;
        }
        case 2: {
            // clear
            da.draw(canvasGroup, PC, Object.assign({}, config, {passThrough: true}));
            break;
        }
        case 3: {
            break;
        }
        default:
            console.log("what");
        }

        ex.ctx.strokeStyle = "black";
        drawDrawPoints(ex.ctx, ex);
        labelDrawPoint(ex.ctx, pt);

    } else {
        ex.ctx.beginPath();
        ex.ctx.strokeStyle = "black";
        ex.ctx.arc(pt.x, pt.y, 1, 0, Math.PI * 2, true);
        console.log(da.roundToDec(pt.x), da.roundToDec(pt.y), "in cm ->",
            da.roundToDec(da.cucm(pt.x)),
            da.roundToDec(da.cucm(pt.y)),
            "h*" + (da.cucm(pt.y) / PC.dim.height).toFixed(4));
        ex.ctx.stroke();
        ex.ctx.fill();
    }
    return false;
});

function getSortedKeys(obj) {
    var keys = [];
    for (var k in obj) {
        if (obj.hasOwnProperty(k)) {
            keys.push(k);
        }
    }
    keys.sort(function (a, b) {
        "use strict";
        var aa = obj[a].linkedPart || "";
        var bb = obj[b].linkedPart || "";
        if (aa === bb) {
            return (a > b) ? 1 : -1;
        }
        return (aa > bb) ? 1 : -1;
    });
    return keys;
}

function redrawPage(PC, calcPhysique) {

    ex = da.draw(canvasGroup, PC, Object.assign({}, config, !calcPhysique));

    drawDrawPoints(ex.ctx, ex);

    return PC;
}


function savePlayer(PC) {
    // `localStorage.setItem()` may throw an exception, so try/catch.
    try {
        console.log(da.serialize(PC));
        localStorage.setItem("DADsavedChar", da.serialize(PC));
    } catch (e) {
        // Report the error in some fashion.
    }
}

function loadPlayer(PC) {
    var serializedPC = localStorage.getItem("DADsavedChar");

    if (serializedPC) {
        PC = da.deserialize(serializedPC);
        // would've doubly applied worn clothing's modifiers
        PC.clothes.forEach(function (clothing) {
            da.removeMods(PC.Mods, clothing.Mods);
        });
        PC = redrawPage(PC, true);
        generateGUI(PC);
        console.log("loaded", PC);
    } else {
        console.log("no player saved!");
    }
    return PC;
}


// interactive creation
var interactiveCreation = {
    Presets      : "",
    randomness   : 1,
    "female bias": 1,
};
var creationGUI, parametersFolder, prototypesFolder;
var instanceParameters = {};
var defaultInstanceParameters = {};

// facial _expression selection
var expressions = [];
// var myExpression = document.getElementById("expressions");
for (var expression in da.Expression) {
    if (da.Expression.hasOwnProperty(expression) &&
        da.Expression[expression].hasOwnProperty("Mods")) {
        expressions.push(expression);
    }
}


function generateRandomCharacter() {
    // save what clothes we're wearing
    var currentlyWorn = PC.clothes;
    // make new character
    // higher value creates more feminine characters in general
    PC = da.createRandomCharacter(interactiveCreation["female bias"],
        interactiveCreation.randomness);
    // don't do this in your code, actually wear the clothes with PC.changeClothes() unless you know what you're doing
    currentlyWorn.forEach(function (clothing) {
        PC.wearClothing(clothing);
    });
    PC = redrawPage(PC, true);
    generateGUI(PC);
}

var patterns = da.listAvailablePatterns();
var sides = [null, "left", "right"];
var modifiers = [];
for (var mod in da.modLimits) {
    if (da.modLimits.hasOwnProperty(mod)) {
        modifiers.push(mod);
    }
}
modifiers.sort();
var baseDimensions = [];
for (var dim in da.baseDimDesc.human) {
    if (da.baseDimDesc.human.hasOwnProperty(dim)) {
        baseDimensions.push(dim);
    }
}
baseDimensions.sort();
var stats = [];
for (var stat in da.statLimits) {
    if (da.statLimits.hasOwnProperty(stat)) {
        stats.push(stat);
    }
}
stats.sort();

var interactiveModal = document.getElementById("interactive_modal");
var closeInteractiveModal = document.getElementById("interactive_modal_top_bar");
var interactiveGUIArea = document.getElementById("creation_selection");
var prototypeText = "";
var prototypeSelectedText = "";
var actionText = "";

var evalText = document.getElementById("eval_text");
evalText.spellcheck = false;

function createEvalText() {
    var parameterText = propertiesToText(instanceParameters, defaultInstanceParameters);
    if (parameterText.length > 2) {
        parameterText = ", " + parameterText;
    }
    evalText.value = prototypeText + prototypeSelectedText + parameterText + ");\n\n" + actionText;
    // evalText.style.height = "5px";
    evalText.style.height = (evalText.scrollHeight) + 2 + "px";
}

function propertiesToText(obj, comparisonObject, indent = 0) {
    var indentSpacing = " ".repeat(indent * 2);
    var indentSpacingProperties = " ".repeat((indent + 1) * 2);
    var parameterText = "{";
    for (var param in obj) {
        if (obj.hasOwnProperty(param)) {
            if (obj[param] === comparisonObject[param]) {
                continue;
            }
            var value;
            if (typeof obj[param] === "number" || obj[param] === "null" ||
                (typeof obj[param] === "string" &&
                 (obj[param].endsWith(")") && obj[param].startsWith("da.") ||
                  !isNaN(obj[param])))) {
                value = obj[param];
            } else if (typeof obj[param] === "object") {
                value = propertiesToText(obj[param], {}, indent + 1);
                // nothing
                if (value.length < 3) {
                    continue;
                }
            } else {
                value = "\"" + obj[param] + "\"";
            }
            parameterText += "\n" + indentSpacingProperties + param + ": " + value + ",";
        }
    }
    if (parameterText.length > 2) {
        parameterText += "\n" + indentSpacing + "}";
    } else {
        parameterText = "";
    }
    return parameterText;
}

function evalAction() {
    "use strict";
    console.log(eval("(function(){" + evalText.value + "}());"));
    redrawAvatar();
}

closeInteractiveModal.addEventListener("click", function () {
    interactiveModal.style.visibility = "hidden";
    prototypeText = "";
    prototypeSelectedText = "";
    actionText = "";
    evalText.value = "";
});


interactiveCreation["Wear Clothing"] = function () {
    evalText.value = "";
    actionText = "PC.wearClothing(myClothing);";
    // bring up modal
    displayInteractiveModal(guiMenuItem, "left");


    // first need to select prototype

    prototypesFolder = creationGUI.addFolder("Part Prototypes");
    prototypesFolder.open();

    var clothingPrototypes = [[], []];
    for (var clothing in da) {
        // is a clothing class
        if (da.hasOwnProperty(clothing) &&
            typeof da[clothing] === "function" &&
            da[clothing].prototype instanceof da.Clothing) {
            var level = 0;

            var pro = Object.getPrototypeOf(da[clothing].prototype);
            while (pro !== da.Clothing.prototype) {
                ++level;
                pro = Object.getPrototypeOf(pro);
            }
            clothingPrototypes[level].push(clothing);
        }
    }

    clothingPrototypes.forEach(function (clothingPrototypesLevel) {
        clothingPrototypesLevel.sort();

        clothingPrototypesLevel.forEach(function (clothing) {
            "use strict";
            var pro = da[clothing].prototype;
            // put in correct folder
            var prototypeChain = [];
            while (pro !== da.Clothing.prototype) {
                prototypeChain.push(pro.constructor.name);
                pro = Object.getPrototypeOf(pro);
            }

            var parentFolder = prototypesFolder;
            for (var i = prototypeChain.length - 1; i > 0; --i) {
                if (parentFolder.__folders.hasOwnProperty(prototypeChain[i]) === false) {
                    parentFolder = parentFolder.addFolder(prototypeChain[i]);
                } else {
                    parentFolder = parentFolder.__folders[prototypeChain[i]];
                }
            }

            // now to add the base concrete class which should always be prototypeChain[0]
            var concreteClass = prototypeChain[0];
            if (da[concreteClass].prototype.partPrototypes) {
                var tempObj = {};
                tempObj[concreteClass] = listPrototypeParameters.bind(null,
                    da[concreteClass],
                    da.Clothes.create,
                    applyClothing, addPartParameters);
                parentFolder.add(tempObj, concreteClass);
            } else {
                // else is another folder
                parentFolder.addFolder(prototypeChain[0]);
            }
        });
    });

    prototypeText = "var myClothing = da.Clothes.create(da.";

    function applyClothing(clothing) {
        "use strict";
        PC.wearClothing(clothing);
        redrawAvatar();
    }

    function addPartParameters(parametersFolder, recreateInstance) {
        // allow modifiers
        instanceParameters["Mods"] = {};
        var modsFolder = parametersFolder.addFolder("Body Modifiers");
        modsFolder.add({"Add modifier": ""}, "Add modifier", modifiers)
        .onChange(addModifier.bind(null, modsFolder, recreateInstance, instanceParameters["Mods"]));

        // force recreation to take into account side (which all parts need in their instantiation)
        recreateInstance();
    }

    function addModifier(modsFolder, recreateInstance, mods, newMod) {
        var modDesc = da.modLimits[newMod];
        mods[newMod] = modDesc.avg;
        var mod = modsFolder.add(mods, newMod);
        if (modDesc.low < -1e3 || modDesc.high > 1e3) {
            mod = mod.min(-20).max(40);
        } else {
            mod = mod.min(modDesc.low).max(modDesc.high);
        }
        mod.onChange(recreateInstance);
    }
};


interactiveCreation["Remove Clothing"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    var menuItems = {};
    var clothingPrototypes = {};
    PC.clothes.forEach(function (clothing) {
        clothingPrototypes[clothing.constructor.name] = removeSpecificClothing.bind(null, clothing);
        menuItems[clothing.constructor.name] =
            creationGUI.add(clothingPrototypes, clothing.constructor.name);
    });

    function removeSpecificClothing(clothing) {
        // PC.removeClothing(clothing);
        creationGUI.remove(menuItems[clothing.constructor.name]);
        evalText.value = "PC.removeClothing(PC.clothes[" + PC.clothes.indexOf(clothing) + "]);";
        evalAction();
    }

    prototypeText = "";
};


interactiveCreation["Facial Expression"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    var expressionObj = {
        expression: "neutral",
        degree    : 1
    };
    creationGUI.add(expressionObj, "expression", expressions).onChange(applyExpression);
    creationGUI.add(expressionObj, "degree", 0, 2).onChange(applyExpression);


    function applyExpression() {
        evalText.value =
            "PC.applyExpression(da.Expression.create(da.Expression." + expressionObj.expression +
            ", " + expressionObj.degree + "));";
        evalAction();
    }

    prototypeText = "";
};

interactiveCreation["Strip All"] = function () {
    "use strict";
    evalText.value = "PC.removeAllClothing();";
    // bring up modal
    displayInteractiveModal(guiMenuItem);
    evalAction();
};


interactiveCreation["Attach Bodypart"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    // first need to select prototype
    prototypesFolder = creationGUI.addFolder("Part Prototypes");
    prototypesFolder.open();

    var partPrototypes = [[], [], [], []];
    for (var part in da) {
        // is a clothing class
        if (da.hasOwnProperty(part) &&
            typeof da[part] === "function" &&
            (Object.getPrototypeOf(da[part].prototype) instanceof da.DecorativePart ||
             Object.getPrototypeOf(da[part].prototype) instanceof da.FacePart ||
             Object.getPrototypeOf(da[part].prototype) instanceof da.BodyPart)) {
            var level = 0;

            var pro = Object.getPrototypeOf(da[part].prototype);
            while (pro !== Object.prototype) {
                ++level;
                pro = Object.getPrototypeOf(pro);
            }
            if (level > partPrototypes.length) {
                partPrototypes[level] = [];
            }
            partPrototypes[level].push(part);
        }
    }

    partPrototypes.forEach(function (partPrototypeLevels) {
        partPrototypeLevels.sort();

        partPrototypeLevels.forEach(function (part) {
            "use strict";
            var pro = da[part].prototype;
            // put in correct folder
            var prototypeChain = [];
            while (pro !== Object.prototype) {
                prototypeChain.push(pro.constructor.name);
                pro = Object.getPrototypeOf(pro);
            }

            var parentFolder = prototypesFolder;
            for (var i = prototypeChain.length - 1; i > 0; --i) {
                if (parentFolder.__folders.hasOwnProperty(prototypeChain[i]) === false) {
                    parentFolder = parentFolder.addFolder(prototypeChain[i]);
                } else {
                    parentFolder = parentFolder.__folders[prototypeChain[i]];
                }
            }

            // now to add the base concrete class which should always be prototypeChain[0]
            var concreteClass = prototypeChain[0];
            if (da[concreteClass].prototype.hasOwnProperty("calcDrawPoints")) {
                var tempObj = {};
                tempObj[concreteClass] = listPrototypeParameters.bind(null,
                    da[concreteClass],
                    createBodyPart,
                    applyBodyPart, addPartParameters);
                parentFolder.add(tempObj, concreteClass);
            } else {
                // else is another folder
                parentFolder.addFolder(prototypeChain[0]);
            }
        });
    });

    prototypeText = "var myPart = da.Part.create(da.Part.";

    function createBodyPart(Prototype) {
        "use strict";
        return da.Part.create(Prototype, instanceParameters);
    }

    function applyBodyPart(bodyPart) {
        "use strict";
        actionText = "PC.attachPart(myPart";
        if (bodyPart instanceof da.DecorativePart) {
            actionText += ", PC.decorativeParts";
            PC.attachPart(bodyPart, PC.decorativeParts);
        } else if (bodyPart instanceof da.FacePart) {
            actionText += ", PC.faceParts";
            PC.attachPart(bodyPart, PC.faceParts);
        } else {
            PC.attachPart(bodyPart);
        }
        actionText += ");";
        redrawAvatar();
    }

    function addPartParameters(parametersFolder, recreateInstance) {
        // add side parameter
        instanceParameters["side"] = "right";
        parametersFolder.add(instanceParameters, "side", sides).onChange(recreateInstance);

        // allow modifiers
        instanceParameters["Mods"] = {};
        var modsFolder = parametersFolder.addFolder("Body Modifiers");
        modsFolder.add({"Add modifier": ""}, "Add modifier", modifiers)
        .onChange(addModifier.bind(null, modsFolder, recreateInstance, instanceParameters["Mods"]));

        // force recreation to take into account side (which all parts need in their instantiation)
        recreateInstance();
    }

    function addModifier(modsFolder, recreateInstance, mods, newMod) {
        var modDesc = da.modLimits[newMod];
        mods[newMod] = modDesc.avg;
        var mod = modsFolder.add(mods, newMod);
        if (modDesc.low < -1e3 || modDesc.high > 1e3) {
            mod = mod.min(-20).max(40);
        } else {
            mod = mod.min(modDesc.low).max(modDesc.high);
        }
        mod.onChange(recreateInstance);
    }

};


function listPrototypeParameters(prototype, creator, applier, addMoreParameters) {
    if (creationGUI.__folders.hasOwnProperty("Part Parameters")) {
        creationGUI.removeFolder("Part Parameters");
    }
    parametersFolder = creationGUI.addFolder("Part Parameters");

    prototypesFolder.close();
    parametersFolder.open();

    prototypeSelectedText = prototype.name;

    var instance = creator(prototype);

    var excludedParameters = [
        "side",
        "layer",
        "loc",
        "reflect",
        "muscleGroup",
        "partPrototypes",
        "clothingLayer",
        "aboveParts",
        "belowParts",
        "parts",
        "shadingParts",
        "childParts",
        "parentPart",
        "coverConceal",
        "uncoverable",
    ];

    // consider all properties (inherited) of object
    var parameters = [];
    for (var param in instance) {
        if (excludedParameters.indexOf(param) > -1) {
            continue;
        }
        parameters.push(param);
    }
    parameters.sort();

    var colourParameters = ["fill", "stroke"];
    instanceParameters = {};
    defaultInstanceParameters = {};
    parameters.forEach(function (param) {
        if (colourParameters.indexOf(param) > -1) {
            return;
        }
        var parameter;

        if (typeof instance[param] === "object") {
            return;
        } else if (param.endsWith("Pattern") || param.endsWith("Fill") ||
                   param.endsWith("Stroke")) {
            colourParameters.push(param);
            return;
        } else {
            defaultInstanceParameters[param] =
                instanceParameters[param] = da.clone(instance[param]);
            parameter = parametersFolder.add(instanceParameters, param).listen();
        }
        if (param === "cling") {
            parameter = parameter.min(0).max(1);
        } else if (param.endsWith("Coverage")) {
            parameter = parameter.min(-0.5).max(1);
        } else if (param.endsWith("Width") || param.endsWith("Height")) {
            parameter = parameter.min(-20).max(20);
        }
        parameter.onChange(recreateInstance);
    });

    colourParameters.forEach(function (colour) {
        "use strict";
        var colourOptions = {
            alpha      : 1,
            pattern    : "",
            patternSize: da.IMAGE_MAXSIZE,
        };

        // allow solid colour as well as patterns
        if (typeof instance[colour] === "function") {
            colourOptions["solid colour"] =
                instance[colour].call(instance.defaultProperties, ex.ctxGroup[0], ex);
        } else {
            colourOptions["solid colour"] = instance[colour];
        }

        // convert to hex for dat.gui to parse
        ex.ctx.strokeStyle = colourOptions["solid colour"];
        colourOptions["solid colour"] = ex.ctx.strokeStyle;

        var colourFolder = parametersFolder.addFolder(colour);

        try {
            var basicColourFolder = colourFolder.addFolder("Basic Colour");
            basicColourFolder.addColor(colourOptions, "solid colour")
            .onChange(changeBasicColour.bind(null, colour, colourOptions));
            basicColourFolder.add(colourOptions, "alpha", 0, 1)
            .onChange(changeBasicColour.bind(null, colour, colourOptions));
        } catch (e) {
            colourFolder.removeFolder("Basic Colour");
        }

        var patternColourFolder = colourFolder.addFolder("Pattern");
        patternColourFolder.add(colourOptions, "pattern", patterns)
        .onChange(changePattern.bind(null, colour, colourOptions));
        patternColourFolder.add(colourOptions, "patternSize", 20, 500)
        .onChange(changePattern.bind(null, colour, colourOptions));

    });

    createEvalText();

    if (addMoreParameters) {
        addMoreParameters(parametersFolder, recreateInstance);
    }

    applier(instance);

    function recreateInstance() {
        var newInstance = creator(prototype, instanceParameters);
        // update instanceParameters object
        parameters.forEach(function (param) {
            if (colourParameters.indexOf(param) > -1) {
                return;
            }

            if (typeof instance[param] !== "object") {
                instanceParameters[param] = newInstance[param];
            }
        });
        applier(newInstance);
        createEvalText();
        redrawAvatar();
    }

    function changeBasicColour(colourName, colourOptions) {
        instanceParameters[colourName] = instance[colourName] =
            da.adjustColor(colourOptions["solid colour"], {a: colourOptions.alpha});

        createEvalText();
        redrawAvatar();
    }

    function changePattern(colourName, colourOptions) {
        instanceParameters[colourName] =
            "da.getPattern(\"" + colourOptions.pattern + "\", " + colourOptions.patternSize + ")";
        instance[colourName] = da.getPattern(colourOptions.pattern, colourOptions.patternSize);
        createEvalText();
        redrawAvatar();
    }

}

function displayInteractiveModal(element, relative = "left") {
    if (relative.indexOf("right") > -1) {
        interactiveModal.style.left = element.offsetLeft + element.offsetWidth + 10 + "px";
    } else if (relative.indexOf("left") > -1) {
        interactiveModal.style.right = document.body.offsetWidth - element.offsetLeft + 20 + "px";
    } else {
        interactiveModal.style.left = element.offsetLeft - 10 + "px";
    }

    interactiveModal.style.visibility = "visible";
    interactiveGUIArea.innerHTML = "";

    // create GUI
    if (creationGUI) {
        creationGUI.destroy();
    }
    creationGUI = new dat.GUI({autoPlace: false});
    interactiveGUIArea.appendChild(creationGUI.domElement);

    interactiveModal.focus();
}

interactiveCreation["Get Dimensions"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    var dimDesc = da.baseDimDesc[PC.skeleton];
    evalText.value = "basedim : {";
    for (var dim in PC.basedim) {
        if (PC.basedim.hasOwnProperty(dim) && dimDesc.hasOwnProperty(dim) &&
            PC.basedim != dimDesc[dim].avg) {
            evalText.value += "\n\t" + dim + ": " + PC.basedim[dim] + ",";
        }
    }
    evalText.value += "\n},";

    evalText.focus();
    evalText.select();
};

interactiveCreation["Get Stats"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    for (var stat in da.statLimits) {
        if (da.statLimits.hasOwnProperty(stat)) {
            evalText.value += "\n\t" + stat + ": " + PC[stat] + ",";
        }
    }

    evalText.focus();
    evalText.select();
};

interactiveCreation["Get Modifiers"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    var modDesc = da.modLimits;
    evalText.value = "Mods: {";
    for (var mod in PC.Mods) {
        if (PC.Mods.hasOwnProperty(mod) && modDesc.hasOwnProperty(mod) &&
            PC.Mods[mod] !== modDesc[mod].avg) {
            evalText.value += "\n\t" + mod + ": " + PC.Mods[mod] + ",";
        }
    }
    evalText.value += "\n},";

    evalText.focus();
    evalText.select();

};


interactiveCreation["Save"] = function () {
    savePlayer(PC);
};
interactiveCreation["Load"] = function () {
    PC = loadPlayer(PC);
};

interactiveCreation["Create in Code"] = function () {
    "use strict";
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    var PCserialized = da.serialize(PC);
    evalText.value = "da.deserialize('" + PCserialized + "');";

    evalText.focus();
    evalText.select();

};

interactiveCreation["Drawpoints"] = toggleDrawPoints;
interactiveCreation["Randomize Character"] = generateRandomCharacter;

function updateDisplay(guiElement) {
    for (var i in guiElement.__controllers) {
        var controller = guiElement.__controllers[i];
        if (controller.updateDisplay) {
            controller.updateDisplay();
        }
    }
    for (var f in guiElement.__folders) {
        updateDisplay(guiElement.__folders[f]);
    }
}

// rendering at 60FPS
var redrawTask = null;

function redrawAvatar() {
    "use strict";

    redrawTask = function () {
        window.ex = da.draw(canvasGroup, PC, config);
        if (ex instanceof Promise) {
            ex.then(function (exports) {
                window.ex = exports;
                drawDrawPoints(ex.ctx, ex);
                updateDisplay(gui);
            });
        } else {
            drawDrawPoints(ex.ctx, ex);
            updateDisplay(gui);
        }
    };
}

function redrawFrame() {
    if (redrawTask) {
        redrawTask();
        redrawTask = null;
    }
    requestAnimationFrame(redrawFrame);
}

// kickstart listener
redrawFrame();

interactiveCreation["Wield Item"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    instanceParameters = {
        layer: da.Layer.BASE,
        name : "",
        src  : "link (e.g. res/shield.svg or http://i.imgur.com/WICGfj1.png)",
        width: 80,
        x    : 0,
        y    : 0
    };
    defaultInstanceParameters = Object.assign({}, instanceParameters);

    creationGUI.add(instanceParameters, "name").onChange(createEvalText);
    creationGUI.add(instanceParameters, "src").onChange(createEvalText);
    creationGUI.add(instanceParameters, "layer", da.Layer).onChange(createEvalText);
    creationGUI.add(instanceParameters, "width", 20, 200).onChange(createEvalText);
    creationGUI.add(instanceParameters, "x", -180, 100).onChange(createEvalText);
    creationGUI.add(instanceParameters, "y", -100, 220).onChange(createEvalText);
    creationGUI.add({"Wield": rewieldItem}, "Wield");

    prototypeText = "var myItem = da.Items.create(da.Item";
    actionText = "PC.wieldItem(myItem);";

    var item;

    function rewieldItem() {
        createEvalText();
        if (item) {
            PC.removeItem(item);
        }
        if (instanceParameters.src === defaultInstanceParameters.src) {
            alert("Item needs a proper source (can be local or online)");
            return;
        }
        if (instanceParameters.name === defaultInstanceParameters.name) {
            alert("An item needs a name");
            return;
        }
        item = da.Items.create(da.Item, instanceParameters);
        PC.wieldItem(item);
        redrawAvatar();
    }
};

interactiveCreation["Remove Item"] = function () {
    evalText.value = "";
    // bring up modal
    displayInteractiveModal(guiMenuItem);

    var menuItems = {};
    var items = {};
    PC.items.forEach(function (item) {
        items[item.name] = removeSpecificItem.bind(null, item);
        menuItems[item.name] = creationGUI.add(items, item.name);
    });

    function removeSpecificItem(item) {
        creationGUI.remove(menuItems[item.name]);
        evalText.value = "PC.removeItem(PC.items[" + PC.items.indexOf(item) + "]);";
        evalAction();
    }
};


interactiveCreation["Animate Transformation"] = function () {
    evalText.value = "";
    prototypeText = "function drawAndExport() {\n\
    da.draw(window.canvasGroup, PC, window.config).then(function (exports) {\n\
        window.ex = exports;\n\
    });\
    \n}\n\n" + "var transformation = da.createTransformation(PC, drawAndExport";

    // bring up modal
    displayInteractiveModal(guiMenuItem);

    actionText = "da.transformAndShow(transformation, 5000);";
    creationGUI.add({"duration (ms)": 5000}, "duration (ms)", 100, 20000)
    .onChange(function (newDuration) {
        actionText = "da.transformAndShow(transformation, " + newDuration + ");";
        createEvalText();
    });

    instanceParameters.basedim = {};
    var dimsFolder = creationGUI.addFolder("Body Dimensions");
    dimsFolder.add({" + Add base dim": ""}, " + Add base dim", baseDimensions)
    .onChange(addBaseDim);

    instanceParameters.Mods = {};
    var modsFolder = creationGUI.addFolder("Body Modifiers");
    modsFolder.add({" + Add modifier": ""}, " + Add modifier", modifiers)
    .onChange(addModifier);

    var statsFolder = creationGUI.addFolder("Statistic");
    statsFolder.add({" + Add stat": ""}, " + Add stat", stats)
    .onChange(addStat);

    creationGUI.add({transform: evalAction}, "transform");
    defaultInstanceParameters = da.clone(instanceParameters);
    createEvalText();

    function addModifier(newMod) {
        var modDesc = da.modLimits[newMod];
        instanceParameters.Mods[newMod] = 0;
        var mod = modsFolder.add(instanceParameters.Mods, newMod);
        var range = Math.max(Math.abs(modDesc.low), Math.abs(modDesc.high));
        if (range > 40) {
            range = 40;
        }
        mod = mod.min(-range).max(range);
        mod.onChange(createEvalText);
    }

    function addBaseDim(value) {
        var desc = da.baseDimDesc.human[value];
        instanceParameters.basedim[value] = 0;
        var controller = dimsFolder.add(instanceParameters.basedim, value);
        var range = Math.max(Math.abs(desc.low), Math.abs(desc.high));
        if (range > 40) {
            range = 40;
        }
        controller = controller.min(-range).max(range);
        controller.onChange(createEvalText);
    }

    function addStat(value) {
        var desc = da.statLimits[value];
        instanceParameters[value] = 0;
        var controller = statsFolder.add(instanceParameters, value);
        var range = Math.max(Math.abs(desc.low), Math.abs(desc.high));
        if (range > 40) {
            range = 40;
        }
        controller = controller.min(-range).max(range);
        controller.onChange(createEvalText);
    }
};

var guiMenuItem;
var loadingModal = document.getElementById("loading_modal");

function generateGUI(PC) {
    "use strict";
    if (gui) {
        gui.destroy();
    }
    gui = new dat.GUI();

    var drawPoints = gui.add(interactiveCreation, "Drawpoints");
    drawPoints.title("Ctrl + q");
    guiMenuItem = drawPoints.__li;

    var texts = gui.addFolder("Text Colors");
    var colorConfigs = [];
    colorConfigs.push(texts.addColor(config, "nameColor"));
    colorConfigs.push(texts.addColor(config, "genderColor"));
    colorConfigs.push(texts.addColor(config, "heightColor"));
    colorConfigs.push(texts.addColor(config, "heightBarColor"));

    colorConfigs.forEach(function (controller) {
        controller.onChange(function () {
            ex = da.draw(canvasGroup, PC, config);
            drawDrawPoints(window.ex.ctx, window.ex);
        });
    });


    var create = gui.addFolder("Create Character");
    create.add(interactiveCreation, "Presets", Object.keys(presetCharacters))
    .onChange(function (character) {
        presetCharacters[character]();
        // set window to show it's loading

        window.ex = da.draw(canvasGroup, window.PC, config);
        drawDrawPoints(ex.ctx, window.ex);
        if (window.ex instanceof Promise) {
            loadingModal.style.visibility = "visible";
            window.ex.then(function (exports) {
                console.log("Finished Redrawing Character");
                loadingModal.style.visibility = "hidden";
                window.ex = exports;
            });
        }
        generateGUI(window.PC);
    });
    create.add(interactiveCreation, "randomness", 0, 2);
    create.add(interactiveCreation, "female bias", -1, 1);
    var randomize = create.add(interactiveCreation, "Randomize Character");
    randomize.title("Ctrl + Alt + c");
    create.add(interactiveCreation, "Create in Code");


    var exports = gui.addFolder("Export Character");
    var save = exports.add(interactiveCreation, "Save");
    save.title("Ctrl + s");
    var load = exports.add(interactiveCreation, "Load");
    load.title("Ctrl + Shift + s");


    var dimensions = gui.addFolder("Body Dimensions");
    dimensions.add(interactiveCreation, "Get Dimensions");

    var dimFolders = {};
    var sortedProperties = getSortedKeys(da.baseDimDesc[PC.skeleton]);

    var controller, folder, i, linkedPart, p, titleText;
    for (i = 0; i < sortedProperties.length; ++i) {
        p = sortedProperties[i];

        var dimDesc = da.baseDimDesc[PC.skeleton][p];

        linkedPart = "Miscellaneous";
        if (dimDesc.hasOwnProperty("linkedPart")) {
            linkedPart = dimDesc.linkedPart;
        }

        if (dimFolders.hasOwnProperty(linkedPart) === false) {
            dimFolders[linkedPart] = dimensions.addFolder(linkedPart);
        }
        folder = dimFolders[linkedPart];

        controller = folder.add(PC.basedim, p, dimDesc.low, dimDesc.high);
        titleText = "";
        if (dimDesc.hasOwnProperty("units") && dimDesc.units !== "arbitrary") {
            titleText += "(" + dimDesc.units + ") ";
        }
        if (dimDesc.units === "index") {
            controller.step(1);
        }
        if (dimDesc.hasOwnProperty("desc")) {
            titleText += dimDesc.desc;
        }
        controller.title(titleText);

        controller.onChange(redrawAvatar);

    }


    var mods = gui.addFolder("Body Modifiers");
    mods.add(interactiveCreation, "Get Modifiers");

    var modFolders = {};
    sortedProperties = getSortedKeys(da.modLimits);
    for (i = 0; i < sortedProperties.length; ++i) {
        p = sortedProperties[i];

        var modDesc = da.modLimits[p];


        linkedPart = "Miscellaneous";
        if (modDesc.hasOwnProperty("linkedPart")) {
            linkedPart = modDesc.linkedPart;
        }

        if (modFolders.hasOwnProperty(linkedPart) === false) {
            modFolders[linkedPart] = mods.addFolder(linkedPart);
        }
        folder = modFolders[linkedPart];

        var low = (modDesc.low < -1e3) ? -20 : modDesc.low;
        var high = (modDesc.high > 1e3) ? 40 : modDesc.high;
        controller = folder.add(PC.Mods, p, low, high);
        titleText = "";
        if (modDesc.hasOwnProperty("units") && modDesc.units !== "arbitrary") {
            titleText += "(" + modDesc.units + ") ";
        }
        if (modDesc.hasOwnProperty("desc")) {
            titleText += modDesc.desc;
        }
        controller.title(titleText);

        controller.onChange(redrawAvatar.bind(null,
            !(PC.Mods.hasOwnProperty(p) || PC.hasOwnProperty(p))));
    }

    var statistics = gui.addFolder("Statistics");
    statistics.add(interactiveCreation, "Get Stats");

    // create sliders for each stat
    sortedProperties = getSortedKeys(da.statLimits);
    for (i = 0; i < sortedProperties.length; ++i) {
        p = sortedProperties[i];
        var statDesc = da.statLimits[p];

        controller = statistics.add(PC, p, statDesc.low, statDesc.high);
        titleText = "";
        if (statDesc.hasOwnProperty("units") && statDesc.units !== "arbitrary") {
            titleText += "(" + statDesc.units + ") ";
        }
        if (statDesc.hasOwnProperty("desc")) {
            titleText += statDesc.desc;
        }
        controller.title(titleText);

        controller.onChange(redrawAvatar);
    }

    var modify = gui.addFolder("More Customizations");
    modify.open();

    var clothing = modify.addFolder("Clothing");
    clothing.add(interactiveCreation, "Wear Clothing");
    clothing.add(interactiveCreation, "Remove Clothing");
    clothing.add(interactiveCreation, "Strip All");

    var items = modify.addFolder("Items");
    items.add(interactiveCreation, "Wield Item");
    items.add(interactiveCreation, "Remove Item");

    var body = modify.addFolder("Body");
    body.add(interactiveCreation, "Attach Bodypart");
    body.add(interactiveCreation, "Facial Expression");

    modify.add(interactiveCreation, "Animate Transformation");


    // extend right click to toggle expand/collapse all
    var baseFolders = gui.__folders;
    for (folder in baseFolders) {
        if (baseFolders.hasOwnProperty(folder)) {
            baseFolders[folder].__ul.children[0].addEventListener("contextmenu", function (event) {
                event.preventDefault();
                event.stopPropagation();
                recursivelyToggleGUI(this, !this.closed);
            }.bind(baseFolders[folder]), false);
        }
    }
    return gui;
}

generateGUI(PC);
